<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //Hash::make($request->password)
        User::insert([
            'id' => '95d13207-522a-4d9f-9476-942d7e372035',
            'name' => 'superadmin',
            'email' => 'superadmin',
            'password' => Hash::make('superadmin'),
            'username' => 'superadmin',
            'is_ldap' => 0,
            'id_role' => '00100a71-3904-4d40-8804-7e7dc2c50e9b',
            'id_departement' => '9c628ebb-c31c-4b65-9b11-8f5b3abd116f',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s'),
        ]);
    }
}
