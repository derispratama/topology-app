<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('surrounding_servers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_server');
            $table->string('surrounding_server_name',255);
            $table->string('ip_address',255);
            $table->timestamps();

            $table->foreign('id_server')->references('id')->on('servers');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('surrounding_servers');
    }
};
