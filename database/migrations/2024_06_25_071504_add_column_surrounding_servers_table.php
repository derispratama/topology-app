<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::table('surrounding_servers', function (Blueprint $table) {
            $table->string('os', 255);
            $table->string('cpu', 255);
            $table->string('ram', 255);
            $table->string('memory', 255);
            $table->string('type', 255);
            $table->text('installed_apps');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('surrounding_servers', function (Blueprint $table) {
            $table->dropColumn("os");
            $table->dropColumn("cpu");
            $table->dropColumn("ram");
            $table->dropColumn("memory");
            $table->dropColumn("type");
            $table->text('installed_apps');
        });
    }
};
