<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('lb_servers', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('id_server');
            $table->string('name', 255)->default('Load Balancer');
            $table->string('ip_address', 255);
            $table->timestamps();

            $table->foreign('id_server')->references('id')->on('servers');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('lb_servers');
    }
};
