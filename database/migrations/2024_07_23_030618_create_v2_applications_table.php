<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('v2_applications', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('id_department');
            $table->string('application_name', 255);
            $table->text('gojs_meta');
            $table->string('version', 10);
            $table->timestamps();
            $table->foreign('id_department')->references('id')->on('departements');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('v2_applications');
    }
};
