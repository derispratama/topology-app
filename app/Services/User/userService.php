<?php

namespace App\Services\User;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserService
{
    protected $model = User::class;
    public function __construct()
    {
    }

    public function isUserExist($username)
    {
        return User::where('username', $username)->exists();
    }

    public function validateUser($data)
    {
        return Validator::make($data, [
            'name' => 'required',
            'email' => 'required|email',
            'username' => 'required',
            'password' => 'required',
            'is_ldap' => 'required',
            'id_role' => 'required',
            'id_departement' => 'required',
        ]);
    }

    public function validateUserUpdate($data)
    {
        return Validator::make($data, [
            'name' => 'required',
            'email' => 'required|email',
            'username' => 'required',
            'is_ldap' => 'required',
            'id_role' => 'required',
            'id_departement' => 'required',
        ]);
    }

    public function createUser($data)
    {
        $user = new User;
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->username = $data['username'];
        $user->password = Hash::make($data['password']);
        $user->is_ldap = $data['is_ldap'];
        $user->id_role = $data['id_role'];
        $user->id_departement = $data['id_departement'];
        $user->save();

        return $user;
    }

    public function updateUser($data, $id)
    {
        $user = User::findOrFail($id);
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->username = $data['username'];
        $user->is_ldap = $data['is_ldap'];
        $user->id_role = $data['id_role'];
        $user->id_departement = $data['id_departement'];
        if (isset($data['password'])) {
            $user->password = Hash::make($data['password']);
        }
        $user->save();

        return $user;
    }

    public function getById($id)
    {
        return User::findOrFail($id);
    }
}
