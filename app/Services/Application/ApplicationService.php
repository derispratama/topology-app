<?php

namespace App\Services\Application;

use App\Models\User;

class ApplicationService
{

    public function __construct()
    {
    }

    public function isSuperadmin($user_id)
    {
        $user = User::where('id', $user_id)->with('roles')->get();

        return $user[0]->roles->role_name == 'superadmin' ? true : false;
    }
}
