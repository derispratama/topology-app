<?php

namespace App\Services\Dashboard;

use App\Models\Application;
use App\Models\Departement;
use App\Models\User;

class DashboardService
{

    public function __construct()
    {
    }

    public function getApplicationData()
    {
        $result = [];
        $departments = Departement::with('applications')->get();

        foreach ($departments as $department) {
            array_push($result, [
                'id' => $department->id,
                'name' => $department->departement_name,
                'total' => $department->applications->count()
            ]);
        }
        return $result;
    }

    public function getCardInfo()
    {
        $totalApplications =  Application::count();
        $totalDepartments = Departement::count();
        $totalUsers = User::count();

        return [
            [
                'title' => 'Total Applications',
                'count' => $totalApplications,
                'icon' => 'bi-table',
                'url' => '/application'
            ],
            [
                'title' => 'Total Department',
                'count' => $totalDepartments,
                'icon' => 'bi-buildings',
                'url' => '/department'
            ],
            [
                'title' => 'Total Users',
                'count' => $totalUsers,
                'icon' => 'bi-people',
                'url' => '/users'
            ]
        ];
    }
}
