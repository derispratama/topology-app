<?php

namespace App\Http\Controllers;

use App\Services\User\UserService;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public $userService;
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function index($userId)
    {
        $user = $this->userService->getById($userId);
        if ($user) {
            // dd($user);
            // return response()->json($user->id);

            return view('settings.profile.index', ['user' => $user]);
        } else {
            return response()->json(['error' => 'User not found'], 404);
        }
        // return view('settings.profile.index');
    }
}
