<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Services\Dashboard\DashboardService;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    protected $dashboardService;

    public function __construct(DashboardService $dashboardService)
    {
        $this->dashboardService = $dashboardService;
    }
    public function index()
    {

        $applications = $this->dashboardService->getApplicationData();
        $card = $this->dashboardService->getCardInfo();
        $result = [
            'applications' => $applications,
            'card' => $card
        ];
        return response()->json([
            'messages' => 'success',
            'status' => true,
            'data' => $result
        ]);
    }
}
