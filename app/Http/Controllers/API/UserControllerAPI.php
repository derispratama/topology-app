<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Services\User\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserControllerAPI extends Controller
{
    protected $model = User::class;
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function index()
    {
        $data = User::with('department')->with('roles')->get();
        return UserResource::collection($data);
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $validator = $this->userService->validateUser($request->all());

            if ($validator->passes()) {
                if ($this->userService->isUserExist($request->username, $request->email)) {
                    DB::rollBack();
                    return response()->json([
                        "messages" => "Username or Email already exist"
                    ], 400);
                }
                $data = $this->userService->createUser($request->all());
                DB::commit();
                return response()->json($data, 200);
            } else {
                DB::rollBack();
                return response()->json([
                    "messages" => $validator->errors()
                ], 400);
            }
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json([
                "messages" => $th->getMessage()
            ], 500);
        }
    }

    public function update(Request $request, string $id)
    {
        DB::beginTransaction();
        try {
            $validator = $this->userService->validateUserUpdate($request->all());

            if (!$validator->passes()) {
                DB::rollBack();
                return response()->json([
                    "messages" => $validator->errors()
                ], 400);
            }

            $user = $this->userService->getById($id);
            if ($user->username != $request->username) {
                if ($this->userService->isUserExist($request->username, $request->email)) {
                    DB::rollBack();
                    return response()->json([
                        "messages" => "Username or Email already exist"
                    ], 400);
                }
            }
            $result = $this->userService->updateUser($request->all(), $id);

            DB::commit();
            return response()->json($result, 200);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json([
                "messages" => $th->getMessage()
            ], 400);
        }
    }

    public function show(string $id)
    {
        try {
            $result = $this->userService->getById($id)->load('department', 'roles');
            return response()->json($result, 200);
        } catch (\Throwable $th) {
            return response()->json([
                "messages" => $th->getMessage()
            ], 400);
        }
    }

    public function destroy(string $id)
    {
        try {
            $data = User::findOrFail($id);
            $data->delete();
            return response()->json($data, 200);
        } catch (\Throwable $th) {
            return response()->json([
                "messages" => $th->getMessage()
            ], 400);
        }
    }
}
