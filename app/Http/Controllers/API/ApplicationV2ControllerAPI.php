<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\ApplicationV2Resource;
use App\Models\ApplicationV2;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ApplicationV2ControllerAPI extends Controller
{
    protected $model = ApplicationV2::class;

    public function index()
    {
        $data = ApplicationV2::with('department')->get();
        return ApplicationV2Resource::collection($data);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'application_name' => 'required',
            'id_department' => 'required',
            'gojs_meta' => 'required',
            'version' => 'required',
        ]);

        if ($validator->passes()) {
            $data = new ApplicationV2;
            $data->application_name = $request->application_name;
            $data->id_department = $request->id_department;
            $data->gojs_meta = $request->gojs_meta;
            $data->version = $request->version;
            $data->save();

            return response()->json($data, 200);
        } else {
            return response()->json([
                "messages" => $validator->errors()
            ], 400);
        }
    }

    public function update(Request $request, string $id)
    {
        $validator = Validator::make($request->all(), [
            'application_name' => 'required',
            'id_department' => 'required',
            'gojs_meta' => 'required',
            'version' => 'required',
        ]);

        if ($validator->passes()) {
            $data = ApplicationV2::findOrFail($id);
            $data->application_name = $request->application_name;
            $data->id_department = $request->id_department;
            $data->gojs_meta = $request->gojs_meta;
            $data->version = $request->version;
            $data->save();

            return response()->json($data, 200);
        } else {
            return response()->json([
                "messages" => $validator->errors()
            ], 400);
        }
    }

    public function show(string $id)
    {
        $data = ApplicationV2::findOrFail($id);
        return new ApplicationV2Resource($data);
    }

    public function destroy(string $id)
    {
        try {
            $data = ApplicationV2::findOrFail($id);
            $data->delete();
            return response()->json($data, 200);
        } catch (\Throwable $th) {
            return response()->json([
                "messages" => $th->getMessage()
            ], 400);
        }
    }
}
