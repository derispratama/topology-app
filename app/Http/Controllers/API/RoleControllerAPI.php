<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\RoleResource;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleControllerAPI extends Controller
{
    protected $model = Role::class;

    public function index()
    {
        $data = Role::all();
        return RoleResource::collection($data);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'role_name' => 'required',
        ]);

        if ($validator->passes()) {
            $data = new Role;
            $data->role_name = $request->role_name;
            $data->save();

            return response()->json($data, 200);
        } else {
            return response()->json([
                "messages" => $validator->errors()
            ], 400);
        }
    }

    public function update(Request $request,string $id)
    {
        $validator = Validator::make($request->all(), [
            'role_name' => 'required',
        ]);

        if ($validator->passes()) {
            $data = Role::findOrFail($id);
            $data->role_name = $request->role_name;
            $data->save();

            return response()->json($data, 200);
        } else {
            return response()->json([
                "messages" => $validator->errors()
            ], 400);
        }
    }

    public function show(string $id)
    {
        $data = Role::findOrFail($id);
        return new RoleResource($data);
    }

    public function destroy(string $id)
    {
        //
    }
}
