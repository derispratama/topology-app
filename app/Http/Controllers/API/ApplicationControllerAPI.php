<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\ApplicationResource;
use App\Models\Application;
use App\Models\LbServer;
use App\Models\Server;
use App\Models\Service;
use App\Models\SurroundingServer;
use App\Services\Application\ApplicationService;
use App\Services\User\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApplicationControllerAPI extends Controller
{
    protected $model = Application::class;
    protected $applicationService;
    protected $userService;

    public function __construct(ApplicationService $applicationService, UserService $userService)
    {
        $this->applicationService = $applicationService;
        $this->userService = $userService;
    }

    public function index(Request $request)
    {
        try {
            $payload = $request->query('payload');
            if (!$payload) {
                return response()->json(['error' => 'Payload is missing'], 400);
            }

            $decode_payload = json_decode(base64_decode($payload), true);
            if (json_last_error() !== JSON_ERROR_NONE) {
                return response()->json(['error' => 'Invalid payload format'], 400);
            }

            $department_id = $decode_payload['department_id'] ?? null;
            $user_id = $decode_payload['user_id'] ?? null;

            if (!$user_id) {
                return response()->json(['error' => 'User ID is missing'], 400);
            }

            $is_superadmin = $this->applicationService->isSuperadmin($user_id);

            if (!$is_superadmin) {
                $user = $this->userService->getById($user_id);
                if (!$user) {
                    return response()->json(['error' => 'User not found'], 404);
                }

                $data = $this->model::with('servers.surrounding_servers.services')
                    ->with('departement')
                    ->where('applications.id_departement', $user->id_departement)
                    ->get();

                return ApplicationResource::collection($data);
            }

            if ($department_id) {
                $data = $this->model::with('servers.surrounding_servers.services')
                    ->with('departement')
                    ->where('applications.id_departement', $department_id)
                    ->get();

                return ApplicationResource::collection($data);
            }

            $data = $this->model::with('servers.surrounding_servers.services')
                ->with('departement')
                ->get();

            return ApplicationResource::collection($data);
        } catch (\Exception $e) {
            return response()->json(['error' => 'An error occurred: ' . $e->getMessage()], 500);
        }
    }


    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            return $this->insertApp($request);
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json([
                "id_arr_sv" => $th->getMessage(),
                "code" => $th->getCode(),
                "line" => $th->getLine(),
                "file" => $th->getFile(),
            ], 400);
        }
    }

    public function update(Request $request, string $id)
    {
        DB::beginTransaction();
        try {
            return $this->deleteUpdateApp($request, $id, 'update');
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json([
                "id_arr_sv" => $th->getMessage(),
                "code" => $th->getCode(),
                "line" => $th->getLine(),
                "file" => $th->getFile(),
            ], 400);
        }
    }


    public function show(string $id)
    {
        $data =  $this->model::with('servers.surrounding_servers.services')->with('servers.lb_servers.surrounding_servers.services')->with('departement')->where('applications.id', $id)->get();
        return ApplicationResource::collection($data);
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            return $this->deleteUpdateApp('', $id, 'delete');
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json([
                "id_arr_sv" => $th->getMessage(),
                "code" => $th->getCode(),
                "line" => $th->getLine(),
                "file" => $th->getFile(),
            ], 400);
        }
    }

    function deleteUpdateApp($request = '', $id, $action)
    {
        $server = Server::where('id_application', $id)->get();

        if (count($server) > 0 && isset($server[0]->id)) {
            $id_arr_sv = [];
            $id_arr_sr_sv = [];

            for ($i = 0; $i < count($server); $i++) {
                array_push($id_arr_sv, $server[$i]->id);
            }

            $surrounding_server = SurroundingServer::whereIn('id_server', $id_arr_sv)->get();
            $lb_server = LbServer::whereIn('id_server', $id_arr_sv)->get();

            for ($i = 0; $i < count($surrounding_server); $i++) {
                array_push($id_arr_sr_sv, $surrounding_server[$i]->id);
            }

            if (count($id_arr_sr_sv) > 0) {
                $check_service = Service::whereIn('id_surrounding_server', $id_arr_sr_sv)->get();

                if (count($check_service) > 0 && isset($check_service[0]->id)) {
                    Service::whereIn('id_surrounding_server', $id_arr_sr_sv)->delete();
                }
            }

            if (count($id_arr_sv) > 0) {
                if (count($surrounding_server) > 0 && isset($surrounding_server[0]->id)) {
                    SurroundingServer::whereIn('id_server', $id_arr_sv)->delete();
                }
            }

            if (count($id_arr_sv) > 0) {
                if (count($lb_server) > 0 && isset($lb_server[0]->id)) {
                    LbServer::whereIn('id_server', $id_arr_sv)->delete();
                }
            }

            Server::where('id_application', $id)->delete();
        }

        Application::where('id', $id)->delete();

        if ($action == 'update') {
            return $this->insertApp($request);
        } else {
            DB::commit();
            return response()->json([
                "status" => true,
                "messages" => "Data has been deleted",
            ], 200);
        }
    }

    function insertApp($request)
    {
        $data_application = new Application();
        $data_application->id_departement = $request['departement']['id'];
        $data_application->application_name = $request['application_name'];
        $data_application->created_at = date('Y-m-d h:i:s');
        $data_application->updated_at = date('Y-m-d h:i:s');
        $data_application->ip_load_balancer = $request['ip_load_balancer'];
        $data_application->save();

        for ($i = 0; $i < count($request['servers']); $i++) {
            $data_server = new Server();
            $data_server->id_application = $data_application->id;
            $data_server->server_name = $request['servers'][$i]['server_name'];
            $data_server->ip_address = $request['servers'][$i]['ip_address'];
            $data_server->os = $request['servers'][$i]['os'];
            $data_server->cpu = $request['servers'][$i]['cpu'];
            $data_server->ram = $request['servers'][$i]['ram'];
            $data_server->memory = $request['servers'][$i]['memory'];
            $data_server->installed_apps = $request['servers'][$i]['installed_apps'];
            $data_server->created_at = date('Y-m-d h:i:s');
            $data_server->updated_at = date('Y-m-d h:i:s');
            $data_server->save();

            for ($j = 0; $j < count($request['servers'][$i]['surrounding_servers']); $j++) {
                $data_surrounding_server = new SurroundingServer();
                $data_surrounding_server->id_server = $data_server->id;
                $data_surrounding_server->surrounding_server_name = $request['servers'][$i]['surrounding_servers'][$j]['surrounding_server_name'];
                $data_surrounding_server->ip_address = $request['servers'][$i]['surrounding_servers'][$j]['ip_address'];
                $data_surrounding_server->os = $request['servers'][$i]['surrounding_servers'][$j]['os'];
                $data_surrounding_server->cpu = $request['servers'][$i]['surrounding_servers'][$j]['cpu'];
                $data_surrounding_server->ram = $request['servers'][$i]['surrounding_servers'][$j]['ram'];
                $data_surrounding_server->memory = $request['servers'][$i]['surrounding_servers'][$j]['memory'];
                $data_surrounding_server->type = $request['servers'][$i]['surrounding_servers'][$j]['type'];
                $data_surrounding_server->installed_apps = $request['servers'][$i]['surrounding_servers'][$j]['installed_apps'];
                $data_surrounding_server->created_at = date('Y-m-d h:i:s');
                $data_surrounding_server->updated_at = date('Y-m-d h:i:s');
                $data_surrounding_server->id_lb = null;
                $data_surrounding_server->save();

                for ($k = 0; $k < count($request['servers'][$i]['surrounding_servers'][$j]['services']); $k++) {
                    $data_service = new Service();
                    $data_service->id_surrounding_server = $data_surrounding_server->id;
                    $data_service->service_name = $request['servers'][$i]['surrounding_servers'][$j]['services'][$k]['service_name'];
                    $data_service->port = $request['servers'][$i]['surrounding_servers'][$j]['services'][$k]['port'];
                    $data_service->params_request = $request['servers'][$i]['surrounding_servers'][$j]['services'][$k]['params_request'];
                    $data_service->params_response = $request['servers'][$i]['surrounding_servers'][$j]['services'][$k]['params_response'];
                    $data_service->url = $request['servers'][$i]['surrounding_servers'][$j]['services'][$k]['url'];
                    $data_service->created_at = date('Y-m-d h:i:s');
                    $data_service->updated_at = date('Y-m-d h:i:s');
                    $data_service->save();
                }
            }

            for ($j = 0; $j < count($request['servers'][$i]['lb_servers']); $j++) {
                $data_lb_server = new LbServer();
                $data_lb_server->id_server = $data_server->id;
                $data_lb_server->name = $request['servers'][$i]['lb_servers'][$j]['name'];
                $data_lb_server->ip_address = $request['servers'][$i]['lb_servers'][$j]['ip_address'];
                $data_lb_server->created_at = date('Y-m-d h:i:s');
                $data_lb_server->updated_at = date('Y-m-d h:i:s');
                $data_lb_server->save();

                for ($k = 0; $k < count($request['servers'][$i]['lb_servers'][$j]['surrounding_servers']); $k++) {
                    $data_surrounding_server = new SurroundingServer();
                    $data_surrounding_server->id_server = $data_lb_server->id_server;
                    $data_surrounding_server->surrounding_server_name = $request['servers'][$i]['lb_servers'][$j]['surrounding_servers'][$k]['surrounding_server_name'];
                    $data_surrounding_server->ip_address = $request['servers'][$i]['lb_servers'][$j]['surrounding_servers'][$k]['ip_address'];
                    $data_surrounding_server->os = $request['servers'][$i]['lb_servers'][$j]['surrounding_servers'][$k]['os'];
                    $data_surrounding_server->cpu = $request['servers'][$i]['lb_servers'][$j]['surrounding_servers'][$k]['cpu'];
                    $data_surrounding_server->ram = $request['servers'][$i]['lb_servers'][$j]['surrounding_servers'][$k]['ram'];
                    $data_surrounding_server->memory = $request['servers'][$i]['lb_servers'][$j]['surrounding_servers'][$k]['memory'];
                    $data_surrounding_server->type = $request['servers'][$i]['lb_servers'][$j]['surrounding_servers'][$k]['type'];
                    $data_surrounding_server->installed_apps = $request['servers'][$i]['lb_servers'][$j]['surrounding_servers'][$k]['installed_apps'];
                    $data_surrounding_server->created_at = date('Y-m-d h:i:s');
                    $data_surrounding_server->updated_at = date('Y-m-d h:i:s');
                    $data_surrounding_server->id_lb = $data_lb_server->id;
                    $data_surrounding_server->save();

                    for ($sv = 0; $sv < count($request['servers'][$i]['lb_servers'][$j]['surrounding_servers'][$k]['services']); $sv++) {
                        $data_service = new Service();
                        $data_service->id_surrounding_server = $data_surrounding_server->id;
                        $data_service->service_name = $request['servers'][$i]['lb_servers'][$j]['surrounding_servers'][$k]['services'][$sv]['service_name'];
                        $data_service->port = $request['servers'][$i]['lb_servers'][$j]['surrounding_servers'][$k]['services'][$sv]['port'];
                        $data_service->params_request = $request['servers'][$i]['lb_servers'][$j]['surrounding_servers'][$k]['services'][$sv]['params_request'];
                        $data_service->params_response = $request['servers'][$i]['lb_servers'][$j]['surrounding_servers'][$k]['services'][$sv]['params_response'];
                        $data_service->url = $request['servers'][$i]['lb_servers'][$j]['surrounding_servers'][$k]['services'][$sv]['url'];
                        $data_service->created_at = date('Y-m-d h:i:s');
                        $data_service->updated_at = date('Y-m-d h:i:s');
                        $data_service->save();
                    }
                }
            }
        }
        DB::commit();

        return response()->json([
            "status" => true,
            "messages" => "Data has been saved",
        ], 200);
    }
}
