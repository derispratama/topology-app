<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\DepartementResource;
use App\Models\Departement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DepartementControllerAPI extends Controller
{
    protected $model = Departement::class;

    public function index()
    {
        $data = Departement::with('users')->get();
        return DepartementResource::collection($data);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'departement_name' => 'required',
        ]);

        if ($validator->passes()) {
            $data = new Departement;
            $data->departement_name = $request->departement_name;
            $data->save();

            return response()->json($data, 200);
        } else {
            return response()->json([
                "messages" => $validator->errors()
            ], 400);
        }
    }

    public function update(Request $request, string $id)
    {
        $validator = Validator::make($request->all(), [
            'departement_name' => 'required',
        ]);

        if ($validator->passes()) {
            $data = Departement::findOrFail($id);
            $data->departement_name = $request->departement_name;
            $data->save();

            return response()->json($data, 200);
        } else {
            return response()->json([
                "messages" => $validator->errors()
            ], 400);
        }
    }

    public function show(string $id)
    {
        $data = Departement::findOrFail($id);
        return new DepartementResource($data);
    }

    public function destroy(string $id)
    {
        try {
            $data = Departement::findOrFail($id);
            $data->delete();

            return response()->json(null, 204);
        } catch (\Exception $e) {
            return response()->json([
                "message" => "Failed to delete the department.",
                "error" => $e->getMessage()
            ], 500);
        }
    }
}
