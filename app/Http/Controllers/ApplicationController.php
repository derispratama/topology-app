<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApplicationController extends Controller
{
    public function index(Request $request)
    {
        $departmentId = $request->query('id');

        return view('settings.application.index', ['id' => $departmentId]);
    }

    public function index_v2(Request $request)
    {
        $departmentId = $request->query('id');

        return view('settings.application.index_v2', ['id' => $departmentId]);
    }

    public function create()
    {
        return view('settings.application.gojs');
    }
    public function createv2()
    {
        return view('settings.application.newIndex');
    }
    public function createOld()
    {
        return view('settings.application.form');
    }

    public function edit($id)
    {
        return view('settings.application.newIndex', [
            'id' => $id
        ]);
    }
    public function edit_v2($id)
    {
        return view('settings.application.gojs', [
            'id' => $id
        ]);
    }
    public function show($id)
    {
        return view('settings.application.view', [
            'id' => $id
        ]);
    }
    public function show_v2($id)
    {
        return view('settings.application.view_v2', [
            'id' => $id
        ]);
    }
}
