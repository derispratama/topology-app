<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApplicationV2Controller extends Controller
{
    public function index(Request $request)
    {
        $departmentId = $request->query('id');

        return view('settings.application.index', ['id' => $departmentId]);
    }

    public function create()
    {
        return view('settings.application.newIndex');
    }

    public function edit($id)
    {
        return view('settings.application.newIndex', [
            'id' => $id
        ]);
    }
    public function show($id)
    {
        return view('settings.application.view', [
            'id' => $id
        ]);
    }
}
