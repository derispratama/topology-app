<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class UserInfoDetails extends Component
{
    /**
     * Create a new component instance.
     */


    public $name;
    public $email;
    public $username;
    public $role;
    public $department;
    public function __construct(
        $name,
        $email,
        $username,
        $role,
        $department
    ) {
        $this->name = $name;
        $this->email = $email;
        $this->username = $username;
        $this->role = $role;
        $this->department = $department;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.user-info-details');
    }
}
