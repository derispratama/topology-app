<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class InfoRow extends Component
{
    /**
     * Create a new component instance.
     */
    public $field;
    public $value;
    public $isBottom;
    public function __construct($field, $value, $isBottom = false)
    {
        $this->field = $field;
        $this->value = $value;
        $this->isBottom = $isBottom;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.info-row');
    }
}
