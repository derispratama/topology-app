<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class ConfirmAlertModal extends Component
{
    /**
     * Create a new component instance.
     */

    public $title;
    public $message;
    public $callback;
    public function __construct($title = '', $message = '', $callback = null)
    {
        $this->title = $title;
        $this->message = $message;
        $this->callback = $callback;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.confirm-alert-modal');
    }
}
