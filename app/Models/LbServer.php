<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Laravel\Sanctum\HasApiTokens;

class LbServer extends Model
{
    use HasFactory, HasUuids, HasApiTokens;

    protected $guarded = [];

    public function surrounding_servers(): HasMany
    {
        return $this->hasMany(SurroundingServer::class, 'id_lb', 'id');
    }
}
