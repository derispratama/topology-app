<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Laravel\Sanctum\HasApiTokens;

class Server extends Model
{
    use HasFactory, HasUuids, HasApiTokens;

    protected $guarded = [];

    public function surrounding_servers(): HasMany
    {
        return $this->hasMany(SurroundingServer::class, 'id_server', 'id')->whereNull('surrounding_servers.id_lb');
    }

    public function lb_servers(): HasMany
    {
        return $this->hasMany(LbServer::class, 'id_server', 'id');
    }
}
