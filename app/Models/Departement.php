<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Laravel\Sanctum\HasApiTokens;

class Departement extends Model
{
    use HasFactory, HasUuids, HasApiTokens;

    public function users(): HasMany
    {
        return $this->hasMany(User::class, 'id_departement', 'id');
    }

    public function applications(): HasMany
    {
        return $this->hasMany(Application::class, 'id_departement', 'id');
    }
}
