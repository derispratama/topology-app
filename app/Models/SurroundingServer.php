<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Laravel\Sanctum\HasApiTokens;

class SurroundingServer extends Model
{
    use HasFactory, HasUuids, HasApiTokens;

    protected $guarded = [];

    public function services(): HasMany
    {
        return $this->hasMany(Service::class, 'id_surrounding_server', 'id');
    }

    public function lb_servers(): HasOne
    {
        return $this->hasOne(LbServer::class, 'id', 'id_lb');
    }
}
