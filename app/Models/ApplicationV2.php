<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Laravel\Sanctum\HasApiTokens;

class ApplicationV2 extends Model
{
    use HasFactory, HasUuids, HasApiTokens;
    protected $table = 'v2_applications';

    public function department(): BelongsTo
    {
        return $this->belongsTo(Departement::class, 'id_department', 'id');
    }
}
