<?php

use App\Http\Controllers\API\RoleController;
use App\Http\Controllers\ApplicationController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

Route::get('/application/create', [ApplicationController::class, 'create']);
Route::get('/', function () {
    return view('auth.login');
})->name('login');

Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
Route::get('/application-old', [ApplicationController::class, 'index'])->name('application');
Route::get('/application', [ApplicationController::class, 'index_v2'])->name('application');
Route::get('/application/create-old-v2', [ApplicationController::class, 'createv2']);
Route::get('/application/create-old', [ApplicationController::class, 'createOld']);
Route::get('/application-old/{id}/edit', [ApplicationController::class, 'edit']);
Route::get('/application/{id}/edit', [ApplicationController::class, 'edit_v2']);
Route::get('/application-old/{id}', [ApplicationController::class, 'show']);
Route::get('/application/{id}', [ApplicationController::class, 'show_v2']);

//Department Web Route
Route::get('/department', [DepartmentController::class, 'index'])->name('department');

//User Web Route
Route::get('/users', [UserController::class, 'index'])->name('users');

Route::get('/profile/{userId}', [ProfileController::class, 'index'])->name('profile');
