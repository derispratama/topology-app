<?php

use App\Http\Controllers\API\ApplicationControllerAPI;
use App\Http\Controllers\API\ApplicationV2ControllerAPI;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\api\DashboardController;
use App\Http\Controllers\API\DepartementController;
use App\Http\Controllers\API\DepartementControllerAPI;
use App\Http\Controllers\API\RoleController;
use App\Http\Controllers\API\RoleControllerAPI;
use App\Http\Controllers\API\UserController;
use App\Http\Controllers\API\UserControllerAPI;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:sanctum');

Route::middleware(['auth:sanctum', 'abilities:check-status,place-orders'])->group(function () {
    Route::post('/logout', [AuthController::class, 'logout'])->name('logout');
    Route::get('/dashboard', [DashboardController::class, 'index']);
    Route::apiResource('/applications', ApplicationControllerAPI::class);
    Route::apiResource('/v2_applications', ApplicationV2ControllerAPI::class);
    Route::apiResource('/roles', RoleControllerAPI::class);
    Route::apiResource('/departements', DepartementControllerAPI::class);
    Route::apiResource('/users', UserControllerAPI::class);
});

Route::post('/login', [AuthController::class, 'login']);
