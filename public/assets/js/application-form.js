import { defaultErrorSwal, postDiagram, updateAppById } from "./api.js";
import { getToken } from "./auth.js";
import { generateTopology, getDiagramById } from "./generate-topology.js";
import { renderTable } from "./render-table.js";
import { validateForm, validateIP } from "./utils.js";

document.addEventListener("DOMContentLoaded", function () {
  getToken();
  const pathUrl = getPathUrl();
  const colors = ["#ffffff", "#e0e0e0", "#b0b0b0"];
  const serverTableBody = document.querySelector("#serverTable tbody");
  const uuidDepartment = JSON.parse(localStorage.getItem("user"))[
    "id_departement"
  ];
  const defaultServer = {
    server_name: "",
    ip_address: "",
    os: "",
    cpu: "",
    ram: "",
    memory: "",
    installed_apps: "",
    surrounding_servers: [],
  };
  let serverType = "server";
  let servers = [];
  const appId = getId();
  if (appId && appId !== "") {
    const getDiagram = async (id) => {
      try {
        const result = await getDiagramById(id);
        servers = result.data[0].servers;
        disabeButton(false);
        renderTable(getTableData());
      } catch (error) {
        console.error("Error fetching diagram data:", error);
      }
    };
    getDiagram(appId);
  }

  function getTableData() {
    return {
      servers,
      serverTableBody,
      colors,
    };
  }
  const applicationName = document.getElementById("application_name");
  const generateBtn = document.getElementById("generateBtn");
  const saveBtn = document.getElementById("saveBtn");

  const appNameError = document.getElementById("application_name_error");
  const errorData = {
    applicationName: "",
  };
  const addServerBtn = document.getElementById("addServerBtn");
  const confirmModal = new bootstrap.Modal(
    document.getElementById("confirmModal")
  );
  const confirmSurroundingModal = new bootstrap.Modal(
    document.getElementById("confirmSurroundingModal")
  );
  const updateServiceOfSurroundingServerModal = new bootstrap.Modal(
    document.getElementById("updateServiceOfSurroundingServerModal")
  );
  const confirmServiceSurroundingModal = new bootstrap.Modal(
    document.getElementById("confirmServiceSurroundingModal")
  );
  const inputServerModal = new bootstrap.Modal(
    document.getElementById("inputServerModal")
  );
  const cancleInputServerModal = document.getElementById(
    "cancleInputServerModal"
  );

  const confirmRemoveBtn = document.getElementById("confirmRemoveBtn");
  const confirmRemoveSurroundingBtn = document.getElementById(
    "confirmRemoveSurroundingBtn"
  );
  const confirmRemoveServiceSurroundingBtn = document.getElementById(
    "confirmRemoveServiceSurroundingBtn"
  );
  const confirmServiceOfSurroundingServerBtn = document.getElementById(
    "confirmServiceOfSurroundingServerBtn"
  );
  const cancelRemoveBtn = document.getElementById("cancelRemoveBtn");
  const cancelRemoveSurroundingBtn = document.getElementById(
    "cancelRemoveSurroundingBtn"
  );
  const cancelRemoveServiceSurroundingBtn = document.getElementById(
    "cancelRemoveServiceSurroundingBtn"
  );
  const cancelServiceOfSurroundingServerBtn = document.getElementById(
    "cancelServiceOfSurroundingServerBtn"
  );
  disabeButton(true);
  let currentIndexToRemove = null;
  let currentSurroundingIndexToRemove = {
    serverIndex: null,
    sIndex: null,
  };

  let currentServiceIndex = {
    serverIndex: null,
    sIndex: null,
    serviceIndex: null,
  };

  function renderError() {
    appNameError.innerHTML = errorData.applicationName;
  }

  renderTable(getTableData());

  window.updateServer = function (index, key, value) {
    servers[index][key] = value;
    updateDiagram();
  };

  window.addSurroundingServer = function (index) {
    servers[index].surrounding_servers.push({
      surrounding_server_name: "",
      ip_address: "",
      os: "",
      cpu: "",
      ram: "",
      memory: "",
      type: "",
      installed_apps: "",
      services: [],
    });
    updateDiagram();
  };

  window.updateSurroundingServer = function (serverIndex, sIndex, key, value) {
    servers[serverIndex].surrounding_servers[sIndex][key] = value;
    updateDiagram();
  };

  window.addService = function (index, sIndex) {
    servers[index].surrounding_servers[sIndex].services.push({
      service_name: "",
      port: null,
      params_request: "",
      params_response: "",
      url: "",
    });
    renderTable(getTableData());
  };

  window.showViewServiceModal = function (...args) {
    isView = true;
    showServiceModal(...args);
  };
  window.showServiceModal_ = function (...args) {
    isView = false;
    showServiceModal(...args);
  };

  function showServiceModal(...args) {
    const serverIndex = args[0];
    const surroundingIndex = args[1];
    const serviceIndex = args[2];

    currentServiceIndex.serverIndex = serverIndex;
    currentServiceIndex.sIndex = surroundingIndex;
    currentServiceIndex.serviceIndex = serviceIndex;

    // Fetch the correct service object using serviceIndex
    const service =
      servers[serverIndex].surrounding_servers[surroundingIndex].services[
        serviceIndex
      ];

    // Get the modal input elements
    const serviceNameInput = document.getElementById("inputServiceName");
    const portInput = document.getElementById("inputPort");
    const inputRequestParams = document.getElementById("inputRequestParams");
    const inputResponseParams = document.getElementById("inputResponseParams");
    const inputUrl = document.getElementById("inputUrl");

    const elementLists = [
      serviceNameInput,
      portInput,
      inputRequestParams,
      inputResponseParams,
      inputUrl,
    ];

    elementLists.forEach((elementList) => {
      if (isView) {
        elementList.readOnly = true;
        document
          .getElementById("confirmServiceOfSurroundingServerBtn")
          .classList.add("d-none");
        document.getElementById(
          "cancelServiceOfSurroundingServerBtn"
        ).innerHTML = "Done";
      } else elementList.readOnly = false;
    });

    // Update the input values with the service data
    serviceNameInput.value = service.service_name;
    portInput.value = service.port;
    inputRequestParams.value = service.params_request;
    inputResponseParams.value = service.params_response;
    inputUrl.value = service.url;

    // Show the modal
    updateServiceOfSurroundingServerModal.show();
  }

  window.removeServer = function (index) {
    servers.splice(index, 1);
    renderTable(getTableData());
  };

  window.removeSurroundingServer = function (serverIndex, sIndex) {
    servers[serverIndex].surrounding_servers.splice(sIndex, 1);
    renderTable(getTableData());
  };

  window.confirmRemoveServer = function (index) {
    currentIndexToRemove = index;
    confirmModal.show();
  };

  window.confirmRemoveSurroundingServer = function (serverIndex, sIndex) {
    currentSurroundingIndexToRemove = {
      serverIndex,
      sIndex,
    };
    confirmSurroundingModal.show();
  };

  window.confirmRemoveService = function (serverIndex, sIndex, serviceIndex) {
    currentServiceIndex = {
      serverIndex,
      serviceIndex,
      sIndex,
    };
    confirmServiceSurroundingModal.show();
  };

  let previousAppNameState = applicationName.value.trim() === "" ? false : true;

  applicationName.addEventListener("input", function () {
    const currentState = applicationName.value.trim() === "" ? false : true;

    if (currentState) {
      disabeButton(false);
    } else {
      disabeButton(true);
    }

    updateDiagram();
  });

  function disabeButton(val) {
    addServerBtn.disabled = val;
    generateBtn.disabled = val;
    saveBtn.disabled = val;
  }

  cancleInputServerModal.addEventListener("click", () => {
    inputServerModal.hide();
  });
  confirmRemoveBtn.addEventListener("click", function () {
    if (currentIndexToRemove !== null) {
      servers.splice(currentIndexToRemove, 1);
      currentIndexToRemove = null;
      confirmModal.hide();
      updateDiagram();
    }
  });

  confirmRemoveSurroundingBtn.addEventListener("click", function () {
    const { serverIndex, sIndex } = currentSurroundingIndexToRemove;
    if (serverIndex !== null && sIndex !== null) {
      servers[serverIndex].surrounding_servers.splice(sIndex, 1);
      currentSurroundingIndexToRemove = {
        serverIndex: null,
        sIndex: null,
      };
      confirmSurroundingModal.hide();
      updateDiagram();
    }
  });

  confirmRemoveServiceSurroundingBtn.addEventListener("click", function () {
    const { serverIndex, sIndex, serviceIndex } = currentServiceIndex;
    if (serverIndex !== null && sIndex !== null && serverIndex !== null) {
      servers[serverIndex].surrounding_servers[sIndex].services.splice(
        serviceIndex,
        1
      );
      currentServiceIndex = {
        serverIndex: null,
        serviceIndex: null,
        sIndex: null,
      };
    }
    confirmServiceSurroundingModal.hide();
    updateDiagram();
  });

  confirmServiceOfSurroundingServerBtn.addEventListener("click", function () {
    const { serverIndex, sIndex, serviceIndex } = currentServiceIndex;
    const data = {
      service_name: document.getElementById("inputServiceName").value,
      port: document.getElementById("inputPort").value,
      params_request: document.getElementById("inputRequestParams").value,
      params_response: document.getElementById("inputResponseParams").value,
      url: document.getElementById("inputUrl").value,
    };
    servers[serverIndex].surrounding_servers[sIndex].services[serviceIndex] =
      data;
    currentServiceIndex = {
      serverIndex: null,
      serviceIndex: null,
      sIndex: null,
    };
    updateServiceOfSurroundingServerModal.hide();
    updateDiagram();
  });
  cancelRemoveBtn.addEventListener("click", function () {
    confirmModal.hide();
  });

  cancelServiceOfSurroundingServerBtn.addEventListener("click", function () {
    updateServiceOfSurroundingServerModal.hide();
  });

  cancelRemoveSurroundingBtn.addEventListener("click", function () {
    confirmSurroundingModal.hide();
  });

  cancelRemoveServiceSurroundingBtn.addEventListener("click", function () {
    confirmServiceSurroundingModal.hide();
  });

  addServerBtn.addEventListener("click", function () {
    renderError();
    servers.push({
      server_name: "",
      ip_address: "",
      os: "",
      cpu: "",
      ram: "",
      memory: "",
      installed_apps: "",
      surrounding_servers: [],
    });
    updateDiagram();
  });

  generateBtn.addEventListener("click", function () {
    const applicationName = document.getElementById("application_name").value;
    const dataObject = {
      data: [
        {
          application_name: applicationName,
          servers: servers,
          departement: {
            id: uuidDepartment,
          },
        },
      ],
    };
    const err = validateForm(dataObject);
    if (err.isValid) updateDiagram();
    else {
      errorMessage(err.error);
    }
  });

  function errorMessage(msg) {
    Swal.fire({
      text: msg,
      icon: "error",
      buttonsStyling: false,
      confirmButtonText: "OK",
      customClass: {
        confirmButton: "btn btn-danger",
      },
    });
  }

  saveBtn.addEventListener("click", function () {
    const applicationName = document.getElementById("application_name").value;
    if (applicationName == "") {
      Swal.fire({
        text: `${"Application name is required"}`,
        icon: "error",
        buttonsStyling: false,
        confirmButtonText: "OK",
        customClass: {
          confirmButton: "btn btn-primary",
        },
      });
      return;
    }
    const dataObject = {
      application_name: applicationName,
      servers: servers,
      departement: {
        id: uuidDepartment,
      },
    };
    const err = validateForm({
      data: [{ ...dataObject }],
    });
    if (err.isValid) {
      if (pathUrl === "edit") {
        updateAppById(appId, dataObject);
      } else postDiagram(dataObject);
    } else {
      errorMessage(err.error);
    }
  });

  function updateDiagram() {
    renderTable(getTableData());
    const applicationName = document.getElementById("application_name").value;
    const dataObject = {
      data: [
        {
          application_name: applicationName,
          servers: servers,
          departement: {
            id: uuidDepartment,
          },
        },
      ],
    };
    const checkForm = validateForm(dataObject);
    if (checkForm.isValid) generateTopology(dataObject);
  }
  let isView = false;
  window.openViewModal = function (...args) {
    isView = true;
    openServerModal(...args);
  };

  window.openServerModal_ = function (...args) {
    isView = false;
    openServerModal(...args);
  };

  function openServerModal(...args) {
    const type = args[0];
    const serverIndex = args[1];
    const ssIndex = args[2];
    const inputServerNameLabel = document.getElementById(
      "inputServerNameLabel"
    );

    const inputServerIPLabel = document.getElementById("inputServerIPLabel");

    // Selecting input elements
    const inputServerName = document.getElementById("inputServerName");
    const inputServerIP = document.getElementById("inputServerIP");
    const inputOSServer = document.getElementById("inputOSServer");
    const inputCPUServer = document.getElementById("inputCPUServer");
    const inputRamServer = document.getElementById("inputRamServer");
    const inputMemoryServer = document.getElementById("inputMemoryServer");
    const divSelecServerType = document.getElementById("divSelecServerType");

    // Selecting textarea element
    const textareaAppServer = document.getElementById("inputAppServer");

    // Selecting select element
    const selectServerType = document.getElementById("inputServerType");
    const inputSurroundingServerTypeLabel = document.getElementById(
      "inputSurroundingServerTypeLabel"
    );

    const inputElements = [
      inputServerName,
      inputServerIP,
      inputOSServer,
      inputCPUServer,
      inputRamServer,
      inputMemoryServer,
      textareaAppServer,
      selectServerType,
    ];
    inputElements.forEach((inputElement) => {
      inputElement.classList.remove("is-invalid");
      inputElement.classList.remove("is-valid");
      if (isView) {
        inputElement.readOnly = true;
        selectServerType.setAttribute("disabled", "disabled");
      } else {
        inputElement.readOnly = false;
        selectServerType.removeAttribute("disabled");
      }
    });

    if (isView) {
      document.getElementById("saveServerInfoBtn").classList.add("d-none");
      document.getElementById("cancleInputServerModal").innerHTML = "Done";
    }

    switch (type) {
      case "server":
        serverType = "server";
        currentIndexToRemove = serverIndex;
        inputServerNameLabel.innerHTML = "Server Name";
        inputServerIPLabel.innerHTML = "Server IP";
        inputServerName.value = servers[serverIndex].server_name;
        inputServerIP.value = servers[serverIndex].ip_address;
        inputOSServer.value = servers[serverIndex].os;
        inputCPUServer.value = servers[serverIndex].cpu;
        inputRamServer.value = servers[serverIndex].ram;
        inputMemoryServer.value = servers[serverIndex].memory;
        textareaAppServer.value = servers[serverIndex].installed_apps;
        divSelecServerType.childNodes.forEach((child) => {
          if (child.nodeType === 1) {
            child.classList.add("d-none");
          }
        });
        inputServerModal.show();
        break;
      case "surrounding_server":
        serverType = "surrounding_server";
        currentSurroundingIndexToRemove = {
          serverIndex,
          sIndex: ssIndex,
        };
        inputServerNameLabel.innerHTML = "Surrounding Server Name";
        inputServerIPLabel.innerHTML = "Surrounding Server IP";
        inputServerName.value =
          servers[serverIndex].surrounding_servers[
            ssIndex
          ].surrounding_server_name;
        inputServerIP.value =
          servers[serverIndex].surrounding_servers[ssIndex].ip_address;
        inputOSServer.value =
          servers[serverIndex].surrounding_servers[ssIndex].os;
        inputCPUServer.value =
          servers[serverIndex].surrounding_servers[ssIndex].cpu;
        inputRamServer.value =
          servers[serverIndex].surrounding_servers[ssIndex].ram;
        inputMemoryServer.value =
          servers[serverIndex].surrounding_servers[ssIndex].memory;
        textareaAppServer.value =
          servers[serverIndex].surrounding_servers[ssIndex].installed_apps;
        selectServerType.value =
          servers[serverIndex].surrounding_servers[ssIndex].type;
        divSelecServerType.childNodes.forEach((child) => {
          if (child.nodeType === 1) {
            child.classList.remove("d-none");
          }
        });
        inputServerModal.show();
        break;
      default:
        return;
    }
  }

  // Example starter JavaScript for disabling form submissions if there are invalid fields
  (function () {
    "use strict";

    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.querySelectorAll(".needs-validation");

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms).forEach(function (form) {
      form.addEventListener(
        "submit",
        function (event) {
          // Check if the form inputs are valid
          event.preventDefault();
          // event.stopPropagation();
          const ip = $("#inputServerIP").val();
          form.classList.remove("was-validated");

          const inputServerIP = form.querySelector("#inputServerIP");
          const inputServerName = form.querySelector("#inputServerName");
          const inputOSServer = form.querySelector("#inputOSServer");
          const inputCPUServer = form.querySelector("#inputCPUServer");
          const inputRamServer = form.querySelector("#inputRamServer");
          const inputMemoryServer = form.querySelector("#inputMemoryServer");
          const textareaAppServer = form.querySelector("#inputAppServer");
          const selectServerType = form.querySelector("#inputServerType");
          const inputElements = [
            inputServerName,
            inputOSServer,
            inputCPUServer,
            inputRamServer,
            inputMemoryServer,
            textareaAppServer,
            selectServerType,
          ];
          if (!validateIP(ip)) {
            inputServerIP.classList.add("is-invalid");
            inputServerIP.classList.remove("is-valid");
          } else {
            inputServerIP.classList.remove("is-invalid");
            inputServerIP.classList.add("is-valid");
          }
          inputElements.forEach((inputElement, _index) => {
            const value = inputElement.value;

            if (value && value !== null && value !== "") {
              inputElement.classList.remove("is-invalid");
              inputElement.classList.add("is-valid");
            } else {
              inputElement.classList.add("is-invalid");
              inputElement.classList.remove("is-valid");
            }
          });
          let checkElementForm = [...inputElements, inputServerIP];
          if (serverType === "server") {
            checkElementForm = checkElementForm.filter(
              (element) => element !== selectServerType
            );
          }

          if (isHasIsValid(checkElementForm)) {
            const serverData = {
              server_name: inputServerName.value,
              ip_address: inputServerIP.value,
              os: inputOSServer.value,
              cpu: inputCPUServer.value,
              ram: inputRamServer.value,
              memory: inputMemoryServer.value,
              installed_apps: textareaAppServer.value,
            };
            if (serverType === "server") {
              servers[currentIndexToRemove] = {
                ...servers[currentIndexToRemove],
                ...serverData,
              };
            } else if (serverType === "surrounding_server") {
              const { serverIndex, sIndex } = currentSurroundingIndexToRemove;
              const oldData = servers[serverIndex].surrounding_servers[sIndex];
              if (selectServerType.value == "db") {
                oldData.services = [];
              }
              servers[serverIndex].surrounding_servers[sIndex] = {
                ...oldData,
                ...serverData,
                surrounding_server_name: serverData.server_name,
                type: selectServerType.value,
              };
            }
            updateDiagram();
            inputServerModal.hide();
          }
        },
        false
      );
    });
  })();

  function isHasIsValid(elements) {
    let count = 0;
    for (let i = 0; i < elements.length; i++) {
      if (elements[i].classList.contains("is-valid")) {
        count++;
      }
    }
    return count === elements.length;
  }

  (function () {
    switch (pathUrl) {
      case "view":
        addServerBtn.classList.add("d-none");
        generateBtn.classList.add("d-none");
        saveBtn.classList.add("d-none");
        break;

      default:
        break;
    }
  })();
});
