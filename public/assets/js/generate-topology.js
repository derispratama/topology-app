import mermaid from "http://127.0.0.1:8000/node_modules/mermaid/dist/mermaid.esm.min.mjs";
import { getToken } from "./auth.js";
const API_URL = getEnv();
const id = getId();

mermaid.initialize({ startOnLoad: false, theme: "default" });

// Example of using the render function
const drawDiagram = async function (graphDefinition) {
  const element = document.querySelector("#graphDiv");
  const { svg } = await mermaid.render("graphDiv", graphDefinition);
  element.innerHTML = svg;
  $("#topology").html(svg);
};

function getDBTopology(arr, alias, ip_address, name) {
  const { length } = arr;
  const found = arr.filter((el) => el.ip_address === ip_address);
  if (found.length == 0)
    arr.push({ alias: alias, ip_address: ip_address, name: name });

  if (found.length == 0) {
    return `-->${alias}[(${name}\n${ip_address})]\n`;
  } else {
    return `-->${found[0].alias}[(${found[0].name}\n${found[0].ip_address})]\n`;
  }
}

function getSurroundingServer(arr, alias, ip_address, name, lb_sr_servers) {
  const { length } = arr;
  if (lb_sr_servers != "" && lb_sr_servers != null) {
    const found = arr.filter(
      (el) => el.ip_address === lb_sr_servers.ip_address
    );
    if (found.length == 0) {
      return `-->${alias}[${name}\n${ip_address}]`;
    } else {
      return `${found[0].alias}[${found[0].name}\n${found[0].ip_address}]-->${alias}[${name}\n${ip_address}]\n`;
    }
  } else {
    return `-->${alias}[${name}\n${ip_address}]`;
  }
}

export const generateTopology = async (res) => {
  const arrChar = [
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
  ];
  const arrLB = [
    "LBA",
    "LBB",
    "LBC",
    "LBD",
    "LBE",
    "LBF",
    "LBG",
    "LBH",
    "LBI",
    "LBJ",
    "LBK",
    "LBL",
    "LBM",
    "LBN",
  ];

  const arrLBSR = [
    "LBSRA",
    "LBSRB",
    "LBSRC",
    "LBSRD",
    "LBSRE",
    "LBSRF",
    "LBSRG",
    "LBSRH",
    "LBSRI",
    "LBSRJ",
    "LBSRK",
    "LBSRL",
    "LBSRM",
    "LBSRN",
  ];

  const arrLBSV = [
    "LBSVA",
    "LBSVB",
    "LBSVC",
    "LBSVD",
    "LBSVE",
    "LBSVF",
    "LBSVG",
    "LBSVH",
    "LBSVI",
    "LBSVJ",
    "LBSVK",
    "LBSVL",
    "LBSVM",
    "LBSVN",
  ];

  const arrChar2 = [
    "AA",
    "BB",
    "CC",
    "DD",
    "EE",
    "FF",
    "GG",
    "HH",
    "II",
    "JJ",
    "KK",
    "LL",
    "MM",
    "NN",
  ];
  const arrChar3 = [
    "AAA",
    "BBB",
    "CCC",
    "DDD",
    "EEE",
    "FFF",
    "GGG",
    "HHH",
    "III",
    "JJJ",
    "KKK",
    "LLL",
    "MMM",
    "NNN",
  ];
  let str = `flowchart TB`;
  const server_length = res.data[0].servers.length;

  let z = 0;
  let zLB = 0;
  let x = 0;
  let xLb = 0;
  let yLb = 0;

  let arrTempDB = [];
  let arrTempLB = [];
  for (let i = 0; i < server_length; i++) {
    if (
      res.data[0].ip_load_balancer == null ||
      res.data[0].ip_load_balancer == ""
    ) {
      str += `\nAPP[${res.data[0].application_name}]`;
    } else {
      if (i == 0) {
        str += `\nAPP[${res.data[0].application_name}]-->`;
      }
      str += `LBAPP[Load Balancer\n${res.data[0].ip_load_balancer}]`;
    }

    const surrounding_servers_length =
      res.data[0].servers[i].surrounding_servers.length;
    const server_name = res.data[0].servers[i].server_name;
    const server_ip_address = res.data[0].servers[i].ip_address;

    if (surrounding_servers_length == 0) {
      str += `-->${arrChar[i]}[[<i class="fa fa-desktop" style="color:black;font-size:25px;"></i>\n ${server_name}\n${server_ip_address}]]`;
      str += "\n";
    }

    let b = 0;
    for (let j = 0; j < surrounding_servers_length; j++) {
      const surrounding_server_name =
        res.data[0].servers[i].surrounding_servers[j].surrounding_server_name;
      const surrounding_server_ip_address =
        res.data[0].servers[i].surrounding_servers[j].ip_address;
      const type = res.data[0].servers[i].surrounding_servers[j].type;
      const id_lb = res.data[0].servers[i].surrounding_servers[j].id_lb;

      str += `${j == 0 ? "-->" : ""} ${
        arrChar[i]
      }[[<i class="fa fa-desktop" style="color:black;font-size:25px;"></i>\n ${server_name}\n${server_ip_address}]]\n`;

      if (id_lb == null) {
        if (type == "app") {
          str += `-->${arrChar2[z]}[${surrounding_server_name}\n${surrounding_server_ip_address}]`;
        } else if (type == "db") {
          // str += getDBTopology(arrTempDB,arrChar2[z],surrounding_server_ip_address,surrounding_server_name);
          str += `-->${arrChar2[z]}[(${surrounding_server_name}\n${surrounding_server_ip_address})]\n`;
        }

        str += j == surrounding_servers_length - 1 ? "" : "\n";

        const services_length =
          res.data[0].servers[i].surrounding_servers[j].services.length;

        if (services_length == 0) {
          str += "\n";
        }

        for (let k = 0; k < services_length; k++) {
          str += `\n${arrChar2[z]}[${surrounding_server_name}\n${surrounding_server_ip_address}]`;

          const service_name =
            res.data[0].servers[i].surrounding_servers[j].services[k]
              .service_name;
          const service_port =
            res.data[0].servers[i].surrounding_servers[j].services[k].port;

          str += `-->|${service_port}|${arrChar3[x]}([${service_name}])`;
          str += "\n";

          x++;
        }
        z++;
      }
    }
  }

  for (let i = 0; i < server_length; i++) {
    const server_name = res.data[0].servers[i].server_name;
    const server_ip_address = res.data[0].servers[i].ip_address;
    const lb_servers_length = res.data[0].servers[i].lb_servers.length;
    const surrounding_servers_length =
      res.data[0].servers[i].surrounding_servers.length;

    if (surrounding_servers_length == 0) {
      str += "\n";
    }

    let b = 0;

    for (let j = 0; j < lb_servers_length; j++) {
      const lb_name = res.data[0].servers[i].lb_servers[j].name;
      const lb_ip_address = res.data[0].servers[i].lb_servers[j].ip_address;
      const lb_surrounding_servers_length =
        res.data[0].servers[i].lb_servers[j].surrounding_servers.length;

      str += `${arrChar[i]}[[<i class="fa fa-desktop" style="color:black;font-size:25px;"></i>\n ${server_name}\n${server_ip_address}]]\n`;

      str += `-->${arrLB[zLB]}[${lb_name}\n${lb_ip_address}]`;

      if (lb_surrounding_servers_length == 0) {
        str += "\n";
      }

      for (let s = 0; s < lb_surrounding_servers_length; s++) {
        const lb_surrounding_server_name =
          res.data[0].servers[i].lb_servers[j].surrounding_servers[s]
            .surrounding_server_name;
        const lb_surrounding_server_ip_address =
          res.data[0].servers[i].lb_servers[j].surrounding_servers[s]
            .ip_address;
        const lb_sr_type =
          res.data[0].servers[i].lb_servers[j].surrounding_servers[s].type;
        const lb_id_lb =
          res.data[0].servers[i].lb_servers[j].surrounding_servers[s].id_lb;

        str += `\n${arrLB[zLB]}[${lb_name}\n${lb_ip_address}]`;

        str += `-->${arrLBSR[xLb]}[${lb_surrounding_server_name}\n${lb_surrounding_server_ip_address}]`;

        const services_length =
          res.data[0].servers[i].lb_servers[j].surrounding_servers[s].services
            .length;

        if (services_length == 0) {
          str += "\n";
        }

        for (let k = 0; k < services_length; k++) {
          str += `\n${arrLBSR[xLb]}[${lb_surrounding_server_name}\n${lb_surrounding_server_ip_address}]`;

          const service_name =
            res.data[0].servers[i].lb_servers[j].surrounding_servers[s]
              .services[k].service_name;
          const service_port =
            res.data[0].servers[i].lb_servers[j].surrounding_servers[s]
              .services[k].port;

          str += `-->|${service_port}|${arrLBSV[yLb]}([${service_name}])`;
          str += "\n";

          yLb++;
        }
        xLb++;
      }
      zLB++;
    }
  }

  // console.log(str);
  await drawDiagram(str);
};

// if (id && id !== "") {
//   // Define an async function to get data and update the UI
//   const getData = async () => {
//     try {
//       const data = await getDiagramById(id);
//       //
//       // Update UI elements
//       $("#application_name").val(data.data[0].application_name);
//       $("#departement").val(data.data[0].departement.departement_name);

//       // Generate topology
//       generateTopology(data);
//     } catch (error) {
//       console.error("Error fetching data:", error);
//     }
//   };

//   // Call the getData function
//   getData();
// }

// Define the getDiagramById function
export async function getDiagramById(id) {
  try {
    const response = await fetch(`${API_URL}/api/applications/${id}`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    });
    if (!response.ok) {
      throw new Error("Network response was not ok");
    }
    const result = await response.json();
    return result;
  } catch (error) {
    console.error("There was a problem with the fetch operation:", error);
    throw error;
  }
}
