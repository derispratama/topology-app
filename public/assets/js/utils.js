export function validateIP(ip) {
  const ipPattern =
    /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
  return ipPattern.test(ip);
}

export function validateForm(formData) {
  const { application_name, servers, departement } = formData.data[0];
  // Validate application_name (assuming it should not be empty)
  if (!application_name.trim()) {
    return { isValid: false, error: "Application name cannot be empty" };
  }

  // Validate each server in the servers array
  let sIndex = 0;
  for (const server of servers) {
    sIndex++;
    // Validate server_name
    if (!server.server_name.trim()) {
      return {
        isValid: false,
        error: `Server name cannot be empty (index: ${sIndex})`,
      };
    }

    // Validate ip_address
    if (!validateIP(server.ip_address)) {
      return {
        isValid: false,
        error: `Invalid IP Address: ${server.ip_address} (index: ${sIndex})`,
      };
    }

    // Validate cpu, ram, and memory (assuming they should not be empty)
    if (!server.cpu.trim() || !server.ram.trim() || !server.memory.trim()) {
      return {
        isValid: false,
        error: `CPU, RAM, and Memory cannot be empty (index: ${sIndex})`,
      };
    }
    let ssIndex = 0;
    // Validate surrounding_servers array
    for (const surroundingServer of server.surrounding_servers) {
      ssIndex++;
      // Validate surrounding_server_name
      if (!surroundingServer.surrounding_server_name.trim()) {
        return {
          isValid: false,
          error: `Surrounding server name cannot be empty (index: ${sIndex}.${ssIndex})`,
        };
      }

      // Validate ip_address of surrounding server
      if (!validateIP(surroundingServer.ip_address)) {
        return {
          isValid: false,
          error: `Invalid IP Address for surrounding server: ${surroundingServer.ip_address} (index: ${sIndex}.${ssIndex})`,
        };
      }
      let serviceIndex = 0;
      // Validate services array
      for (const service of surroundingServer.services) {
        serviceIndex++;
        // Validate service_name and port (assuming they should not be empty)
        if (!service.service_name.trim() || !service.port.trim()) {
          return {
            isValid: false,
            error: `Service name and port cannot be empty \n (index: ${sIndex}.${ssIndex}.${serviceIndex})`,
          };
        }

        if (!service.url.trim()) {
          return {
            isValid: false,
            error: `Service url cannot be empty \n(index: ${sIndex}.${ssIndex}.${serviceIndex})`,
          };
        }

        if (!service.params_request.trim() || !service.params_response.trim()) {
          return {
            isValid: false,
            error: `Service request params and response params cannot be empty \n(index: ${sIndex}.${ssIndex}.${serviceIndex})`,
          };
        }
      }
    }
  }

  // Validate department ID (assuming it should exist)
  if (!departement || !departement.id) {
    return { isValid: false, error: "Department ID is required" };
  }

  // If all validations pass
  return { isValid: true };
}

export const privateRoute = (user) => {
  if (user && user.roles.role_name !== "superadmin") {
    window.location.href = "/application";
  }
};

export const logoutApp = () => {
  Swal.fire({
    html: `Are you sure you want to logout?`,
    icon: "warning",
    buttonsStyling: false,
    showCancelButton: true,
    confirmButtonText: "Yes",
    cancelButtonText: "No",
    customClass: {
      confirmButton: "btn btn-primary",
      cancelButton: "btn btn-active-light-primary",
    },
  }).then((result) => {
    if (result.isConfirmed) {
      localStorage.removeItem("token");
      localStorage.removeItem("user");
      window.location.href = "/";
    }
  });
};
