import { getToken } from "../auth.js";
import { privateRoute } from "../utils.js";
import {
  createDepartment,
  deleteDepartmentById,
  getDepartmens,
  updateDepartmentByIdApi,
} from "./api.js";
import { departmentValidator } from "./validator.js";

document.addEventListener("DOMContentLoaded", function () {
  let user = JSON.parse(localStorage.getItem("user"));

  privateRoute(user);

  // Initialize
  const token = getToken();
  let departmentId = "";
  let actionStatus = "create";

  // Button Definitions
  const addDepartmentBtn = document.getElementById("addDepartmentBtn");
  const submitButton = document.getElementById("submitBtn");
  const deleteDepartmentBtn = document.getElementById("deleteDepartmentBtn");

  // Modal definitions
  const departmentModal = new bootstrap.Modal(
    document.getElementById("add_department_modal")
  );
  const deleteDepartmentModal = new bootstrap.Modal(
    document.getElementById("delete_department_modal")
  );

  // Form Definitions
  const form = document.getElementById("department_form");
  const inputDepartmentName = document.getElementById("inputDepartmentName");

  // Listener handler
  addDepartmentBtn.addEventListener("click", () => {
    actionStatus = "create";
    inputDepartmentName.value = "";
    departmentModal.show();
  });

  deleteDepartmentBtn.addEventListener("click", () => {
    deleteDepartmentModal.hide();
    deleteDepartmentById({
      token,
      departmentId,
    });
  });

  // Window Function
  window.deleteDepartmentById = (id) => {
    departmentId = id;
    deleteDepartmentModal.show();
  };

  window.updateDepartmentById = (id, departmentName) => {
    actionStatus = "update";
    departmentId = id;
    inputDepartmentName.value = departmentName;
    departmentModal.show();
  };

  //Validator
  const validator = departmentValidator(form);

  submitButton.addEventListener("click", function (e) {
    e.preventDefault();

    if (validator) {
      validator.validate().then(function (status) {
        if (status === "Valid") {
          submitButton.setAttribute("data-kt-indicator", "on");
          submitButton.disabled = true;
          switch (actionStatus) {
            case "create":
              createDepartment({
                token,
                departement_name: inputDepartmentName.value,
              }).then(() => {
                submitButton.disabled = false;
                submitButton.removeAttribute("data-kt-indicator");
                departmentModal.hide();
              });
              break;
            case "update":
              updateDepartmentByIdApi({
                token,
                id: departmentId,
                name: inputDepartmentName.value,
              }).then(() => {
                submitButton.disabled = false;
                submitButton.removeAttribute("data-kt-indicator");
                departmentModal.hide();
              });
              break;

            default:
              break;
          }
        }
      });
    }
  });

  //first load
  getDepartmens({ token });
});
