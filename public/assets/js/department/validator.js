export const departmentValidator = (form) =>
  FormValidation.formValidation(form, {
    fields: {
      inputDepartmentName: {
        validators: {
          notEmpty: {
            message: "Department Name is required",
          },
        },
      },
    },

    plugins: {
      trigger: new FormValidation.plugins.Trigger(),
      bootstrap: new FormValidation.plugins.Bootstrap5({
        rowSelector: ".fv-row",
        eleInvalidClass: "",
        eleValidClass: "",
      }),
    },
  });
