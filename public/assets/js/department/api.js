export function getDepartmens({ token }) {
  $.ajax({
    url: "/api/departements",
    method: "GET",
    headers: {
      Authorization: `Bearer ${token}`,
    },
    dataType: "json",
    beforeSend(res) {},
    complete(res) {},
    error(jqXHR, textStatus, errorThrown) {
      if (jqXHR.status === 401) {
        unautorizedAlert();
      } else {
        console.log("Error:", errorThrown);
      }
    },
    success(res) {
      let no = 1;
      if ($.fn.DataTable.isDataTable("#kt_datatable_dom_positioning")) {
        $("#kt_datatable_dom_positioning").DataTable().destroy();
      }

      // Clear the table body
      $("#kt_datatable_dom_positioning tbody").empty();
      const html = res.data.map((v, i) => {
        v.users.length;
        return `<tr>
                    <td>${no++}</td>
                    <td>${v.departement_name}</td>
                    <td>${v.users.length}</td>
                    <td>
                        <button class="btn btn-sm btn-primary" onclick="updateDepartmentById('${
                          v.id
                        }','${v.departement_name}')">Edit</button>
                    <button class="btn btn-sm btn-danger" ${
                      v.users.length > 0 ? "disabled" : ""
                    } onclick="deleteDepartmentById('${v.id}')">delete</button>
                    </td>
                </tr>`;
      });
      $("#kt_datatable_dom_positioning tbody").html(html);

      $("#kt_datatable_dom_positioning").DataTable({
        language: {
          lengthMenu: "Show _MENU_",
        },
        dom:
          "<'row mb-2'" +
          "<'col-sm-6 d-flex align-items-center justify-conten-start dt-toolbar'l>" +
          "<'col-sm-6 d-flex align-items-center justify-content-end dt-toolbar'f>" +
          ">" +
          "<'table-responsive'tr>" +
          "<'row'" +
          "<'col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start'i>" +
          "<'col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end'p>" +
          ">",
      });
    },
  });
}

export async function createDepartment({ token, departement_name }) {
  try {
    const requestOptions = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        accept: "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        departement_name,
      }),
      redirect: "follow",
    };

    const response = await fetch("/api/departements", requestOptions);

    if (!response.ok) {
      const errorData = await response.json();
      throw new Error(`Error: ${response.status} - ${errorData.message}`);
    }

    const data = await response.json();
    Swal.fire({
      text: "Form has been successfully submitted!",
      icon: "success",
      buttonsStyling: false,
      confirmButtonText: "Ok, got it!",
      customClass: {
        confirmButton: "btn btn-primary",
      },
    }).then(() => {
      getDepartmens({ token });
    });
    return data;
  } catch (error) {
    console.log("Error:", error.message);
    Swal.fire({
      text: "Failed to create department.",
      icon: "error",
      buttonsStyling: false,
      confirmButtonText: "Ok, got it!",
      customClass: {
        confirmButton: "btn btn-primary",
      },
    });
  }
}

export async function deleteDepartmentById({ token, departmentId }) {
  const myHeaders = new Headers();
  myHeaders.append("Authorization", `Bearer ${token}`);

  const requestOptions = {
    method: "DELETE",
    headers: myHeaders,
    redirect: "follow",
  };

  try {
    const response = await fetch(
      `/api/departements/${departmentId}`,
      requestOptions
    );

    if (!response.ok) {
      const errorData = await response.json();
      throw new Error(`Error: ${response.status} - ${errorData.message}`);
    }

    await response.text();
    Swal.fire({
      text: "Department has been successfully deleted!",
      icon: "success",
      buttonsStyling: false,
      confirmButtonText: "Ok, got it!",
      customClass: {
        confirmButton: "btn btn-primary",
      },
    });
    getDepartmens({ token });
  } catch (error) {
    console.error("Error:", error.message);
    Swal.fire({
      text: "Failed to delete department.",
      icon: "error",
      buttonsStyling: false,
      confirmButtonText: "Ok, got it!",
      customClass: {
        confirmButton: "btn btn-primary",
      },
    });
  }
}

export const updateDepartmentByIdApi = async ({ token, id, name }) => {
  const myHeaders = new Headers();
  myHeaders.append("Authorization", `Bearer ${token}`);
  myHeaders.append("Content-Type", "application/json");

  const requestOptions = {
    method: "PUT",
    headers: myHeaders,
    body: JSON.stringify({ departement_name: name }),
    redirect: "follow",
  };

  try {
    const response = await fetch(`/api/departements/${id}`, requestOptions);

    if (!response.ok) {
      const errorData = await response.json();
      throw new Error(`Error: ${response.status} - ${errorData.message}`);
    }

    await response.json();
    Swal.fire({
      text: "Department has been successfully updated!",
      icon: "success",
      buttonsStyling: false,
      confirmButtonText: "Ok, got it!",
      customClass: {
        confirmButton: "btn btn-primary",
      },
    });

    // Optionally, refresh the departments list
    getDepartmens({ token });
  } catch (error) {
    console.error("Error:", error.message);
    Swal.fire({
      text: "Failed to update department.",
      icon: "error",
      buttonsStyling: false,
      confirmButtonText: "Ok, got it!",
      customClass: {
        confirmButton: "btn btn-primary",
      },
    });
  }
};
