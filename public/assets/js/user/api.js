import { unautorizedAlert } from "../auth.js";

export function getUsers({ token }) {
  const user = JSON.parse(localStorage.getItem("user"));
  $.ajax({
    url: "/api/users",
    method: "GET",
    headers: {
      Authorization: `Bearer ${token}`,
    },
    dataType: "json",
    beforeSend(res) {},
    complete(res) {},
    error(jqXHR, textStatus, errorThrown) {
      if (jqXHR.status === 401) {
        unautorizedAlert();
      } else {
        console.log("Error:", errorThrown);
      }
    },
    success(res) {
      let no = 1;
      if ($.fn.DataTable.isDataTable("#kt_datatable_dom_positioning")) {
        $("#kt_datatable_dom_positioning").DataTable().destroy();
      }

      // Clear the table body
      $("#kt_datatable_dom_positioning tbody").empty();
      const html = res.data.map((v, i) => {
        const vString = JSON.stringify(v)
          .replace(/\\/g, "\\\\")
          .replace(/'/g, "\\'")
          .replace(/"/g, "&quot;");
        const isSuperadmin = v.username === "superadmin" ? true : false;
        return `<tr>
                      <td>${no++}</td>
                      <td>${v.name}</td>
                      <td>${v.roles.role_name}</td>
                      <td>${v.department.departement_name}</td>
                      <td>
                          <button class="btn btn-sm btn-primary"
                          ${
                            isSuperadmin && user.username !== "superadmin"
                              ? "disabled"
                              : ""
                          }
                          onclick="updateUserById('${
                            v.id
                          }','${vString}')">Edit</button>
                      <button class="btn btn-sm btn-danger" ${
                        user.username === v.username || isSuperadmin
                          ? "disabled"
                          : ""
                      } onclick="deleteUserById('${v.id}')">delete</button>
                      </td>
                  </tr>`;
      });
      $("#kt_datatable_dom_positioning tbody").html(html);

      $("#kt_datatable_dom_positioning").DataTable({
        language: {
          lengthMenu: "Show _MENU_",
        },
        dom:
          "<'row mb-2'" +
          "<'col-sm-6 d-flex align-items-center justify-conten-start dt-toolbar'l>" +
          "<'col-sm-6 d-flex align-items-center justify-content-end dt-toolbar'f>" +
          ">" +
          "<'table-responsive'tr>" +
          "<'row'" +
          "<'col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start'i>" +
          "<'col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end'p>" +
          ">",
      });
    },
  });
}

export const getRoleApi = async ({ token }) => {
  try {
    const response = await fetch(`/api/roles`, {
      headers: {
        "Content-Type": "application/json",
        accept: "application/json",
        Authorization: `Bearer ${token}`,
      },
    });

    if (!response.ok) {
      throw new Error("Failed to fetch roles");
    }

    const data = await response.json();
    return data;
  } catch (error) {
    console.log("e");
  }
};

export const getDepartmentsApi = async ({ token }) => {
  try {
    const response = await fetch(`/api/departements`, {
      headers: {
        "Content-Type": "application/json",
        accept: "application/json",
        Authorization: `Bearer ${token}`,
      },
    });

    if (!response.ok) {
      throw new Error("Failed to fetch departments");
    }

    const data = await response.json();
    return data;
  } catch (error) {
    console.log("e");
  }
};

export const createUserApi = async ({ token, data }) => {
  try {
    const response = await fetch(`/api/users`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        accept: "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(data),
    });

    if (!response.ok) {
      throw new Error("Failed to create user");
    }

    const result = await response.json();
    Swal.fire({
      text: `User has been successfully created!`,
      icon: "success",
      buttonsStyling: false,
      confirmButtonText: "OK",
      customClass: {
        confirmButton: "btn btn-primary",
      },
    });
    return result;
  } catch (error) {
    Swal.fire({
      text: `${error.message}`,
      icon: "error",
      buttonsStyling: false,
      confirmButtonText: "OK",
      customClass: {
        confirmButton: "btn btn-danger",
      },
    });
  }
};

export const deleteUserByIdApi = async ({ token, userId }) => {
  try {
    const response = await fetch(`/api/users/${userId}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        accept: "application/json",
        Authorization: `Bearer ${token}`,
      },
    });

    if (!response.ok) {
      throw new Error("Failed to delete user");
    }
    const result = await response.json();
    Swal.fire({
      text: `User has been successfully deleted!`,
      icon: "success",
      buttonsStyling: false,
      confirmButtonText: "OK",
      customClass: {
        confirmButton: "btn btn-primary",
      },
    });
    return result;
  } catch (error) {
    Swal.fire({
      text: `${error.message}`,
      icon: "error",
      buttonsStyling: false,
      confirmButtonText: "OK",
      customClass: {
        confirmButton: "btn btn-danger",
      },
    });
  }
};

export const updateUserByIdApi = async ({ token, userId, data }) => {
  try {
    const response = await fetch(`/api/users/${userId}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        accept: "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(data),
    });

    if (!response.ok) {
      const errResponse = await response.json();
      const errMsg = errResponse.messages || "Failed to update user";
      throw new Error(errMsg);
    }

    const result = await response.json();

    const swalResult = await Swal.fire({
      text: `User has been successfully updated!`,
      icon: "success",
      buttonsStyling: false,
      confirmButtonText: "OK",
      customClass: {
        confirmButton: "btn btn-primary",
      },
    });

    if (swalResult.isConfirmed) {
      return true;
    }

    return false;
  } catch (error) {
    console.log(error.message);
    await Swal.fire({
      text: `${error.message}`,
      icon: "error",
      buttonsStyling: false,
      confirmButtonText: "OK",
      customClass: {
        confirmButton: "btn btn-danger",
      },
    });
    return false;
  }
};
