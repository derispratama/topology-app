import { getToken } from "../auth.js";
import { privateRoute } from "../utils.js";
import {
  createUserApi,
  deleteUserByIdApi,
  getDepartmentsApi,
  getRoleApi,
  getUsers,
  updateUserByIdApi,
} from "./api.js";
import { userValidator } from "./validator.js";

document.addEventListener("DOMContentLoaded", function () {
  let user = JSON.parse(localStorage.getItem("user"));

  privateRoute(user);

  let actionStatus = "create";
  let isShowPassword = false;
  let userId = "";
  const token = getToken();
  let validate;
  getUsers({ token });

  //Button Definitions
  const addUserBtn = document.getElementById("addUserBtn");
  const saveUserBtn = document.getElementById("saveUserBtn");
  const showPasswordBtn = document.getElementById("showPasswordBtn");

  //Input Definitions
  const inputName = document.getElementById("inputName");
  const inputEmail = document.getElementById("inputEmail");
  const inputUsername = document.getElementById("inputUsername");
  const inputPassword = document.getElementById("inputPassword");
  const inputIsLdap = document.getElementById("inputIsLdap");
  const inputRole = document.getElementById("inputRole");
  const inputDepartment = document.getElementById("inputDepartment");

  const inputElement = [
    inputName,
    inputEmail,
    inputUsername,
    inputPassword,
    inputRole,
    inputDepartment,
  ];

  // Form Definitions
  const form = document.getElementById("user_form");

  //validator
  // const validator = userValidator(form, actionStatus);
  const resetForm = () => {
    inputElement.forEach((element) => {
      element.value = "";
    });
    if (validate) {
      validate.resetForm();
    }
    inputIsLdap.checked = false;
  };

  const toggleLoading = (isLoading) => {
    if (isLoading) {
      saveUserBtn.setAttribute("data-kt-indicator", "on");
      saveUserBtn.disabled = true;
    } else {
      saveUserBtn.removeAttribute("data-kt-indicator");
      saveUserBtn.disabled = false;
    }
  };

  const validateAndSubmit = async (validatorFn, apiFn, data) => {
    validatorFn.validate().then(async (status) => {
      if (status === "Valid") {
        toggleLoading(true);
        await apiFn({ token, userId, data });
        resetForm();
        getUsers({ token });
        toggleLoading(false);
        userModal.hide();
      }
    });
  };

  saveUserBtn.addEventListener("click", async (e) => {
    e.preventDefault();

    const data = {
      name: inputName.value,
      email: inputEmail.value,
      username: inputUsername.value,
      password: inputPassword.value,
      is_ldap: inputIsLdap.checked,
      id_role: inputRole.value,
      id_departement: inputDepartment.value,
    };
    validate = userValidator(form, actionStatus);

    if (actionStatus === "create") {
      validateAndSubmit(validate, createUserApi, data);
    } else if (actionStatus === "update") {
      data.password = inputPassword.value === "" ? null : inputPassword.value;
      validateAndSubmit(validate, updateUserByIdApi, data);
    }
  });

  //Modal Definitions
  const userModal = new bootstrap.Modal(document.getElementById("user_modal"));
  const confirmModal = new bootstrap.Modal(
    document.getElementById("confirm_alert_modal")
  );

  userModal._element.addEventListener("hidden.bs.modal", function () {
    // Perform any actions you want when the modal is closed
    resetForm(); // Reset the form when modal is closed
  });

  //Listener
  addUserBtn.addEventListener("click", async () => {
    actionStatus = "create";
    const roles = await getRoleApi({ token });
    const departments = await getDepartmentsApi({ token });
    // add select role and department roles.data[].role_name
    const roleOptions = roles.data.map((role) => {
      return `<option value="${role.id}">${role.role_name}</option>`;
    });

    const departmentOptions = departments.data.map((department) => {
      return `<option value="${department.id}">${department.departement_name}</option>`;
    });

    inputRole.innerHTML = roleOptions.join("");
    inputDepartment.innerHTML = departmentOptions.join("");
    userModal.show();
  });

  showPasswordBtn.addEventListener("click", () => {
    if (isShowPassword === false) {
      inputPassword.type = "text";
      showPasswordBtn.innerHTML = '<i class="bi bi-eye-slash-fill fs-2"></i>';
      isShowPassword = true;
    } else {
      inputPassword.type = "password";
      showPasswordBtn.innerHTML = '<i class="bi bi-eye-fill fs-2"></i>';
      isShowPassword = false;
    }
  });

  //
  window.deleteUser = async () => {
    await deleteUserByIdApi({ token, userId });
    getUsers({ token });
    confirmModal.hide();
  };

  window.deleteUserById = (id) => {
    userId = id;
    confirmModal.show();
  };

  window.updateUserById = async (id, userStr) => {
    const user = JSON.parse(userStr);
    actionStatus = "update";
    userId = id;
    const roles = await getRoleApi({ token });
    const departments = await getDepartmentsApi({ token });
    // add select role and department roles.data[].role_name
    const roleOptions = roles.data.map((role) => {
      return `<option value="${role.id}">${role.role_name}</option>`;
    });

    const departmentOptions = departments.data.map((department) => {
      return `<option value="${department.id}">${department.departement_name}</option>`;
    });

    inputName.value = user.name;
    inputEmail.value = user.email;
    inputUsername.value = user.username;
    inputIsLdap.checked = user.is_ldap;
    inputRole.innerHTML = roleOptions.join("");
    inputRole.value = user.id_role;
    inputDepartment.innerHTML = departmentOptions.join("");
    inputDepartment.value = user.id_departement;
    inputPassword.value = "";
    userModal.show();

    if (user.is_ldap) {
      inputIsLdap.checked = true;
    }
  };
});
