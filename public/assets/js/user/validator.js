let formValidationInstance = null;

export const userValidator = (form, actionStatus) => {
  const fields = {
    name: {
      validators: {
        notEmpty: {
          message: "Name is required",
        },
      },
    },
    email: {
      validators: {
        notEmpty: {
          message: "Email is required",
        },
        emailAddress: {
          message: "The value is not a valid email address",
        },
      },
    },
    username: {
      validators: {
        notEmpty: {
          message: "Username is required",
        },
      },
    },
    id_role: {
      validators: {
        notEmpty: {
          message: "Role is required",
        },
      },
    },
    id_departement: {
      validators: {
        notEmpty: {
          message: "Department is required",
        },
      },
    },
  };

  // Only add password validation during create
  if (actionStatus === "create") {
    fields.password = {
      validators: {
        notEmpty: {
          message: "Password is required",
        },
      },
    };
  }

  // Destroy the existing formValidationInstance if it exists
  if (formValidationInstance) {
    formValidationInstance.destroy();
  }

  // Initialize form validation with the new fields
  formValidationInstance = FormValidation.formValidation(form, {
    fields: fields,
    plugins: {
      trigger: new FormValidation.plugins.Trigger(),
      bootstrap: new FormValidation.plugins.Bootstrap5({
        rowSelector: ".fv-row",
        eleInvalidClass: "",
        eleValidClass: "",
      }),
    },
  });

  return formValidationInstance;
};
