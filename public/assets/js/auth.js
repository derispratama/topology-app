export function getToken() {
  const token = localStorage.getItem("token");
  if (!token) {
    unautorizedAlert();
  }

  return token;
}

export function unautorizedAlert() {
  Swal.fire({
    html: `Invalid token, please login!`,
    icon: "error",
    buttonsStyling: false,
    confirmButtonText: "Ok",
    customClass: {
      confirmButton: "btn btn-danger",
    },
  }).then((result) => {
    if (result.isConfirmed) {
      localStorage.removeItem("token");
      localStorage.removeItem("user");
      window.location.href = "/";
    }
  });
}
