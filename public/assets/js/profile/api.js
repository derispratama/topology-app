export const getUserByIdAPI = async ({ token, userId }) => {
  try {
    const response = await fetch(`/api/users/${userId}`, {
      headers: {
        "Content-Type": "application/json",
        accept: "application/json",
        Authorization: `Bearer ${token}`,
      },
    });
    if (!response.ok) {
      throw new Error("Failed to fetch user");
    }
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error);
  }
};
