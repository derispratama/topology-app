import { getToken } from "../auth.js";
import { logoutApp, privateRoute } from "../utils.js";
import {
  getDepartmentsApi,
  getRoleApi,
  getUsers,
  updateUserByIdApi,
} from "../user/api.js";
import { getUserByIdAPI } from "./api.js";
import { userValidator } from "../user/validator.js";

document.addEventListener("DOMContentLoaded", function () {
  const token = getToken();
  const user = JSON.parse(localStorage.getItem("user"));
  let isShowPassword = false;
  let validate;
  const userId = user.id;

  // Modal Definitions
  const userModal = new bootstrap.Modal(document.getElementById("user_modal"));

  // Form Definitions
  const form = document.getElementById("user_form");

  const inputName = document.getElementById("inputName");
  const inputEmail = document.getElementById("inputEmail");
  const inputUsername = document.getElementById("inputUsername");
  const inputPassword = document.getElementById("inputPassword");
  const inputIsLdap = document.getElementById("inputIsLdap");
  const inputRole = document.getElementById("inputRole");
  const inputDepartment = document.getElementById("inputDepartment");

  const inputElement = [
    inputName,
    inputEmail,
    inputUsername,
    inputPassword,
    inputRole,
    inputDepartment,
  ];

  //container definition
  const ldapContainer = document.getElementById("container-ldap");
  const roleContainer = document.getElementById("container-role");
  const departmentContainer = document.getElementById("container-department");

  ldapContainer.classList.add("d-none");
  roleContainer.classList.add("d-none");
  departmentContainer.classList.add("d-none");

  window.logout2 = () => {
    logoutApp();
  };

  // Button Definitions
  const editBtn = document.getElementById("editBtn");
  const saveUserBtn = document.getElementById("saveUserBtn");
  const showPasswordBtn = document.getElementById("showPasswordBtn");

  // Listener handler
  editBtn.addEventListener("click", async () => {
    const roles = await getRoleApi({ token });
    const departments = await getDepartmentsApi({ token });

    const userData = await getUserByIdAPI({ token, userId: user.id });
    // add select role and department roles.data[].role_name
    const roleOptions = roles.data.map((role) => {
      return `<option value="${role.id}">${role.role_name}</option>`;
    });

    const departmentOptions = departments.data.map((department) => {
      return `<option value="${department.id}">${department.departement_name}</option>`;
    });

    inputRole.innerHTML = roleOptions.join("");
    inputDepartment.innerHTML = departmentOptions.join("");

    inputName.value = userData.name;
    inputEmail.value = userData.email;
    inputUsername.value = userData.username;
    inputIsLdap.checked = userData.is_ldap;
    inputRole.innerHTML = roleOptions.join("");
    inputRole.value = userData.id_role;
    inputDepartment.innerHTML = departmentOptions.join("");
    inputDepartment.value = userData.id_departement;
    inputPassword.value = "";

    inputUsername.readOnly = true;
    userModal.show();
  });

  showPasswordBtn.addEventListener("click", () => {
    if (isShowPassword === false) {
      inputPassword.type = "text";
      showPasswordBtn.innerHTML = '<i class="bi bi-eye-slash-fill fs-2"></i>';
      isShowPassword = true;
    } else {
      inputPassword.type = "password";
      showPasswordBtn.innerHTML = '<i class="bi bi-eye-fill fs-2"></i>';
      isShowPassword = false;
    }
  });
  const toggleLoading = (isLoading) => {
    if (isLoading) {
      saveUserBtn.setAttribute("data-kt-indicator", "on");
      saveUserBtn.disabled = true;
    } else {
      saveUserBtn.removeAttribute("data-kt-indicator");
      saveUserBtn.disabled = false;
    }
  };

  const resetForm = () => {
    inputElement.forEach((element) => {
      element.value = "";
    });
    if (validate) {
      validate.resetForm();
    }
    inputIsLdap.checked = false;
  };

  const validateAndSubmit = async (validatorFn, apiFn, data) => {
    validatorFn.validate().then(async (status) => {
      if (status === "Valid") {
        toggleLoading(true);
        const success = await apiFn({ token, userId, data });
        toggleLoading(false);

        if (success) {
          const userData = await getUserByIdAPI({ token, userId: user.id });
          localStorage.setItem("user", JSON.stringify(userData));
          resetForm();
          userModal.hide();
          location.reload(); // Run location.reload() on success
        }
      }
    });
  };

  saveUserBtn.addEventListener("click", async (e) => {
    e.preventDefault();

    const data = {
      name: inputName.value,
      email: inputEmail.value,
      username: inputUsername.value,
      password: inputPassword.value,
      is_ldap: inputIsLdap.checked,
      id_role: inputRole.value,
      id_departement: inputDepartment.value,
    };
    validate = userValidator(form, "update");

    data.password = inputPassword.value === "" ? null : inputPassword.value;
    validateAndSubmit(validate, updateUserByIdApi, data);
  });
});
