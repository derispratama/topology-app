import { getToken } from "./auth.js";

export async function updateAppById(id, data) {
  try {
    const response = await fetch(`${getEnv()}/api/applications/${id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        accept: "application/json",
        Authorization: `Bearer ${getToken()}`,
      },
      body: JSON.stringify(data),
    });

    if (!response.ok) {
      throw Error("error");
    }
    Swal.fire({
      text: "Diagram has been save!",
      icon: "success",
      buttonsStyling: false,
      confirmButtonText: "Done",
      customClass: {
        confirmButton: "btn btn-primary",
      },
    }).then((result) => {
      if (result.isConfirmed) {
        window.location.href = "/application";
      }
    });
  } catch (error) {
    defaultErrorSwal();
  }
}

export async function postDiagram(data) {
  try {
    const response = await fetch(`${getEnv()}/api/applications`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        accept: "application/json",
        Authorization: `Bearer ${getToken()}`,
      },
      body: JSON.stringify(data),
    });
    if (!response.ok) {
      throw Error("error");
    }
    Swal.fire({
      text: "Diagram has been save!",
      icon: "success",
      buttonsStyling: false,
      confirmButtonText: "Done",
      customClass: {
        confirmButton: "btn btn-primary",
      },
    }).then((result) => {
      if (result.isConfirmed) {
        window.location.href = "/application";
      }
    });
  } catch (error) {
    defaultErrorSwal();
    console.log(error);
  }
}

export function defaultErrorSwal() {
  Swal.fire({
    text: `${"something went wrong"}`,
    icon: "error",
    buttonsStyling: false,
    confirmButtonText: "OK",
    customClass: {
      confirmButton: "btn btn-danger",
    },
  });
}
