import { generateTree } from "./render-tree.js";
import { generateTopology } from "../generate-topology.js";
import {
  loadBalancerValidator,
  serverValidator,
  serviceValidator,
  surroundingServerValidator,
} from "./validator.js";
import { getToken } from "../auth.js";
import { getAppByIdApi, postDiagram, updateAppById } from "./api.js";
document.addEventListener("DOMContentLoaded", async function () {
  const token = getToken();
  const user = JSON.parse(localStorage.getItem("user"));
  let surroundingType = "app";
  const pathType = getPathUrl();
  let isLB = false;
  const appId = getId();
  let currentServerIndex = 0;
  let currentSurroundingServerIndex = {
    serverIndex: 0,
    surroundingServerIndex: 0,
  };
  let currentServiceIndex = {
    serverIndex: 0,
    surroundingServerIndex: 0,
    serviceIndex: 0,
  };
  let currentLBIndex = {
    serverIndex: 0,
    lbIndex: 0,
  };
  let currentLBSRIndex = {
    serverIndex: 0,
    lbIndex: 0,
    lbSRIndex: 0,
  };

  let currentLBServiceIndex = {
    serverIndex: 0,
    lbIndex: 0,
    lbSRIndex: 0,
    lbServiceIndex: 0,
  };

  let serverAction = "add";
  let surroundingServerAction = "add";
  let serviceAction = "add";
  let lbAction = "add";
  let confirmAlertFor = "server";
  let dataObject = {
    application_name: "",
    ip_load_balancer: "",
    servers: [],
    departement: {
      id: null,
    },
  };
  dataObject.departement.id = user.id_departement;
  const topology = document.getElementById("topology-editor");
  const checkBoxLB = document.getElementById("checkbox_load_balancer");
  const inputApplicationName = document.getElementById("application_name");
  const getDiagram = async (id, token) => {
    try {
      const result = await getAppByIdApi({ id, token });
      dataObject = result.data[0];
      inputApplicationName.value = result.data[0].application_name;
      render(result.data[0]);
    } catch (error) {
      console.error("Error fetching diagram data:", error);
    }
  };

  // Render function
  const render = (data) => {
    const emptyTopology = document.getElementById("empty-topology");
    const topologyPreview = document.getElementById("topology");

    if (data.servers.length === 0 || data.application_name === "") {
      emptyTopology.classList.remove("d-none");
      topologyPreview.classList.add("d-none");
    } else {
      emptyTopology.classList.add("d-none");
      topologyPreview.classList.remove("d-none");
      generateTopology({ data: [data] });
    }
    generateTree(topology, data);
  };

  // Fetch and render diagram if appId is present
  if (appId && appId !== "") {
    await getDiagram(appId, token);
  } else {
    render(dataObject);
  }

  //Modal
  const chooseServerModal = new bootstrap.Modal(
    document.getElementById("choose_server_modal")
  );

  const serverModal = new bootstrap.Modal(
    document.getElementById("input_server_modal")
  );
  const surroundingServerModal = new bootstrap.Modal(
    document.getElementById("input_surrounding_server_modal")
  );
  const loadBalancerModal = new bootstrap.Modal(
    document.getElementById("input_load_balancer_modal")
  );

  const confirmModal = new bootstrap.Modal(
    document.getElementById("confirm_alert_modal")
  );

  const serviceModal = new bootstrap.Modal(
    document.getElementById("input_service_modal")
  );

  // Confirm Modal
  const confirmAlertTitle = document.getElementById("confirm_alert_title");
  const confirmAlertMessage = document.getElementById("confirm_alert_message");

  // Button
  const pushServerBtn = document.getElementById("push_server_btn");
  const pushSurroundingServerBtn = document.getElementById(
    "push_surrounding_server_btn"
  );
  const pushLoadBalancerBtn = document.getElementById("push_load_balancer_btn");
  const addServerBtn = document.getElementById("add_server_btn");
  const confirmAlertButton = document.getElementById("confirm_alert_button");
  const pushServiceBtn = document.getElementById("push_service_btn");
  const saveDiagramBtn = document.getElementById("save_diagram_btn");

  // Server Form
  const form = document.getElementById("server_form");
  let validator = serverValidator(form);

  const inputServerName = document.getElementById("server_name");
  const inputServerIp = document.getElementById("server_ip");
  const inputServerOs = document.getElementById("server_os");
  const inputServerCpu = document.getElementById("server_cpu");
  const inputServerRam = document.getElementById("server_ram");
  const inputServerMemory = document.getElementById("server_memory");
  const inputServerDescription = document.getElementById("server_description");

  // Surrounding Server Form

  const surroundingServerForm = document.getElementById(
    "surrounding_server_form"
  );
  let validatorSurroundingServer = surroundingServerValidator(
    surroundingServerForm
  );
  let surroundingServerTitle = document.getElementById(
    "surrounding_server_modal_title"
  );

  const inputSurroundingServerName = document.getElementById(
    "surrounding_server_name"
  );
  const inputSurroundingServerIp = document.getElementById(
    "surrounding_server_ip"
  );
  const inputSurroundingServerOs = document.getElementById(
    "surrounding_server_os"
  );
  const inputSurroundingServerCpu = document.getElementById(
    "surrounding_server_cpu"
  );
  const inputSurroundingServerRam = document.getElementById(
    "surrounding_server_ram"
  );
  const inputSurroundingServerMemory = document.getElementById(
    "surrounding_server_memory"
  );
  const inputSurroundingServerDescription = document.getElementById(
    "surrounding_server_description"
  );

  // Load Balancer Form

  const loadBalancerForm = document.getElementById("load_balancer_form");
  let validatorLoadBalancer = loadBalancerValidator(loadBalancerForm);
  const inputLoadBalancerName = document.getElementById("load_balancer_name");
  const inputLoadBalancerIp = document.getElementById("load_balancer_ip");

  // Service Form

  const serviceForm = document.getElementById("service_form");
  let validatorService = serviceValidator(serviceForm);
  const inputServiceName = document.getElementById("service_name");
  const inputServicePort = document.getElementById("service_port");
  const pathUrlAddon = document.getElementById("path_url_addons");
  const inputServiceParamsRequest = document.getElementById(
    "service_request_params"
  );
  const inputServiceParamsResponse = document.getElementById(
    "service_response_params"
  );
  const inputServiceUrl = document.getElementById("service_url");

  pushServiceBtn.addEventListener("click", function (e) {
    e.preventDefault();
    if (validatorService) {
      validatorService.validate().then(function (status) {
        if (status === "Valid") {
          if (serviceAction === "add") {
            const data = {
              service_name: inputServiceName.value,
              port: inputServicePort.value,
              params_request: inputServiceParamsRequest.value,
              params_response: inputServiceParamsResponse.value,
              url: inputServiceUrl.value,
            };

            if (isLB) {
              dataObject.servers[currentLBSRIndex.serverIndex].lb_servers[
                currentLBSRIndex.lbIndex
              ].surrounding_servers[currentLBSRIndex.lbSRIndex].services.push(
                data
              );
            } else {
              dataObject.servers[
                currentSurroundingServerIndex.serverIndex
              ].surrounding_servers[
                currentSurroundingServerIndex.surroundingServerIndex
              ].services.push(data);
            }
          } else if (serviceAction === "edit") {
            const data = {
              service_name: inputServiceName.value,
              port: inputServicePort.value,
              params_request: inputServiceParamsRequest.value,
              params_response: inputServiceParamsResponse.value,
              url: inputServiceUrl.value,
            };
            if (isLB) {
              dataObject.servers[currentLBServiceIndex.serverIndex].lb_servers[
                currentLBServiceIndex.lbIndex
              ].surrounding_servers[currentLBServiceIndex.lbSRIndex].services[
                currentLBServiceIndex.lbServiceIndex
              ] = data;
            } else {
              dataObject.servers[
                currentServiceIndex.serverIndex
              ].surrounding_servers[
                currentServiceIndex.surroundingServerIndex
              ].services[currentServiceIndex.serviceIndex] = data;
            }
          }

          render(dataObject);
          serviceModal.hide();
        }
      });
    }
  });

  pushServerBtn.addEventListener("click", function (e) {
    e.preventDefault();

    if (validator) {
      validator.validate().then(function (status) {
        if (status === "Valid") {
          switch (serverAction) {
            case "add":
              const data = {
                server_name: inputServerName.value,
                ip_address: inputServerIp.value,
                os: inputServerOs.value,
                cpu: inputServerCpu.value,
                ram: inputServerRam.value,
                memory: inputServerMemory.value,
                installed_apps: inputServerDescription.value,
                surrounding_servers: [],
                lb_servers: [],
              };
              dataObject.servers.push(data);
              break;
            case "edit":
              const dataEdit = {
                server_name: inputServerName.value,
                ip_address: inputServerIp.value,
                os: inputServerOs.value,
                cpu: inputServerCpu.value,
                ram: inputServerRam.value,
                memory: inputServerMemory.value,
                installed_apps: inputServerDescription.value,
              };
              dataObject.servers[currentServerIndex] = {
                ...dataObject.servers[currentServerIndex],
                ...dataEdit,
              };
              break;
            case "delete":
              dataObject.servers.splice(currentServerIndex, 1);
              break;
          }
          render(dataObject);
          serverModal.hide();
        }
      });
    }
  });

  pushLoadBalancerBtn.addEventListener("click", function (e) {
    e.preventDefault();
    if (validatorLoadBalancer) {
      validatorLoadBalancer.validate().then(function (status) {
        if (status === "Valid") {
          if (lbAction === "add") {
            const data = {
              name: inputLoadBalancerName.value,
              ip_address: inputLoadBalancerIp.value,
              surrounding_servers: [],
            };

            dataObject.servers[currentServerIndex].lb_servers.push(data);
          } else if (lbAction === "edit") {
            const dataEdit = {
              name: inputLoadBalancerName.value,
              ip_address: inputLoadBalancerIp.value,
            };
            dataObject.servers[currentLBIndex.serverIndex].lb_servers[
              currentLBIndex.serverIndex
            ] = {
              ...dataObject.servers[currentLBIndex.serverIndex].lb_servers[
                currentLBIndex.serverIndex
              ],
              ...dataEdit,
            };
          }

          render(dataObject);
          loadBalancerModal.hide();
        }
      });
    }
  });

  pushSurroundingServerBtn.addEventListener("click", function (e) {
    e.preventDefault();
    if (validatorSurroundingServer) {
      validatorSurroundingServer.validate().then(function (status) {
        if (status === "Valid") {
          if (surroundingServerAction === "add") {
            const data = {
              surrounding_server_name: inputSurroundingServerName.value,
              ip_address: inputSurroundingServerIp.value,
              os: inputSurroundingServerOs.value,
              cpu: inputSurroundingServerCpu.value,
              ram: inputSurroundingServerRam.value,
              memory: inputSurroundingServerMemory.value,
              type: surroundingType,
              installed_apps: inputSurroundingServerDescription.value,
              services: [],
            };
            if (isLB) {
              dataObject.servers[currentLBIndex.serverIndex].lb_servers[
                currentLBIndex.lbIndex
              ].surrounding_servers.push(data);
            } else {
              dataObject.servers[currentServerIndex].surrounding_servers.push(
                data
              );
            }
          } else if (surroundingServerAction === "edit") {
            const data = {
              surrounding_server_name: inputSurroundingServerName.value,
              ip_address: inputSurroundingServerIp.value,
              os: inputSurroundingServerOs.value,
              cpu: inputSurroundingServerCpu.value,
              ram: inputSurroundingServerRam.value,
              memory: inputSurroundingServerMemory.value,
              installed_apps: inputSurroundingServerDescription.value,
            };
            if (isLB) {
              dataObject.servers[currentLBSRIndex.serverIndex].lb_servers[
                currentLBSRIndex.lbIndex
              ].surrounding_servers[currentLBSRIndex.lbSRIndex] = {
                ...dataObject.servers[currentLBSRIndex.serverIndex].lb_servers[
                  currentLBSRIndex.lbIndex
                ].surrounding_servers[currentLBSRIndex.lbSRIndex],
                ...data,
              };
            } else {
              dataObject.servers[
                currentSurroundingServerIndex.serverIndex
              ].surrounding_servers[
                currentSurroundingServerIndex.surroundingServerIndex
              ] = {
                ...dataObject.servers[currentSurroundingServerIndex.serverIndex]
                  .surrounding_servers[
                  currentSurroundingServerIndex.surroundingServerIndex
                ],
                ...data,
              };
            }
          }
          render(dataObject);
          surroundingServerModal.hide();
        }
      });
    }
  });

  addServerBtn.addEventListener("click", function (e) {
    resetForm();
    serverAction = "add";
    pushServerBtn.innerHTML = "Add Server";
    serverModal.show();
  });

  const resetForm = () => {
    inputServerName.value = "";
    inputServerIp.value = "";
    inputServerOs.value = "";
    inputServerCpu.value = "";
    inputServerRam.value = "";
    inputServerMemory.value = "";
    inputServerDescription.value = "";

    validator.resetForm();
  };

  const resetSurroundingServerForm = () => {
    inputSurroundingServerName.value = "";
    inputSurroundingServerIp.value = "";
    inputSurroundingServerOs.value = "";
    inputSurroundingServerCpu.value = "";
    inputSurroundingServerRam.value = "";
    inputSurroundingServerMemory.value = "";
    inputSurroundingServerDescription.value = "";
    validatorSurroundingServer.resetForm();
  };

  const resetLoadBalancerForm = () => {
    inputLoadBalancerName.value = "";
    inputLoadBalancerIp.value = "";
    validatorLoadBalancer.resetForm();
  };

  const resetServiceForm = () => {
    inputServiceName.value = "";
    inputServicePort.value = "";
    inputServiceParamsRequest.value = "";
    inputServiceParamsResponse.value = "";
    inputServiceUrl.value = "";
    validatorService.resetForm();
  };

  window.addSurroundingServer = (index) => {
    isLB = false;
    pushSurroundingServerBtn.innerHTML = "Add";
    surroundingServerAction = "add";
    currentServerIndex = index;
    chooseServerModal.show();
  };
  window.showSurroundingServerModal = () => {
    resetSurroundingServerForm();
    surroundingServerTitle.innerHTML = "Surrounding Server Information";
    surroundingType = "app";
    chooseServerModal.hide();
    surroundingServerModal.show();
  };

  window.showDBModal = () => {
    surroundingServerTitle.innerHTML = "DB Information";
    resetSurroundingServerForm();
    surroundingType = "db";
    chooseServerModal.hide();
    surroundingServerModal.show();
  };

  window.showLBModal = () => {
    resetLoadBalancerForm();
    pushLoadBalancerBtn.innerHTML = "Add Load Balancer";
    chooseServerModal.hide();
    loadBalancerModal.show();
  };

  window.editServer = (index) => {
    const data = dataObject.servers[index];
    resetForm();
    inputServerName.value = data.server_name;
    inputServerIp.value = data.ip_address;
    inputServerOs.value = data.os;
    inputServerCpu.value = data.cpu;
    inputServerRam.value = data.ram;
    inputServerMemory.value = data.memory;
    inputServerDescription.value = data.installed_apps;
    serverModal.show();
    currentServerIndex = index;
    serverAction = "edit";
    pushServerBtn.innerHTML = "Edit Server";
  };

  window.removeServer = (index) => {
    confirmModal.show();
    confirmAlertFor = "server";
    confirmAlertTitle.innerHTML = "Delete Server";
    confirmAlertMessage.innerHTML =
      "Are you sure you want to delete this server? This action cannot be undone.";
    serverAction = "remove";
    currentServerIndex = index;
  };

  window.deleteSurroundingServer = (
    serverIndex,
    surroundingServerIndex,
    type
  ) => {
    confirmModal.show();
    confirmAlertFor = "surrounding_server";
    const label = type == "app" ? "Surrounding Server" : "Database";
    confirmAlertTitle.innerHTML = `Delete ${label}`;
    confirmAlertMessage.innerHTML = `Are you sure you want to delete this ${label}? This action cannot be undone.`;
    currentSurroundingServerIndex = {
      serverIndex,
      surroundingServerIndex,
    };
  };

  window.deleteService = (serverIndex, surroundingIndex, serviceIndex) => {
    confirmModal.show();
    confirmAlertFor = "service";
    confirmAlertTitle.innerHTML = "Delete Service";
    confirmAlertMessage.innerHTML =
      "Are you sure you want to delete this service? This action cannot be undone.";
    currentServiceIndex = {
      serverIndex: serverIndex,
      surroundingServerIndex: surroundingIndex,
      serviceIndex: serviceIndex,
    };
  };

  window.deleteLb = (serverIndex, lbIndex) => {
    confirmModal.show();
    confirmAlertFor = "lb";
    confirmAlertTitle.innerHTML = "Delete Load Balancer";
    confirmAlertMessage.innerHTML =
      "Are you sure you want to delete this load balancer? This action cannot be undone.";
    currentLBIndex = {
      serverIndex: serverIndex,
      lbIndex: lbIndex,
    };
  };

  window.deleteLbSurroundingServer = (
    serverIndex,
    lbIndex,
    surroundingIndex,
    type
  ) => {
    confirmModal.show();
    confirmAlertFor = "lb_surrounding_server";
    const label = type == "app" ? "Surrounding Server" : "Database";
    confirmAlertTitle.innerHTML = `Delete ${label}`;
    confirmAlertMessage.innerHTML = `Are you sure you want to delete this ${label}? This action cannot be undone.`;
    currentLBSRIndex = {
      serverIndex: serverIndex,
      lbIndex: lbIndex,
      lbSRIndex: surroundingIndex,
    };
  };

  window.deleteLbService = (serverIndex, lbIndex, lbSRIndex, serviceIndex) => {
    confirmModal.show();
    confirmAlertFor = "lb_service";
    confirmAlertTitle.innerHTML = "Delete Load Balancer Service";
    confirmAlertMessage.innerHTML =
      "Are you sure you want to delete this load balancer service? This action cannot be undone.";
    currentLBServiceIndex = {
      serverIndex: serverIndex,
      lbIndex: lbIndex,
      lbSRIndex: lbSRIndex,
      lbServiceIndex: serviceIndex,
    };
  };

  window.addService = (serverIndex, surroundingIndex) => {
    serviceAction = "add";
    pushServiceBtn.innerHTML = "Add Service";
    const surroundingServer =
      dataObject.servers[serverIndex].surrounding_servers[surroundingIndex];
    pathUrlAddon.innerText = `${surroundingServer.ip_address}:/`;
    isLB = false;
    resetServiceForm();
    currentSurroundingServerIndex = {
      serverIndex: serverIndex,
      surroundingServerIndex: surroundingIndex,
    };
    serviceModal.show();
  };

  window.addLbSurroundingServer = (serverIndex, lbIndex) => {
    surroundingServerAction = "add";
    pushSurroundingServerBtn.innerHTML = "Add";
    surroundingServerTitle.innerHTML = "Surrounding Information";
    surroundingType = "app";
    currentLBIndex = {
      serverIndex: serverIndex,
      lbIndex: lbIndex,
    };
    isLB = true;
    resetSurroundingServerForm();
    surroundingServerModal.show();
  };

  window.addLbService = (serverIndex, lbIndex, lbSRIndex) => {
    serviceAction = "add";
    pushServiceBtn.innerHTML = "Add Service";
    const surroundingServer =
      dataObject.servers[serverIndex].lb_servers[lbIndex].surrounding_servers[
        lbSRIndex
      ];
    pathUrlAddon.innerText = `${surroundingServer.ip_address}:/`;
    currentLBSRIndex = {
      serverIndex,
      lbIndex,
      lbSRIndex,
    };
    isLB = true;
    resetServiceForm();
    serviceModal.show();
  };

  window.editSurroundingServer = (serverIndex, surroundingIndex) => {
    surroundingServerAction = "edit";
    pushSurroundingServerBtn.innerHTML = "Edit";
    isLB = false;
    currentSurroundingServerIndex = {
      serverIndex,
      surroundingServerIndex: surroundingIndex,
    };
    resetSurroundingServerForm();
    inputSurroundingServerName.value =
      dataObject.servers[serverIndex].surrounding_servers[
        surroundingIndex
      ].surrounding_server_name;
    inputSurroundingServerIp.value =
      dataObject.servers[serverIndex].surrounding_servers[
        surroundingIndex
      ].ip_address;
    inputSurroundingServerOs.value =
      dataObject.servers[serverIndex].surrounding_servers[surroundingIndex].os;
    inputSurroundingServerCpu.value =
      dataObject.servers[serverIndex].surrounding_servers[surroundingIndex].cpu;
    inputSurroundingServerRam.value =
      dataObject.servers[serverIndex].surrounding_servers[surroundingIndex].ram;
    inputSurroundingServerMemory.value =
      dataObject.servers[serverIndex].surrounding_servers[
        surroundingIndex
      ].memory;
    inputSurroundingServerDescription.value =
      dataObject.servers[serverIndex].surrounding_servers[
        surroundingIndex
      ].installed_apps;
    currentSurroundingServerIndex = {
      serverIndex: serverIndex,
      surroundingServerIndex: surroundingIndex,
    };
    surroundingServerModal.show();
  };

  window.editLb = (serverIndex, lbIndex) => {
    lbAction = "edit";
    resetLoadBalancerForm();
    pushLoadBalancerBtn.innerHTML = "Edit Load Balancer";

    currentLBIndex = {
      serverIndex: serverIndex,
      lbIndex: lbIndex,
    };
    inputLoadBalancerName.value =
      dataObject.servers[currentLBIndex.serverIndex].lb_servers[
        currentLBIndex.lbIndex
      ].name;
    inputLoadBalancerIp.value =
      dataObject.servers[currentLBIndex.serverIndex].lb_servers[
        currentLBIndex.lbIndex
      ].ip_address;
    loadBalancerModal.show();
  };

  window.editLbSurroundingServer = (serverIndex, lbIndex, lbSRIndex) => {
    surroundingServerAction = "edit";
    isLB = true;
    pushSurroundingServerBtn.innerHTML = "Edit";
    resetSurroundingServerForm();
    currentLBSRIndex = {
      serverIndex,
      lbIndex,
      lbSRIndex,
    };
    inputSurroundingServerName.value =
      dataObject.servers[serverIndex].lb_servers[lbIndex].surrounding_servers[
        lbSRIndex
      ].surrounding_server_name;
    inputSurroundingServerIp.value =
      dataObject.servers[serverIndex].lb_servers[lbIndex].surrounding_servers[
        lbSRIndex
      ].ip_address;
    inputSurroundingServerOs.value =
      dataObject.servers[serverIndex].lb_servers[lbIndex].surrounding_servers[
        lbSRIndex
      ].os;
    inputSurroundingServerCpu.value =
      dataObject.servers[serverIndex].lb_servers[lbIndex].surrounding_servers[
        lbSRIndex
      ].cpu;
    inputSurroundingServerRam.value =
      dataObject.servers[serverIndex].lb_servers[lbIndex].surrounding_servers[
        lbSRIndex
      ].ram;
    inputSurroundingServerMemory.value =
      dataObject.servers[serverIndex].lb_servers[lbIndex].surrounding_servers[
        lbSRIndex
      ].memory;
    inputSurroundingServerDescription.value =
      dataObject.servers[serverIndex].lb_servers[lbIndex].surrounding_servers[
        lbSRIndex
      ].installed_apps;

    surroundingServerModal.show();
  };

  window.editService = (serverIndex, surroundingIndex, serviceIndex) => {
    serviceAction = "edit";
    isLB = false;
    resetServiceForm();
    const surroundingServer =
      dataObject.servers[serverIndex].surrounding_servers[surroundingIndex];
    const service = surroundingServer.services[serviceIndex];
    pathUrlAddon.innerHTML = `${surroundingServer.ip_address}:${service.port}/`;
    pushServiceBtn.innerHTML = "Edit Service";
    currentServiceIndex = {
      serverIndex: serverIndex,
      surroundingServerIndex: surroundingIndex,
      serviceIndex: serviceIndex,
    };

    inputServiceName.value =
      dataObject.servers[serverIndex].surrounding_servers[
        surroundingIndex
      ].services[serviceIndex].service_name;
    inputServicePort.value =
      dataObject.servers[serverIndex].surrounding_servers[
        surroundingIndex
      ].services[serviceIndex].port;

    inputServiceUrl.value =
      dataObject.servers[serverIndex].surrounding_servers[
        surroundingIndex
      ].services[serviceIndex].url;

    inputServiceParamsRequest.value =
      dataObject.servers[serverIndex].surrounding_servers[
        surroundingIndex
      ].services[serviceIndex].params_request;
    inputServiceParamsResponse.value =
      dataObject.servers[serverIndex].surrounding_servers[
        surroundingIndex
      ].services[serviceIndex].params_response;
    serviceModal.show();
  };

  window.editLbService = (serverIndex, lbIndex, lbSRIndex, lbServiceIndex) => {
    serviceAction = "edit";
    isLB = true;
    resetServiceForm();
    const surroundingServer =
      dataObject.servers[serverIndex].lb_servers[lbIndex].surrounding_servers[
        lbSRIndex
      ];
    const service = surroundingServer.services[lbServiceIndex];
    pathUrlAddon.innerHTML = `${surroundingServer.ip_address}:${service.port}/`;
    pushServiceBtn.innerHTML = "Edit Service";
    currentLBServiceIndex = {
      serverIndex,
      lbIndex,
      lbSRIndex,
      lbServiceIndex,
    };

    inputServiceName.value =
      dataObject.servers[serverIndex].lb_servers[lbIndex].surrounding_servers[
        lbSRIndex
      ].services[lbServiceIndex].service_name;
    inputServicePort.value =
      dataObject.servers[serverIndex].lb_servers[lbIndex].surrounding_servers[
        lbSRIndex
      ].services[lbServiceIndex].port;
    inputServiceUrl.value =
      dataObject.servers[serverIndex].lb_servers[lbIndex].surrounding_servers[
        lbSRIndex
      ].services[lbServiceIndex].url;
    inputServiceParamsRequest.value =
      dataObject.servers[serverIndex].lb_servers[lbIndex].surrounding_servers[
        lbSRIndex
      ].services[lbServiceIndex].params_request;
    inputServiceParamsResponse.value =
      dataObject.servers[serverIndex].lb_servers[lbIndex].surrounding_servers[
        lbSRIndex
      ].services[lbServiceIndex].params_response;
    serviceModal.show();
  };

  confirmAlertButton.addEventListener("click", function (e) {
    switch (confirmAlertFor) {
      case "server":
        dataObject.servers.splice(currentServerIndex, 1);
        break;

      case "surrounding_server":
        dataObject.servers[
          currentSurroundingServerIndex.serverIndex
        ].surrounding_servers.splice(
          currentSurroundingServerIndex.surroundingServerIndex,
          1
        );
        break;

      case "service":
        dataObject.servers[currentServiceIndex.serverIndex].surrounding_servers[
          currentServiceIndex.surroundingServerIndex
        ].services.splice(currentServiceIndex.serviceIndex, 1);
        break;

      case "lb":
        dataObject.servers[currentLBIndex.serverIndex].lb_servers.splice(
          currentLBIndex.lbIndex,
          1
        );
        break;

      case "lb_surrounding_server":
        dataObject.servers[currentLBSRIndex.serverIndex].lb_servers[
          currentLBSRIndex.lbIndex
        ].surrounding_servers.splice(currentLBSRIndex.lbSRIndex, 1);
        break;

      case "lb_service":
        dataObject.servers[currentLBServiceIndex.serverIndex].lb_servers[
          currentLBServiceIndex.lbIndex
        ].surrounding_servers[currentLBServiceIndex.lbSRIndex].services.splice(
          currentLBServiceIndex.lbServiceIndex,
          1
        );
        break;
    }

    confirmAlertFor = "";
    render(dataObject);
    confirmModal.hide();
  });
  inputApplicationName.addEventListener("input", function (e) {
    dataObject["application_name"] = inputApplicationName.value;
    render(dataObject);
  });

  checkBoxLB.addEventListener("change", function (e) {
    if (!e.target.checked) {
      $("#ip_load_balancer").addClass("d-none");
    } else {
      $("#ip_load_balancer").removeClass("d-none");
    }
    dataObject["ip_load_balancer"] = e.target.checked
      ? $("#ip_load_balancer").val()
      : "";
    render(dataObject);
  });

  $("#ip_load_balancer").on("input", function () {
    dataObject["ip_load_balancer"] = $("#ip_load_balancer").val();
    render(dataObject);
  });

  $("#generate_btn").on("click", function (e) {
    render(dataObject);
  });

  saveDiagramBtn.addEventListener("click", function (e) {
    e.preventDefault();
    if (
      inputApplicationName.value.trim() === "" ||
      dataObject.servers.length === 0
    ) {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Application name and atleast one server is required!",
      });
    } else {
      switch (pathType) {
        case "create":
          postDiagram({
            data: dataObject,
            token,
          });
          break;
        case "edit":
          updateAppById({
            id: appId,
            data: dataObject,
            token,
          });
          break;
      }
    }
  });

  inputServicePort.addEventListener(
    "input",
    function (e) {
      let surroundingServer;

      if (isLB) {
        surroundingServer =
          dataObject.servers[currentLBSRIndex.serverIndex].lb_servers[
            currentLBSRIndex.lbIndex
          ].surrounding_servers[currentLBSRIndex.lbSRIndex];
      } else {
        surroundingServer =
          dataObject.servers[currentSurroundingServerIndex.serverIndex]
            .surrounding_servers[
            currentSurroundingServerIndex.surroundingServerIndex
          ];
      }

      pathUrlAddon.innerText = `${surroundingServer.ip_address}:${e.target.value}/`;
    },
    false
  );
});
