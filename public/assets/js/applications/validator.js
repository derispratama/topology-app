export const serverValidator = (form) =>
  FormValidation.formValidation(form, {
    fields: {
      server_name: {
        validators: {
          notEmpty: {
            message: "Server Name is required",
          },
        },
      },

      server_ip: {
        validators: {
          notEmpty: {
            message: "Server IP is required",
          },
          ip: {
            message: "Invalid IP Address",
          },
        },
      },

      server_os: {
        validators: {
          notEmpty: {
            message: "Server OS is required",
          },
        },
      },

      server_cpu: {
        validators: {
          notEmpty: {
            message: "Server CPU is required",
          },
        },
      },

      server_ram: {
        validators: {
          notEmpty: {
            message: "Server RAM is required",
          },
        },
      },

      server_memory: {
        validators: {
          notEmpty: {
            message: "Server Memory is required",
          },
        },
      },

      server_description: {
        validators: {
          notEmpty: {
            message: "Server Description is required",
          },
        },
      },
    },

    plugins: {
      trigger: new FormValidation.plugins.Trigger(),
      bootstrap: new FormValidation.plugins.Bootstrap5({
        rowSelector: ".fv-row",
        eleInvalidClass: "",
        eleValidClass: "",
      }),
    },
  });

export const surroundingServerValidator = (form) =>
  FormValidation.formValidation(form, {
    fields: {
      surrounding_server_name: {
        validators: {
          notEmpty: {
            message: "Server Name is required",
          },
        },
      },

      surrounding_server_ip: {
        validators: {
          notEmpty: {
            message: "Server IP is required",
          },
          ip: {
            message: "Invalid IP Address",
          },
        },
      },

      surrounding_server_os: {
        validators: {
          notEmpty: {
            message: "Server OS is required",
          },
        },
      },

      surrounding_server_cpu: {
        validators: {
          notEmpty: {
            message: "Server CPU is required",
          },
        },
      },

      surrounding_server_ram: {
        validators: {
          notEmpty: {
            message: "Server RAM is required",
          },
        },
      },

      surrounding_server_memory: {
        validators: {
          notEmpty: {
            message: "Server Memory is required",
          },
        },
      },

      surrounding_server_description: {
        validators: {
          notEmpty: {
            message: "Server Description is required",
          },
        },
      },
    },

    plugins: {
      trigger: new FormValidation.plugins.Trigger(),
      bootstrap: new FormValidation.plugins.Bootstrap5({
        rowSelector: ".fv-row",
        eleInvalidClass: "",
        eleValidClass: "",
      }),
    },
  });

export const loadBalancerValidator = (form) =>
  FormValidation.formValidation(form, {
    fields: {
      load_balancer_name: {
        validators: {
          notEmpty: {
            message: "Server Name is required",
          },
        },
      },

      load_balancer_ip: {
        validators: {
          notEmpty: {
            message: "Server IP is required",
          },
          ip: {
            message: "Invalid IP Address",
          },
        },
      },
    },

    plugins: {
      trigger: new FormValidation.plugins.Trigger(),
      bootstrap: new FormValidation.plugins.Bootstrap5({
        rowSelector: ".fv-row",
        eleInvalidClass: "",
        eleValidClass: "",
      }),
    },
  });

export const serviceValidator = (form) =>
  FormValidation.formValidation(form, {
    fields: {
      service_name: {
        validators: {
          notEmpty: {
            message: "Service Name is required",
          },
        },
      },

      service_port: {
        validators: {
          notEmpty: {
            message: "Port is required",
          },
          integer: {
            message: "Port must be an integer",
          },
        },
      },
      service_url: {
        validators: {
          notEmpty: {
            message: "URL is required",
          },
        },
      },
      service_request_params: {
        validators: {
          notEmpty: {
            message: "Request Params is required",
          },
        },
      },
      service_response_params: {
        validators: {
          notEmpty: {
            message: "Response Params is required",
          },
        },
      },
    },

    plugins: {
      trigger: new FormValidation.plugins.Trigger(),
      bootstrap: new FormValidation.plugins.Bootstrap5({
        rowSelector: ".fv-row",
        eleInvalidClass: "",
        eleValidClass: "",
      }),
    },
  });
