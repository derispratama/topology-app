export async function postDiagram({ data, token }) {
  try {
    const response = await fetch(`/api/applications`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        accept: "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(data),
    });
    if (!response.ok) {
      throw Error("error");
    }
    Swal.fire({
      text: "Diagram has been save!",
      icon: "success",
      buttonsStyling: false,
      confirmButtonText: "Done",
      customClass: {
        confirmButton: "btn btn-primary",
      },
    }).then((result) => {
      if (result.isConfirmed) {
        window.location.href = "/application";
      }
    });
  } catch (error) {
    defaultErrorSwal();
    console.log(error);
  }
}

export function defaultErrorSwal() {
  Swal.fire({
    text: `${"something went wrong"}`,
    icon: "error",
    buttonsStyling: false,
    confirmButtonText: "OK",
    customClass: {
      confirmButton: "btn btn-danger",
    },
  });
}

export const getAppByIdApi = async ({ id, token }) => {
  try {
    const response = await fetch(`/api/applications/${id}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        accept: "application/json",
        Authorization: `Bearer ${token}`,
      },
    });
    if (!response.ok) {
      throw Error("error");
    }
    const data = await response.json();
    return data;
    // return await response.json();
  } catch (error) {
    console.log(error);
  }
};

export async function updateAppById({ id, data, token }) {
  try {
    const response = await fetch(`/api/applications/${id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        accept: "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(data),
    });

    if (!response.ok) {
      throw Error("error");
    }
    Swal.fire({
      text: "Diagram has been save!",
      icon: "success",
      buttonsStyling: false,
      confirmButtonText: "Done",
      customClass: {
        confirmButton: "btn btn-primary",
      },
    }).then((result) => {
      if (result.isConfirmed) {
        window.location.href = "/application";
      }
    });
  } catch (error) {
    defaultErrorSwal();
  }
}
