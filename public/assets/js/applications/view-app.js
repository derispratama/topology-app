import { getToken } from "../auth.js";
import { generateTopology } from "../generate-topology.js";
import { getAppByIdApi } from "./api.js";

document.addEventListener("DOMContentLoaded", async function () {
  let dataObject = {};
  const token = getToken();
  const appId = getId();
  const getDiagram = async (id, token) => {
    try {
      const result = await getAppByIdApi({ id, token });
      dataObject = result.data[0];
      render(result.data[0]);
    } catch (error) {
      console.error("Error fetching diagram data:", error);
    }
  };

  // Render function
  const render = (data) => {
    if (data.servers.length === 0 || data.application_name === "") {
    } else {
      generateTopology({ data: [data] });
    }
  };

  // Fetch and render diagram if appId is present
  if (appId && appId !== "") {
    await getDiagram(appId, token);
  } else {
    render(dataObject);
  }
});
