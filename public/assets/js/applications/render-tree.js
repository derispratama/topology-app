export function generateSurroundingNodes(
  surroundingServer,
  index,
  surroundingIndex,
  ip
) {
  surroundingServer.services.forEach((service, serviceIndex) => {
    const olServiceTarget = document.getElementById(
      `surrounding-server-${index}-${surroundingIndex}`
    );
    const serviceElement = document.createElement("li");
    serviceElement.classList.add("service-level");
    serviceElement.innerHTML = `
            <div class="label-item">
                <div class="row w-100">
          <span><strong>${service.service_name}</strong> </span>
          <span class="fs-8">(${ip}:${service.port}${service.url})</span>
        </div>
                <div class="button-container">
                    <button class="btn btn-sm btn-primary" onclick="editService(${index},${surroundingIndex},${serviceIndex})">edit / detail</button>
                    <button class="btn btn-sm btn-danger" onclick="deleteService(${index},${surroundingIndex},${serviceIndex})">remove</button>
                </div>
            </div>
        `;
    olServiceTarget.appendChild(serviceElement);
  });
}

export function generateServerNodes(server, index) {
  server.surrounding_servers.forEach((surroundingServer, surroundingIndex) => {
    const olSurroundingTarget = document.getElementById(`server-${index}`);
    const surroundingServerName = surroundingServer.surrounding_server_name;
    const surroundingServerElement = document.createElement("li");
    surroundingServerElement.classList.add(
      surroundingServer.type === "app" ? "surrounding-db" : "database"
    );
    surroundingServerElement.innerHTML = `
                <div class="label-item">
                  <div class="row w-100" >
                    <span>
                    ${
                      surroundingServer.type === "db"
                        ? '<i class="bi bi-database" style="font-size: 20px;"></i> '
                        : surroundingServer.type === "app"
                        ? '<i class="fa fa-desktop" style="font-size: 20px;"></i> '
                        : ""
                    } 
                    <strong>${
                      surroundingServer.ip_address
                    }</strong> - ${surroundingServerName}</span>
                    <span class="fs-8"> (${surroundingServer.os}, ${
      surroundingServer.cpu
    } Core, ${surroundingServer.ram} GB, ${surroundingServer.memory} GB)</span>
                  </div>
                <div class="button-container">
                  ${
                    surroundingServer.type === "app"
                      ? `<button class="btn btn-sm btn-success" onclick="addService(${index},${surroundingIndex})">
  
                      add</button>`
                      : ""
                  }
                    <button class="btn btn-sm btn-primary" onclick="editSurroundingServer(${index},${surroundingIndex})">edit / detail</button>
                    <button class="btn btn-sm btn-danger"  onclick="deleteSurroundingServer(${index},${surroundingIndex}, '${
      surroundingServer.type
    }')">remove</button>
                </div>
            </div>
            <ol id="surrounding-server-${index}-${surroundingIndex}">
            </ol>
        `;
    olSurroundingTarget.appendChild(surroundingServerElement);
    // Generate Service Nodes
    generateSurroundingNodes(
      surroundingServer,
      index,
      surroundingIndex,
      surroundingServer.ip_address
    );
  });
}

export function generateLbServiceNodes(
  surroundingServer,
  index,
  lbIndex,
  surroundingIndex,
  ip
) {
  surroundingServer.services.forEach((service, serviceIndex) => {
    const olServiceTarget = document.getElementById(
      `lb-surrounding-server-${index}-${lbIndex}-${surroundingIndex}`
    );
    const serviceElement = document.createElement("li");
    serviceElement.classList.add("service-level");
    serviceElement.innerHTML = `
            <div class="label-item">
                <div class="row w-100">
          <span><strong>${service.service_name}</strong></span>
          <span class="fs-8">(${ip}:${service.port}/${service.url})</span>
        </div>
                <div class="button-container">
                    <button class="btn btn-sm btn-primary" onclick="editLbService(${index},${lbIndex},${surroundingIndex},${serviceIndex})">edit / detail</button>
                    <button class="btn btn-sm btn-danger"  onclick="deleteLbService(${index},${lbIndex},${surroundingIndex},${serviceIndex})">remove</button>
                </div>
            </div>
        `;
    olServiceTarget.appendChild(serviceElement);
  });
}

export function generateLbSurroundingServerNodes(lb, index, lbIndex) {
  lb.surrounding_servers.forEach((surroundingServer, surroundingIndex) => {
    const olSurroundingTarget = document.getElementById(
      `load-balancer-${index}-${lbIndex}`
    );
    const surroundingServerName = surroundingServer.surrounding_server_name;
    const surroundingServerElement = document.createElement("li");
    surroundingServerElement.classList.add("surrounding-db");
    surroundingServerElement.innerHTML = `
            <div class="label-item">
                <div class="row w-100">
                  <span><strong>${
                    surroundingServer.ip_address
                  }</strong> - ${surroundingServerName}</span>
                  <span class="fs-8">(${surroundingServer.os}, ${
      surroundingServer.cpu
    } Core, ${surroundingServer.ram} GB, ${surroundingServer.memory} GB)</span>
                </div>
                <div class="button-container">
                    ${
                      surroundingServer.type === "app"
                        ? `<button class="btn btn-sm btn-success" onclick="addLbService(${index},${lbIndex},${surroundingIndex})">add</button>`
                        : ""
                    }
                    <button class="btn btn-sm btn-primary" onclick="editLbSurroundingServer(${index},${lbIndex},${surroundingIndex})">edit / detail</button>
                    <button class="btn btn-sm btn-danger"  onclick="deleteLbSurroundingServer(${index},${lbIndex},${surroundingIndex}, '${
      surroundingServer.type
    }')">remove</button>
                </div>
            </div>
            <ol id="lb-surrounding-server-${index}-${lbIndex}-${surroundingIndex}">
            </ol>
        `;
    olSurroundingTarget.appendChild(surroundingServerElement);

    // Generate Service Nodes
    generateLbServiceNodes(
      surroundingServer,
      index,
      lbIndex,
      surroundingIndex,
      surroundingServer.ip_address
    );
  });
}

export function generateLbNodes(lb, index, lbIndex) {
  const olLB = document.getElementById(`server-${index}`);
  const lbElement = document.createElement("li");
  lbElement.classList.add("load-balancer");
  lbElement.innerHTML = `
        <div class="label-item">
            <span><strong>${lb.ip_address}</strong> - ${lb.name}</span>
            <div class="button-container">
                <button class="btn btn-sm btn-success" onclick="addLbSurroundingServer(${index},${lbIndex})">add</button>
                <button class="btn btn-sm btn-primary" onclick="editLb(${index},${lbIndex})">edit / detail</button>
                <button class="btn btn-sm btn-danger" onclick="deleteLb(${index},${lbIndex})">remove</button>
            </div>
        </div>
        <ol id="load-balancer-${index}-${lbIndex}">
        </ol>
    `;
  olLB.appendChild(lbElement);

  generateLbSurroundingServerNodes(lb, index, lbIndex);
}

export function generateTree(topology, data) {
  topology.innerHTML = "";
  data.servers.forEach((server, index) => {
    const serverName = server.server_name;
    const serverElement = document.createElement("li");
    serverElement.classList.add("server-level");
    serverElement.innerHTML = `
          <div class="label-item ">
            <div class="row w-100" >
              <span><strong>${server.ip_address} </strong> - ${serverName} </span>
              <span class="fs-8"> (${server.os}, ${server.cpu} Core, ${server.ram} GB, ${server.memory} GB)</span>
            </div>
              <div class="button-container">
                  <button class="btn btn-sm btn-success" onclick="addSurroundingServer(${index})">add</button>
                  <button class="btn btn-sm btn-primary" onclick="editServer(${index})">edit / detail</button>
                  <button class="btn btn-sm btn-danger"  onclick="removeServer(${index})">remove</button>
              </div>
          </div>
          <ol id="server-${index}">
          </ol>
      `;
    topology.appendChild(serverElement);

    generateServerNodes(server, index);

    // Generate load balancers
    server.lb_servers.forEach((lb, lbIndex) => {
      generateLbNodes(lb, index, lbIndex);
    });
  });
}
