export function renderTable({ servers, serverTableBody, colors }) {
  const existingTooltips = [].slice.call(
    document.querySelectorAll('[data-bs-toggle="tooltip"]')
  );
  const pathUrl = getPathUrl();

  existingTooltips.forEach(function (tooltipEl) {
    const tooltipInstance = bootstrap.Tooltip.getInstance(tooltipEl);
    if (tooltipInstance) {
      tooltipInstance.dispose();
    }
  });
  serverTableBody.innerHTML = "";
  if (servers.length == 0) {
    const emptyRow = document.createElement("tr");
    emptyRow.innerHTML = `
      <td colspan="4">
        <div class="d-flex flex-column align-items-center justify-content-center">
          <i class="bi bi-server" style="font-size: 2rem; color: #6c757d;"></i>
          <span style="color: #6c757d;">Server is empty</span>
        </div>
      </td>
    `;
    serverTableBody.appendChild(emptyRow);
  }
  servers.forEach((server, index) => {
    const serverRow = document.createElement("tr");
    serverRow.style.backgroundColor = colors[0];
    let buttonsHtml = "";

    if (pathUrl !== "view") {
      buttonsHtml = `
          <button class="btn btn-success btn-sm" onclick="addSurroundingServer(${index})" 
          data-bs-toggle="tooltip" data-bs-placement="top" title="Add Surrounding Server" data-bs-custom-class="tooltip-inverse"
          id="addSurroundingBtn">
              <i class="bi bi-plus fs-6"></i>
          </button>
          <button class="btn btn-danger btn-sm" onclick="confirmRemoveServer(${index})"
          data-bs-toggle="tooltip" data-bs-placement="top" title="Delete Server" data-bs-custom-class="tooltip-inverse"
          id="deleteSurroundingBtn">
              <i class="bi bi-trash3 fs-6"></i>
          </button>
          <button class="btn btn-warning btn-sm" onclick="openServerModal_('server',${index})"
            data-bs-toggle="tooltip" data-bs-placement="top" title="Server Detail" data-bs-custom-class="tooltip-inverse"
            id="editServerBtn">
                <i class="bi bi-pencil fs-6"></i>
          </button>
      `;
    } else {
      buttonsHtml = `
      <button class="btn btn-primary btn-sm" onclick="openViewModal('server',${index})"
            data-bs-toggle="tooltip" data-bs-placement="top" title="Server Detail" data-bs-custom-class="tooltip-inverse"
            id="editServerBtn">
                <i class="bi bi-eye fs-6"></i>
          </button>
      `;
    }
    serverRow.innerHTML = `
    <th scope="row">${index + 1}</th>
    <td>
        ${!!server.server_name ? server.server_name : "-"}
    </td>
    <td>
        ${!!server.ip_address ? server.ip_address : "-"}
    </td>
    <td style="text-align: center;">
        <div style="display: flex; justify-content: center; align-items: center; gap:6px; flex-direction:row;">
            ${buttonsHtml}
            
        </div>
    </td>
`;
    serverTableBody.appendChild(serverRow);

    if (server.surrounding_servers.length > 0)
      server.surrounding_servers.forEach((surroundingServer, sIndex) => {
        const isHideAddService =
          surroundingServer.type == "" || surroundingServer.type == "db";
        const surroundingRow = document.createElement("tr");
        surroundingRow.style.backgroundColor = colors[1];
        let buttonsHtml = "";
        if (pathUrl !== "view") {
          buttonsHtml = `
          <button class="btn btn-success btn-sm ${
            isHideAddService ? "d-none" : ""
          }" onclick="addService(${index}, ${sIndex})"
          data-bs-toggle="tooltip" data-bs-placement="top" title="Add Service" data-bs-custom-class="tooltip-inverse"
              >
                  <i class="bi bi-plus fs-6"></i>
              </button>
              <button class="btn btn-danger btn-sm" onclick="confirmRemoveSurroundingServer(${index}, ${sIndex})"
            data-bs-toggle="tooltip" data-bs-placement="top" title="Remove Surrounding Server" data-bs-custom-class="tooltip-inverse"
              
              >
                  <i class="bi bi-trash3 fs-6"></i>
              </button>
              <button class="btn btn-warning btn-sm" onclick="openServerModal_('surrounding_server' , ${index} , ${sIndex})"
            data-bs-toggle="tooltip" data-bs-placement="top" title="Surrounding Server Detail" data-bs-custom-class="tooltip-inverse"
              
              >
                <i class="bi bi-pencil fs-6"></i>
              </button>
          `;
        } else {
          buttonsHtml = `
          <button class="btn btn-primary btn-sm" onclick="openViewModal('surrounding_server' , ${index} , ${sIndex})"
                data-bs-toggle="tooltip" data-bs-placement="top" title="Server Detail" data-bs-custom-class="tooltip-inverse"
                id="editServerBtn">
                    <i class="bi bi-eye fs-6"></i>
              </button>
          `;
        }
        surroundingRow.innerHTML = `
                <th scope="row">${index + 1}.${sIndex + 1}</th>
                <td>
                ${
                  surroundingServer.surrounding_server_name
                    ? surroundingServer.surrounding_server_name
                    : "-"
                }
                </td>
                <td>
                ${
                  surroundingServer.ip_address
                    ? surroundingServer.ip_address
                    : "-"
                }
                </td>
                <td style="text-align: center;">
                    <div style="display: flex; justify-content: center; align-items: center; gap:6px;">
                        ${buttonsHtml}
                    </div>
                </td>
`;
        serverTableBody.appendChild(surroundingRow);

        if (surroundingServer.services.length > 0)
          surroundingServer.services.forEach((service, serviceIndex) => {
            const serviceRow = document.createElement("tr");
            serviceRow.style.backgroundColor = colors[2];
            const isValidServiceData =
              service.service_name != null && service.port != null;
            let buttonsHtml = "";
            if (pathUrl !== "view") {
              buttonsHtml = `
              <button class="btn btn-danger btn-sm" onclick="confirmRemoveService(${index}, ${sIndex}, ${serviceIndex})"
              data-bs-toggle="tooltip" data-bs-placement="top" title="Delete Service" data-bs-custom-class="tooltip-inverse"
              >
                  <i class="bi bi-trash3 fs-6"></i>
              </button>
              <button class="btn btn-warning btn-sm" onclick="showServiceModal_(${index}, ${sIndex}, ${serviceIndex})"
              data-bs-toggle="tooltip" data-bs-placement="top" title="Service Detail" data-bs-custom-class="tooltip-inverse"
              >
                <i class="bi bi-pencil fs-6"></i>
              </button>
              `;
            } else {
              buttonsHtml = `
              <button class="btn btn-primary btn-sm" onclick="showViewServiceModal(${index}, ${sIndex}, ${serviceIndex})"
                    data-bs-toggle="tooltip" data-bs-placement="top" title="Server Detail" data-bs-custom-class="tooltip-inverse"
                    id="editServerBtn">
                        <i class="bi bi-eye fs-6"></i>
                  </button>
              `;
            }

            serviceRow.innerHTML = `
              <th scope="row">${index + 1}.${sIndex + 1}.${
              serviceIndex + 1
            }</th>
              <td colspan=2>
              <span class="fs-6">
              ${
                isValidServiceData
                  ? `${service.service_name}:${service.port}`
                  : "Please add service information"
              }
              </span>
              </td>
              <td style="text-align: center;">
                    <div style="display: flex; justify-content: center; align-items: center; gap:6px;">
                        ${buttonsHtml}
                    </div>
                </td>
              `;

            // Append the main cell to the row
            serverTableBody.appendChild(serviceRow);
          });
      });
  });
  const tooltipTriggerList = [].slice.call(
    document.querySelectorAll('[data-bs-toggle="tooltip"]')
  );
  const tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
    return new bootstrap.Tooltip(tooltipTriggerEl);
  });
}
