export async function postDiagramV2({ data, token }) {
  try {
    console.log(data);
    const response = await fetch(`/api/v2_applications`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        accept: "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(data),
    });

    if (!response.ok) {
      throw new Error(`HTTP error! Status: ${response.status}`);
    }

    Swal.fire({
      text: "Diagram has been saved!",
      icon: "success",
      buttonsStyling: false,
      confirmButtonText: "Done",
      customClass: {
        confirmButton: "btn btn-primary",
      },
    }).then((result) => {
      if (result.isConfirmed) {
        window.location.href = "/application";
      }
    });
  } catch (error) {
    Swal.fire({
      text: "An error occurred while saving the diagram.",
      icon: "error",
      buttonsStyling: false,
      confirmButtonText: "Okay",
      customClass: {
        confirmButton: "btn btn-primary",
      },
    });
    console.error(error);
  }
}

export const getAppByIdApiV2 = async ({ id, token }) => {
  try {
    const response = await fetch(`/api/v2_applications/${id}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        accept: "application/json",
        Authorization: `Bearer ${token}`,
      },
    });
    if (!response.ok) {
      throw Error("error");
    }
    const data = await response.json();
    return data;
    // return await response.json();
  } catch (error) {
    console.log(error);
  }
};

export async function updateAppByIdV2({ id, data, token }) {
  try {
    const response = await fetch(`/api/v2_applications/${id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        accept: "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(data),
    });

    if (!response.ok) {
      throw Error("error");
    }
    Swal.fire({
      text: "Diagram has been save!",
      icon: "success",
      buttonsStyling: false,
      confirmButtonText: "Done",
      customClass: {
        confirmButton: "btn btn-primary",
      },
    }).then((result) => {
      if (result.isConfirmed) {
        window.location.href = "/application";
      }
    });
  } catch (error) {
    Swal.fire({
      text: "An error occurred while updating the diagram.",
      icon: "error",
      buttonsStyling: false,
      confirmButtonText: "Okay",
      customClass: {
        confirmButton: "btn btn-primary",
      },
    });
    console.error(error);
  }
}
