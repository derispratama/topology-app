import { getToken } from "../auth.js";
import { getAppByIdApiV2, postDiagramV2, updateAppByIdV2 } from "./api.js";

var myDiagram;
var myPalette;
let currentNodeType = "server";
async function init() {
  const token = getToken();
  const pathUrl = getPathUrl();

  const user = JSON.parse(localStorage.getItem("user"));
  const saveBtn = document.getElementById("save_diagram_btn");
  // var $ = go.GraphObject.make;

  document.getElementById("departement").value =
    user.department.departement_name;
  myDiagram = new go.Diagram(
    "myDiagramDiv", // create a Diagram for the HTML Div element
    {
      "commandHandler.archetypeGroupData": {
        isGroup: true,
        text: "Subnet",
      },
      "undoManager.isEnabled": true,
      // "animationManager.isEnabled": false,

      grid: new go.Panel("Grid", {
        gridCellSize: new go.Size(10, 10),
      }),
    }
  ); // enable undo & redo
  myDiagram.toolManager.draggingTool.isGridSnapEnabled = true;
  myDiagram.toolManager.mouseDownTools.add(new LinkShiftingTool());

  myDiagram.nodeTemplate = new go.Node("Vertical", {
    locationSpot: go.Spot.Center,
    locationObjectName: "BODY",
    selectionObjectName: "BODY",
  })
    .bindTwoWay("location", "loc", go.Point.parse, go.Point.stringify)
    .add(
      new go.Panel("Spot", {}).add(
        new go.Picture({
          name: "BODY",
          width: 50,
          height: 50,
          portId: "top",
          fromLinkable: true,
          toLinkable: true,
          cursor: "pointer",
        }).bind(
          "source",
          "type",
          (t) => `images/network/${t.toLowerCase()}.svg`
        ),
        new go.Shape({
          width: 25,
          height: 25,
          fill: "transparent",
          strokeWidth: 0,
        })
      )
    )
    .add(
      new go.TextBlock({
        font: "12px Lato, sans-serif",
        textAlign: "center",
        margin: 3,
        maxSize: new go.Size(70, NaN),
        portId: "bottom",
        fromLinkable: true,
        toLinkable: true,
        alignment: go.Spot.Bottom,
        background: "white",
        editable: true,
      }).bind(new go.Binding("text").makeTwoWay())
    );

  myDiagram.groupTemplate = new go.Group("Vertical", {
    locationSpot: go.Spot.Center,
    padding: 5,
    background: "transparent",
  })
    .add(
      new go.TextBlock({
        alignment: go.Spot.Left,
        font: "12px georgia",
        editable: true,
        // background: "red",
      }).bindTwoWay("text"),
      new go.Panel("Auto").add(
        new go.Shape("RoundedRectangle", {
          strokeDashArray: [2, 6],
          stroke: "#333",
          fill: "rgba(0,0,0,0)",
        }),
        new go.Placeholder({
          padding: 5,
        })
      )
    )
    .bind("background", "bgcolor");

  myDiagram.linkTemplate = new go.Link({
    reshapable: true,
    resegmentable: true,
    relinkableFrom: true,
    relinkableTo: true,
    adjusting: go.LinkAdjusting.Stretch,
  })
    // remember the (potentially) user-modified route
    .bindTwoWay("points")
    // remember any spots modified by LinkShiftingTool
    .bindTwoWay("fromSpot", "fromSpot", go.Spot.parse, go.Spot.stringify)
    .bindTwoWay("toSpot", "toSpot", go.Spot.parse, go.Spot.stringify)
    .add(new go.Shape(), new go.Shape({ toArrow: "Standard" }));

  myPalette = new go.Palette("myPaletteDiv", {
    nodeTemplateMap: myDiagram.nodeTemplateMap,
    layout: new go.GridLayout({
      cellSize: new go.Size(2, 2),
      isViewportSized: true,
    }),
  });

  myPalette.model.nodeDataArray = [
    {
      type: "blank",
      text: " ",
    },
    {
      type: "Server",
      text: "Server",
      ip: "",
      os: "",
      cpu: "",
      ram: "",
      memory: "",
      app_list: "",
    },
    {
      type: "load-balancer",
      text: "Load Balancer",
      ip: "",
    },
    {
      type: "Cloud",
      text: "Cloud",
    },
    {
      type: "Firewall",
      text: "Firewall",
    },
    {
      type: "service",
      text: "Service",
      port: "",
      path_url: "",
      request_params: "",
      response_params: "",
    },
    {
      type: "database",
      text: "Database",
      ip: "",
      os: "",
      cpu: "",
      ram: "",
      memory: "",
      app_list: "",
    },
  ];

  const postDiagram = async () => {
    const json = document.getElementById("modelJson");

    const data = JSON.parse(json.textContent);
    if (pathUrl === "edit") {
      await updateAppByIdV2({
        id: appId,
        data: {
          application_name: document.getElementById("application_name").value,
          id_department: user.id_departement,
          gojs_meta: JSON.stringify(data),
          version: "1.0.0",
        },
        token,
      });
    } else {
      await postDiagramV2({
        data: {
          application_name: document.getElementById("application_name").value,
          id_department: user.id_departement,
          gojs_meta: JSON.stringify(data),
          version: "1.0.0",
        },
        token,
      });
    }
  };
  saveBtn.addEventListener("click", postDiagram);

  window.save = function () {
    const json = document.getElementById("modelJson");
    json.textContent = myDiagram.model.toJson();
    myDiagram.isModified = false;
  };
  window.load = function () {
    const json = document.getElementById("modelJson");
    myDiagram.model = go.Model.fromJson(json.textContent);
  };

  window.group = function () {
    const selectedParts = myDiagram.selection;
    if (selectedParts.count < 2) return; // need at least 2 selected parts to group

    myDiagram.startTransaction("group");

    // Create a new group node
    const group = {
      key: myDiagram.model.nodeDataArray.length + 1,
      isGroup: true,
      text: "New Group",
    };

    // Add the new group to the model
    myDiagram.model.addNodeData(group);

    // Add the selected parts to the new group
    selectedParts.each(function (part) {
      if (part instanceof go.Node) {
        myDiagram.model.setDataProperty(part.data, "group", group.key);
      }
    });

    myDiagram.commitTransaction("group");
  };

  window.ungroup = function () {
    var selectedParts = myDiagram.selection;
    if (selectedParts.count !== 1) return; // need exactly 1 selected part to ungroup

    var selectedGroup = selectedParts.first();
    if (!(selectedGroup instanceof go.Group)) return; // must be a group

    // Ungroup by moving its members out and removing the group
    selectedGroup.memberParts.each(function (part) {
      if (part instanceof go.Node) {
        myDiagram.model.addNodeData(part.data); // Add the member to the model
      }
    });

    myDiagram.model.removeNodeData(selectedGroup.data); // Remove the group from the model
    myDiagram.commitTransaction("ungroup");
  };
  load();

  const serverProperties = document.getElementById("server-properties");
  const loadBalancerProperties = document.getElementById(
    "load-balancer-properties"
  );
  const databaseProperties = document.getElementById("database-properties");
  const serviceProperties = document.getElementById("service-properties");

  const resetProperties = (properties = []) => {
    properties.forEach((property) => {
      document.getElementById(property).value = "";
    });
  };

  // Function to show properties based on selected node type
  function updateProperties(node) {
    // Hide all property sections
    serverProperties.classList.add("d-none");
    loadBalancerProperties.classList.add("d-none");
    databaseProperties.classList.add("d-none");
    serviceProperties.classList.add("d-none");
    document.getElementById("group-properties").classList.add("d-none");

    console.log(node.data);
    propertiesDiv.classList.remove("d-none");

    // Show relevant property section
    switch (node.data.type) {
      case "Server":
        serverProperties.classList.remove("d-none");
        resetProperties();
        document.getElementById("server-name").value = node.data.text || "";
        document.getElementById("server-ip").value = node.data.ip || "";
        document.getElementById("server-os").value = node.data.os || "";
        document.getElementById("server-cpu").value = node.data.cpu || "";
        document.getElementById("server-ram").value = node.data.ram || "";
        document.getElementById("server-memory").value = node.data.memory || "";
        document.getElementById("server-apps").value = node.data.app_list || "";
        break;
      case "load-balancer":
        loadBalancerProperties.classList.remove("d-none");
        document.getElementById("loadbalancer-name").value =
          node.data.text || "";
        document.getElementById("loadbalancer-ip").value = node.data.ip || "";
        break;
      case "database":
        databaseProperties.classList.remove("d-none");
        document.getElementById("database-name").value = node.data.text || "";
        document.getElementById("database-os").value = node.data.os || "";
        document.getElementById("database-ip").value = node.data.ip || "";
        document.getElementById("database-cpu").value = node.data.cpu || "";
        document.getElementById("database-ram").value = node.data.ram || "";
        document.getElementById("database-memory").value =
          node.data.memory || "";
        document.getElementById("database-apps").value =
          node.data.app_list || "";
        break;
      case "service":
        serviceProperties.classList.remove("d-none");
        document.getElementById("service-name").value = node.data.text || "";
        document.getElementById("service-port").value = node.data.port || "";
        document.getElementById("service-path-url").value =
          node.data.path_url || "";
        document.getElementById("service-request-params").value =
          node.data.request_params || "";
        document.getElementById("service-response-params").value =
          node.data.response_params || "";
        break;
    }
    if (node.data.isGroup && node.data.type !== "service") {
      document.getElementById("group-properties").classList.remove("d-none");
      document.getElementById("group-name").value = node.data.text || "";
      document.getElementById("group-bgcolor").value = node.data.bgcolor || "";
    }
  }
  const updateNodeData = () => {
    const nodeKey = myDiagram.selection.first().data.key; // Get the key of the selected node
    const nodeData = myDiagram.model.findNodeDataForKey(nodeKey);
    console.log("a", nodeData);
    if (nodeData) {
      const nodeType = nodeData.type;
      myDiagram.model.startTransaction("updateNodeData");

      switch (nodeType) {
        case "Server":
          myDiagram.model.setDataProperty(
            nodeData,
            "text",
            document.getElementById("server-name").value
          );
          myDiagram.model.setDataProperty(
            nodeData,
            "ip",
            document.getElementById("server-ip").value
          );
          myDiagram.model.setDataProperty(
            nodeData,
            "os",
            document.getElementById("server-os").value
          );
          myDiagram.model.setDataProperty(
            nodeData,
            "cpu",
            document.getElementById("server-cpu").value
          );
          myDiagram.model.setDataProperty(
            nodeData,
            "ram",
            document.getElementById("server-ram").value
          );
          myDiagram.model.setDataProperty(
            nodeData,
            "memory",
            document.getElementById("server-memory").value
          );
          myDiagram.model.setDataProperty(
            nodeData,
            "app_list",
            document.getElementById("server-apps").value
          );
          break;
        case "load-balancer":
          myDiagram.model.setDataProperty(
            nodeData,
            "text",
            document.getElementById("loadbalancer-name").value
          );
          myDiagram.model.setDataProperty(
            nodeData,
            "ip",
            document.getElementById("loadbalancer-ip").value
          );
          break;
        case "database":
          myDiagram.model.setDataProperty(
            nodeData,
            "text",
            document.getElementById("database-name").value
          );
          myDiagram.model.setDataProperty(
            nodeData,
            "os",
            document.getElementById("database-os").value
          );
          myDiagram.model.setDataProperty(
            nodeData,
            "ip",
            document.getElementById("database-ip").value
          );
          myDiagram.model.setDataProperty(
            nodeData,
            "cpu",
            document.getElementById("database-cpu").value
          );
          myDiagram.model.setDataProperty(
            nodeData,
            "ram",
            document.getElementById("database-ram").value
          );
          myDiagram.model.setDataProperty(
            nodeData,
            "memory",
            document.getElementById("database-memory").value
          );
          myDiagram.model.setDataProperty(
            nodeData,
            "app_list",
            document.getElementById("database-apps").value
          );
          break;
        case "service":
          myDiagram.model.setDataProperty(
            nodeData,
            "text",
            document.getElementById("service-name").value
          );
          myDiagram.model.setDataProperty(
            nodeData,
            "port",
            document.getElementById("service-port").value
          );
          myDiagram.model.setDataProperty(
            nodeData,
            "path_url",
            document.getElementById("service-path-url").value
          );
          myDiagram.model.setDataProperty(
            nodeData,
            "request_params",
            document.getElementById("service-request-params").value
          );
          myDiagram.model.setDataProperty(
            nodeData,
            "response_params",
            document.getElementById("service-response-params").value
          );
          break;
        // Add cases for other node types as needed
      }

      if (nodeData.isGroup === true) {
        console.log(nodeData);
        myDiagram.model.setDataProperty(
          nodeData,
          "text",
          document.getElementById("group-name").value
        );
        myDiagram.model.setDataProperty(
          nodeData,
          "bgcolor",
          document.getElementById("group-bgcolor").value
        );
      }

      myDiagram.model.commitTransaction("updateNodeData");
    }
  };

  document
    .getElementById("server-name")
    .addEventListener("input", updateNodeData);
  document
    .getElementById("server-ip")
    .addEventListener("input", updateNodeData);
  document
    .getElementById("server-os")
    .addEventListener("input", updateNodeData);
  document
    .getElementById("server-cpu")
    .addEventListener("input", updateNodeData);
  document
    .getElementById("server-ram")
    .addEventListener("input", updateNodeData);
  document
    .getElementById("server-memory")
    .addEventListener("input", updateNodeData);
  document
    .getElementById("server-apps")
    .addEventListener("input", updateNodeData);

  document
    .getElementById("loadbalancer-name")
    .addEventListener("input", updateNodeData);
  document
    .getElementById("loadbalancer-ip")
    .addEventListener("input", updateNodeData);

  document
    .getElementById("database-name")
    .addEventListener("input", updateNodeData);
  document
    .getElementById("database-os")
    .addEventListener("input", updateNodeData);
  document
    .getElementById("database-ip")
    .addEventListener("input", updateNodeData);

  document
    .getElementById("database-cpu")
    .addEventListener("input", updateNodeData);
  document
    .getElementById("database-ram")
    .addEventListener("input", updateNodeData);
  document
    .getElementById("database-memory")
    .addEventListener("input", updateNodeData);
  document
    .getElementById("database-apps")
    .addEventListener("input", updateNodeData);

  document
    .getElementById("service-name")
    .addEventListener("input", updateNodeData);
  document
    .getElementById("service-port")
    .addEventListener("input", updateNodeData);
  document
    .getElementById("service-path-url")
    .addEventListener("input", updateNodeData);

  document
    .getElementById("service-request-params")
    .addEventListener("input", updateNodeData);

  document
    .getElementById("service-response-params")
    .addEventListener("input", updateNodeData);

  document
    .getElementById("group-name")
    .addEventListener("input", updateNodeData);

  document
    .getElementById("group-bgcolor")
    .addEventListener("input", updateNodeData);

  // Add a listener to update properties when a node is selected
  myDiagram.addDiagramListener("SelectionMoved", function (e) {
    save();
  });
  myDiagram.addDiagramListener("LinkReshaped", function (e) {
    save();
  });
  myDiagram.addDiagramListener("ChangedSelection", function (e) {
    save();
    // load();
    var node = e.diagram.selection.first();
    if (node instanceof go.Node) {
      updateProperties(node);
    } else {
      propertiesDiv.classList.add("d-none");
    }
  });
  const appId = getId();
  if (pathUrl === "edit") {
    if (appId && appId !== "") {
      const data = await getAppByIdApiV2({ id: appId, token });
      document.getElementById("modelJson").innerHTML = data.data.gojs_meta;
      document.getElementById("application_name").value =
        data.data.application_name;
      load();
    }
  }
}

window.addEventListener("DOMContentLoaded", init);
