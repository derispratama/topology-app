import { getToken } from "../auth.js";
import { getAppByIdApiV2 } from "./api.js";
var myDiagram;

window.addEventListener("DOMContentLoaded", async () => {
  const appId = getId();
  const token = getToken();
  console.log(appId);

  const getAppDetail = async () => {
    const data = await getAppByIdApiV2({ id: appId, token });
    return data;
  };

  const appDetail = await getAppDetail();
  console.log(appDetail);

  myDiagram = new go.Diagram(
    "myDiagramDiv", // create a Diagram for the HTML Div element
    {
      "commandHandler.archetypeGroupData": {
        isGroup: true,
        text: "Subnet",
      },
      "undoManager.isEnabled": true,
      isReadOnly: true,
      // "animationManager.isEnabled": false,

      grid: new go.Panel("Grid", {
        gridCellSize: new go.Size(10, 10),
      }),
    }
  ); // enable undo & redo
  myDiagram.toolManager.draggingTool.isGridSnapEnabled = true;
  myDiagram.toolManager.mouseDownTools.add(new LinkShiftingTool());

  myDiagram.nodeTemplate = new go.Node("Vertical", {
    locationSpot: go.Spot.Center,
    locationObjectName: "BODY",
    selectionObjectName: "BODY",
    movable: false,
  })
    .bindTwoWay("location", "loc", go.Point.parse, go.Point.stringify)
    .add(
      new go.Panel("Spot", {}).add(
        new go.Picture({
          name: "BODY",
          width: 50,
          height: 50,
          portId: "top",
          fromLinkable: true,
          toLinkable: true,
          cursor: "pointer",
        }).bind(
          "source",
          "type",
          (t) => `images/network/${t.toLowerCase()}.svg`
        ),
        new go.Shape({
          width: 25,
          height: 25,
          fill: "transparent",
          strokeWidth: 0,
        })
      )
    )
    .add(
      new go.TextBlock({
        font: "12px Lato, sans-serif",
        textAlign: "center",
        margin: 3,
        maxSize: new go.Size(70, NaN),
        portId: "bottom",
        fromLinkable: true,
        toLinkable: true,
        alignment: go.Spot.Bottom,
        background: "white",
        editable: false,
      }).bind(new go.Binding("text").makeTwoWay())
    );

  myDiagram.groupTemplate = new go.Group("Vertical", {
    locationSpot: go.Spot.Center,
    padding: 5,
    background: "transparent",
  })
    .add(
      new go.TextBlock({
        alignment: go.Spot.Left,
        font: "12px georgia",
        editable: true,
        // background: "red",
      }).bindTwoWay("text"),
      new go.Panel("Auto").add(
        new go.Shape("RoundedRectangle", {
          strokeDashArray: [2, 6],
          stroke: "#333",
          fill: "rgba(0,0,0,0)",
        }),
        new go.Placeholder({
          padding: 5,
        })
      )
    )
    .bind("background", "bgcolor");

  myDiagram.linkTemplate = new go.Link({
    reshapable: true,
    resegmentable: true,
    relinkableFrom: true,
    relinkableTo: true,
    adjusting: go.LinkAdjusting.Stretch,
  })
    // remember the (potentially) user-modified route
    .bindTwoWay("points")
    // remember any spots modified by LinkShiftingTool
    .bindTwoWay("fromSpot", "fromSpot", go.Spot.parse, go.Spot.stringify)
    .bindTwoWay("toSpot", "toSpot", go.Spot.parse, go.Spot.stringify)
    .add(new go.Shape(), new go.Shape({ toArrow: "Standard" }));

  const load = function () {
    const json = document.getElementById("modelJson");
    myDiagram.model = go.Model.fromJson(json.textContent);
  };

  document.getElementById("modelJson").innerHTML = appDetail.data.gojs_meta;
  load();
});
