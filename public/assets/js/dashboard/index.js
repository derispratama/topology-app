import { getToken } from "../auth.js";
import { privateRoute } from "../utils.js";
import { getDashboardData } from "./api.js";
import { renderApplicationChart, renderCardInfo } from "./chart.js";

document.addEventListener("DOMContentLoaded", async function () {
  const token = getToken();
  let user = JSON.parse(localStorage.getItem("user"));

  privateRoute(user);

  const dashboardData = await getDashboardData({ token });

  renderCardInfo(dashboardData);
  renderApplicationChart(dashboardData);
});
