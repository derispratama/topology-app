export const renderApplicationChart = (data) => {
  const applications = data.data.applications.map((application) => {
    return application.total;
  });
  const departments = data.data.applications.map((department) => {
    return department.name;
  });

  const ids = data.data.applications.map((application) => {
    return application.id;
  });
  var options = {
    series: [
      {
        data: applications,
      },
    ],
    chart: {
      type: "bar",
      height: 350,
      events: {
        dataPointSelection: function (event, chartContext, config) {
          window.location.href = `/application/?id=${
            ids[config.dataPointIndex]
          }`;
        },
      },
    },
    plotOptions: {
      bar: {
        borderRadius: 4,
        borderRadiusApplication: "end",
        horizontal: true,
      },
    },
    dataLabels: {
      enabled: true,
    },
    xaxis: {
      categories: departments,
    },
  };

  var chart = new ApexCharts(document.querySelector("#chart"), options);
  chart.render();
};

export const renderCardInfo = (dashboardData) => {
  const cardContainer = document.getElementById("card-container");
  cardContainer.innerHTML = "";
  dashboardData.data.card.forEach((item) => {
    const cardHtml = `
    <div class="col-sm mb-2">
        <a href="${item.url}" class="card hover-elevate-up shadow-sm parent-hover h-100">
            <div class="card-body d-flex align-items-center justify-content-start">
                <i class="bi ${item.icon}" style="font-size:35px;"></i>
                <span class="ms-3 text-gray-700 parent-hover-primary fs-6 fw-bold">
                    ${item.title} : ${item.count}
                </span>
            </div>
        </a>
    </div>
    `;
    cardContainer.innerHTML += cardHtml;
  });
};
