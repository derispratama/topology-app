<!DOCTYPE html>
<!--
Author: Keenthemes
Product Name: Metronic
Product Version: 8.2.0
Purchase: https://1.envato.market/EA4JP
Website: http://www.keenthemes.com
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
License: For each use you must have a valid license purchased only from above link in order to legally use the theme for your project.
-->
<html lang="en">
<!--begin::Head-->

<head>
    <base href="../../../" />
    <title>Topology Apps</title>
    <meta charset="utf-8" />
    <meta name="description"
        content="The most advanced Bootstrap 5 Admin Theme with 40 unique prebuilt layouts on Themeforest trusted by 100,000 beginners and professionals. Multi-demo, Dark Mode, RTL support and complete React, Angular, Vue, Asp.Net Core, Rails, Spring, Blazor, Django, Express.js, Node.js, Flask, Symfony & Laravel versions. Grab your copy now and get life-time updates for free." />
    <meta name="keywords"
        content="metronic, bootstrap, bootstrap 5, angular, VueJs, React, Asp.Net Core, Rails, Spring, Blazor, Django, Express.js, Node.js, Flask, Symfony & Laravel starter kits, admin themes, web design, figma, web development, free templates, free admin themes, bootstrap theme, bootstrap template, bootstrap dashboard, bootstrap dak mode, bootstrap button, bootstrap datepicker, bootstrap timepicker, fullcalendar, datatables, flaticon" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta property="og:title"
        content="Metronic - Bootstrap Admin Template, HTML, VueJS, React, Angular. Laravel, Asp.Net Core, Ruby on Rails, Spring Boot, Blazor, Django, Express.js, Node.js, Flask Admin Dashboard Theme & Template" />
    <!-- <meta property="og:url" content="https://keenthemes.com/metronic" /> -->

    <meta property="og:site_name" content="Keenthemes | Metronic" />
    <!-- <link rel="canonical" href="https://preview.keenthemes.com/metronic8" /> -->
    <link rel="shortcut icon" href="assets/media/logos/logo-bsi.png" />
    <!--begin::Fonts(mandatory for all pages)-->
    <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600,700" /> -->
    <!--end::Fonts-->
    <!--begin::Global Stylesheets Bundle(mandatory for all pages)-->


    <link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet">

    <!--end::Global Stylesheets Bundle-->
    <script>
        // Frame-busting to prevent site from being loaded within a frame without permission (click-jacking) if (window.top != window.self) { window.top.location.replace(window.self.location.href); }
    </script>
</head>
<!--end::Head-->
<!--begin::Body-->

<body id="kt_body" class="auth-bg bgi-size-cover bgi-attachment-fixed bgi-position-center">
    @csrf
    <!--begin::Theme mode setup on page load-->
    <!--end::Theme mode setup on page load-->
    <!--begin::Main-->
    <!--begin::Root-->
    <div class="d-flex flex-column flex-root">
        <!--begin::Page bg image-->
        <style>
            body {
                background-image: url('assets/media/auth/bg01.jpg');
            }

            [data-bs-theme="dark"] body {
                background-image: url('assets/media/auth/bg10-dark.jpeg');
            }
        </style>
        <!--end::Page bg image-->
        <!--begin::Authentication - Sign-in -->
        <div class="d-flex flex-column flex-lg-row flex-column-fluid">
            <!--begin::Aside-->
            <div class="d-flex flex-lg-row-fluid">
                <!--begin::Content-->
                <div class="d-flex flex-column flex-center pb-0 pb-lg-10 p-10 w-100">
                    <!--begin::Image-->
                    <!--<img class="theme-light-show mx-auto mw-100 w-150px w-lg-400px mb-10 mb-lg-20" src="assets/media/auth/winner.png" alt="" />-->
                    <img class="theme-dark-show mx-auto mw-100 w-150px w-lg-400px mb-10 mb-lg-20"
                        src="assets/media/auth/agency-dark.png" alt="" />
                    <!--end::Image-->
                    <!--begin::Title-->
                    <!--<h6 class="text-white-800 fs-2qx fw-bold text-center mb-5">Financing with Risk to Zero</h6>-->
                    <!--end::Title-->
                    <!--begin::Text-->
                    <!--<div class="text-white-600 fs-base text-center fw-semibold">The updated system is expected to help business and marketing look
      <br />  for potential customers for financing and carry out screening to
      <br /> maintain the stability of customer collectability.</div>-->
                    <!--end::Text-->
                </div>
                <!--end::Content-->
            </div>
            <!--begin::Aside-->
            <!--begin::Body-->
            <div class="d-flex flex-column-fluid flex-lg-row-auto justify-content-center justify-content-lg-end p-12">
                <!--begin::Wrapper-->
                <div class="bg-body d-flex flex-column flex-center rounded-4 w-md-600px p-10">
                    <!--begin::Content-->
                    <div class="d-flex flex-center flex-column align-items-stretch h-lg-100 w-md-400px">
                        <!--begin::Wrapper-->
                        <div class="d-flex flex-center flex-column flex-column-fluid pb-15 pb-lg-20 form w-100">
                            <!--begin::Form-->
                            <form class="form w-100" action="" method="POST" class="needs-validation"
                                enctype="multipart/form-data">
                                <!--begin::Image-->
                                @csrf
                                <div class="d-flex">
                                    <img class="theme-light-show mx-auto mw-100 w-100px w-lg-200px mb-10 mb-lg-10"
                                        src="assets/media/logos/logo-bsi.png" alt="" />
                                    <img class="theme-dark-show mx-auto mw-100 w-150px w-lg-100px mb-10 mb-lg-20"
                                        src="assets/media/auth/agency-dark.png" alt="" />
                                </div>
                                <!--end::Image-->
                                <!--begin::Heading-->
                                <div class="text-center mb-11">
                                    <!--begin::Title-->
                                    <h1 class="text-dark fw-bolder mb-3">Welcome to <a class=""
                                            style="color:orange">Topology Apps</a></h1>
                                    <!--end::Title-->
                                    <div class="alert alert-info">
                                        <div class="alert-text ">Login Use Your Credentials</div>
                                    </div>

                                    <div class="alert alert-danger d-flex align-items-center d-none" role="alert"
                                        id="alert-login">
                                        <div>
                                            Failed to Login
                                        </div>
                                    </div>
                                </div>
                                <!--end::Heading-->

                                <!--begin::Input group=-->
                                <div class="fv-row mb-8">
                                    <!--begin::Email-->
                                    <input type="text" id="username" placeholder="Username" name="username"
                                        autocomplete="off" class="form-control bg-transparent" />
                                    <!--end::Email-->
                                    <small class="text text-danger"></small>
                                </div>
                                <!--end::Input group=-->
                                <div class="fv-row mb-3">
                                    <!--begin::Password-->
                                    <input type="password" id="password" placeholder="Password" name="password"
                                        autocomplete="off" class="form-control bg-transparent" />
                                    <small class="text text-danger"></small>
                                    <!--end::Password-->
                                </div>
                                <!--end::Input group=-->

                                <!--begin::Submit button-->
                                <div class="d-grid mb-10 fv-row mb-3" id="">
                                    <button type="button" id="btn_login" class="btn btn-primary">
                                        <!--begin::Indicator label-->
                                        <span class="indicator-label">Sign In</span>
                                        <!--end::Indicator label-->
                                        <!--begin::Indicator progress-->
                                        <span class="indicator-progress">Please wait...
                                            <span
                                                class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                        <!--end::Indicator progress-->
                                    </button>
                                </div>
                                <!--end::Submit button-->
                            </form>

                            <!--end::Form-->
                        </div>
                        <!--end::Wrapper-->

                        <!--end::Footer-->
                    </div>
                    <!--end::Content-->
                </div>
                <!--end::Wrapper-->
            </div>
            <!--end::Body-->
        </div>
        <!--end::Authentication - Sign-in-->
    </div>
    <!--end::Root-->
    <!--end::Main-->
    <!--begin::Javascript-->

    <!--begin::Global Javascript Bundle(mandatory for all pages)-->

    <!-- <script src="assets/js/scripts.bundle.js"></script> -->
    <script src="assets/js/jquery.js"></script>
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
    <!--end::Global Javascript Bundle-->
    <!--begin::Custom Javascript(used for this page only)-->
    <!--end::Custom Javascript-->
    <!--end::Javascript-->
    <script>
        var defaultThemeMode = "light";
        var themeMode;
        if (document.documentElement) {
            if (document.documentElement.hasAttribute("data-bs-theme-mode")) {
                themeMode = document.documentElement.getAttribute("data-bs-theme-mode");
            } else {
                if (localStorage.getItem("data-bs-theme") !== null) {
                    themeMode = localStorage.getItem("data-bs-theme");
                } else {
                    themeMode = defaultThemeMode;
                }
            }
            if (themeMode === "system") {
                themeMode = window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light";
            }
            document.documentElement.setAttribute("data-bs-theme", themeMode);
        }
    </script>

    <script>
        $('#btn_login').on('click', function() {
            const username = $('#username').val();
            const password = $('#password').val();

            $('small').html('');
            $.ajax({
                url: '/api/login',
                method: 'post',
                data: {
                    _token: "{{ csrf_token() }}",
                    username,
                    password,
                },
                success(res) {
                    $('#alert-login').addClass('d-none');
                    if (res.status == true) {
                        localStorage.setItem("token", res.token);
                        localStorage.setItem("user", JSON.stringify(res.user));
                        let url = "{{ route('application') }}";
                        if (res.user['roles']['role_name'] === 'superadmin')
                            url = "{{ route('dashboard') }}";
                        document.location.href = url;
                    }
                },
                error(res) {
                    if (res.responseJSON.errors.errorlogin) {
                        $('#alert-login').removeClass('d-none');
                    } else {
                        $('#alert-login').addClass('d-none');
                        for (const obj in res.responseJSON.errors) {
                            $(`#${obj}`).siblings('small').html(res.responseJSON.errors[obj][0]);
                        }
                    }
                }
            });
        });
    </script>
</body>
<!--end::Body-->

</html>




{{-- <html>
  <body>
    Here is one mermaid diagram:
    <pre class="mermaid">graph&nbsp;TD&#13;A[Client]&nbsp;-->B[Load Balancer]&#13;B&nbsp;-->&nbsp;C[Server1]&#13;B&nbsp;-->&nbsp;D[Server2]</pre>
    And here is another:
    <pre class="mermaid">
        graph&nbsp;TD&#13;A[Client]&nbsp;-->|tcp_123|&nbsp;B&#13;B(Load Balancer)&#13;B&nbsp;-->|tcp_456|&nbsp;C[Server1]&#13;B&nbsp;-->|tcp_456|&nbsp;D[Server2]&#13;
    </pre>

    <script type="module">
      import mermaid from 'https://cdn.jsdelivr.net/npm/mermaid@10/dist/mermaid.esm.min.mjs';
      mermaid.initialize({ startOnLoad: true });
    </script>
  </body>
</html> --}}
