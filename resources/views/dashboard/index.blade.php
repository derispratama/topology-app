@extends('layouts.main')
@section('container')
    {{-- <div class="input-group mb-5">
        <span class="input-group-text" id="basic-addon1">
            <i class="ki-duotone ki-magnifier fs-2x">
                <span class="path1"></span>
                <span class="path2"></span>
            </i>
        </span>
        <input type="text" class="form-control" id="search_masterData" placeholder="Cari Topology" aria-label="Username"
            aria-describedby="basic-addon1" />
    </div> --}}

    {{-- <div class="d-flex flex-column flex-md-row rounded border py-10">
        <ul class="nav nav-tabs nav-pills border-0 flex-row flex-md-column me-5 mb-3 mb-md-0 fs-6">
        </ul>
        <div class="tab-content flex-column" id="myTabContent" style="width:100%;">
        </div>
    </div> --}}
    <x-breadcrumb :paths="['Home']" :urls="['']" />
    <div class="container mb-5">
        <div class="row" id="card-container">

            <x-item-loading />
            <x-item-loading />
            <x-item-loading />

            {{-- <x-card-link url="/application" icon="bi-table" title="Total Applications" count="100" />
            <x-card-link url="/department" icon="bi-buildings" title="Total Department" count="4" />
            <x-card-link url="/users" icon="bi-people" title="Total Users" count="100" /> --}}

        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div id="chart" class="mh-400px chart"></div>
        </div>
    </div>

    <!-- Tambahkan skrip JavaScript untuk memeriksa token -->
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    <script src="{{ asset('assets/js/dashboard/index.js') }}" type="module"></script>
@endsection
