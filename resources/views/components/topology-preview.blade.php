<div class="col-12 mb-5">
    <div class="card">
        <div class="card-header">
            <h5 class="card-title d-flex justify-content-between">Topology Preview</h5>
            {{-- <div class="d-flex align-items-center">
                <button class="btn btn-sm btn-primary me-2 h-50" id="saveTopologyBtn">Copy</button>
                <button class="btn btn-sm" id="expandTopologyBtn"><i class="bi bi-arrows-angle-expand fs-3"
                        style="margin-right: -25px"></i></button>
            </div> --}}

        </div>
        <div class="card-body">
            <div id="graphDiv"></div>
            <div id="topology" class="d-flex justify-content-center align-items-center flex-column"></div>
            <div id="empty-topology" class="d-flex justify-content-center align-items-center flex-column">
                <i class="bi bi-diagram-3" style="font-size: 60px"></i>
                <span class="fs-2 fw-bold" style="color: #99A1B7">Empty topology, please add server and application
                    name</span>
            </div>
        </div>
    </div>
</div>
