<div class="modal fade " tabindex="-1" id="input_server_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="server_form" class="form" action="#" autocomplete="off">
                <div class="modal-header">
                    <h3 class="modal-title">Server Information</h3>

                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal"
                        aria-label="Close">
                        <i class="ki-duotone ki-cross fs-1"><span class="path1"></span><span class="path2"></span></i>
                    </div>
                    <!--end::Close-->
                </div>
                <div class="modal-body row align-items-center">
                    <!--begin::name & ip-->
                    <div class="fv-row col-md-6 has-validation" style="height: 95px;">
                        <label id="inputServerNameLabel" for="server_name" class="form-label">Server Name</label>
                        <input type="text" class="form-control" id="server_name" name="server_name"
                            placeholder="Server name">
                    </div>
                    <div class="fv-row col-md-6" style="height: 95px;">
                        <label id="inputServerIPLabel" for="server_ip" class="form-label">Server IP</label>
                        <input type="text" class="form-control" id="server_ip" name="server_ip"
                            placeholder="10.0.0.2">
                    </div>
                    <!--end::name & ip-->
                    <!--begin:: OS & CPU -->
                    <div class="fv-row col-md-6" style="height: 95px;">
                        <label for="server_os" class="form-label">OS </label>
                        <input type="text" class="form-control" id="server_os" name="server_os" placeholder="RHEL 7">

                    </div>
                    <div class="fv-row col-md-6" style="height: 95px;">
                        <label for="server_cpu" class="form-label">CPU </label>
                        <div class="input-group mb-5">
                            <input type="number" class="form-control" id="server_cpu" name="server_cpu"
                                placeholder="8" />
                            <span class="input-group-text" id="cpu-addon" aria-describedby="server_cpu">Core</span>
                        </div>
                    </div>
                    <!--end:: OS & CPU -->
                    <!--begin:: RAM & Memory -->
                    <div class="fv-row col-md-6" style="height: 95px;">
                        <label for="server_ram" class="form-label">RAM </label>
                        <div class="input-group mb-5">
                            <input type="number" class="form-control" id="server_ram" name="server_ram"
                                placeholder="32">
                            <span class="input-group-text" id="ram-addon" aria-describedby="server_ram">GB</span>
                        </div>
                    </div>
                    <div class="fv-row col-md-6" style="height: 95px;">
                        <label for="server_memory" class="form-label">Memory</label>
                        <div class="input-group mb-5">
                            <input type="number" class="form-control" id="server_memory" name="server_memory"
                                placeholder="100">
                            <span class="input-group-text" id="memory-addon" aria-describedby="server_memory">GB</span>
                        </div>

                    </div>
                    <!--end:: RAM & Memory -->
                    <!--begin::Description-->
                    <div class="fv-row col-12 mt-6">
                        <label for="server_description" class="form-label">Description</label>
                        <textarea class="form-control" placeholder="Leave a comment here" id="server_description" name="server_description"
                            style="min-height: 100px"></textarea>
                    </div>
                    <!--end::Description-->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="push_server_btn"><span class="indicator-label">
                            Add Server
                        </span>
                        <span class="indicator-progress">
                            Please wait... <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                        </span></button>
                </div>
            </form>
        </div>
    </div>

</div>
