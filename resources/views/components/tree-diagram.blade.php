<div class="col-12 col-lg-8">
    <div class="card">
        <div class="card-header">
            <h5 class="card-title">Tree Diagram</h5>
            <div class="d-flex align-items-center">
                <button class="btn btn-success btn-sm" id="add_server_btn">+ Add Server</button>
                {{-- <button class="btn btn-sm" id="saveTopologyBtn"><i class="bi bi-arrows-angle-expand fs-3"></i></button> --}}
            </div>
        </div>
        <div class="card-body">
            <h1>Topology</h1>
            <div class="container mt-4 border rounded py-3">
                <h4>Legend</h4>
                <ul class=" align-items-center d-flex flex-wrap gap-3 text-center list-inline">
                    <li class="list-inline-item d-flex align-items-center"><span class="legend-box server-level"></span>
                        Server</li>
                    <li class="list-inline-item d-flex align-items-center"><span
                            class="legend-box load-balancer"></span> Load Balancer</li>
                    <li class="list-inline-item d-flex align-items-center"><span
                            class="legend-box surrounding-db"></span> Surrounding Server</li>
                    <li class="list-inline-item d-flex align-items-center"><span class="legend-box database"></span>
                        Database</li>
                    <li class="list-inline-item d-flex align-items-center"><span
                            class="legend-box surrounding-child"></span> Service</li>
                </ul>
            </div>
            <ol class="wtree fs-5" id="topology-editor">
            </ol>

        </div>
    </div>
</div>
