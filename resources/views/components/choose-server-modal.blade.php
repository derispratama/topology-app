<div class="modal fade" tabindex="-1" id="choose_server_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Choose Server Type</h3>
                <!--begin::Close-->
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                <!--end::Close-->
            </div>
            <div class="modal-body">
                <div class="row justify-content-center" style="height: 100px;">
                    <div class="col-4 h-100 px-2" onclick="showSurroundingServerModal()">
                        <div
                            class="h-100 d-flex justify-content-center align-items-center border rounded d-flex flex-column gap-3 bg-surrounding-db-bg icon-hover-blue">
                            <i class="bi bi-pc" style="font-size: 50px;"></i>
                            <span>Surrounding Server</span>
                        </div>
                    </div>
                    <div class="col-4 h-100 px-2" onclick="showDBModal()">
                        <div
                            class="h-100 d-flex justify-content-center align-items-center border rounded d-flex flex-column gap-3 bg-database icon-hover-blue">
                            <i class="bi bi-database" style="font-size: 50px;"></i>
                            <span>Database</span>
                        </div>
                    </div>
                    <div class="col-4 h-100 px-2" onclick="showLBModal()" id="select_lb">
                        <div
                            class="h-100 d-flex justify-content-center align-items-center border rounded d-flex flex-column gap-3 bg-load-balancer-bg icon-hover-blue">
                            <i class="bi bi-hdd-rack" style="font-size: 50px;"></i>
                            <span>Load Balancer</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
