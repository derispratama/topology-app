<ol class="breadcrumb text-muted fs-3 fw-semibold mb-4 mt-2">
    @foreach ($paths as $index => $path)
        <li class="breadcrumb-item">
            @if ($urls[$index])
                <a href="{{ $urls[$index] }}">{{ $path }}</a>
            @else
                <span class="text-muted">{{ $path }}</span>
            @endif
        </li>
    @endforeach
</ol>
