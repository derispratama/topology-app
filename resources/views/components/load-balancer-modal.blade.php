<div class="modal fade " tabindex="-1" id="input_load_balancer_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="load_balancer_form" class="form" action="#" autocomplete="off">
                <div class="modal-header">
                    <h3 class="modal-title">Load Balancer</h3>

                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal"
                        aria-label="Close">
                        <i class="ki-duotone ki-cross fs-1"><span class="path1"></span><span class="path2"></span></i>
                    </div>
                    <!--end::Close-->
                </div>
                <div class="modal-body row align-items-center">
                    <!--begin::name & ip-->
                    <div class="fv-row col-md-6 has-validation" style="height: 95px;">
                        <label id="inputServerNameLabel" for="load_balancer_name" class="form-label">Load Balancer
                            Name</label>
                        <input type="text" class="form-control" id="load_balancer_name" name="load_balancer_name"
                            placeholder="LB Name">
                    </div>
                    <div class="fv-row col-md-6" style="height: 95px;">
                        <label id="inputServerIPLabel" for="load_balancer_ip" class="form-label">Server IP</label>
                        <input type="text" class="form-control" id="load_balancer_ip" name="load_balancer_ip"
                            placeholder="10.0.0.2">
                    </div>
                    <!--end::name & ip-->

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="push_load_balancer_btn"><span
                            class="indicator-label">
                            Add Load Balancer
                        </span>
                        <span class="indicator-progress">
                            Please wait... <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                        </span></button>
                </div>
            </form>
        </div>
    </div>

</div>
