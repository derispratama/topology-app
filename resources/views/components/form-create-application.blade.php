<div class="col-12 col-lg-4">
    <div class="card mb-5">
        <div class="card-header">
            <h5 class="card-title">Form Create Application</h5>
            <div class="card-title">

            </div>
        </div>
        <div class="card-body">
            <div class="mb-3">
                <label for="application_name" class="form-label">Application Name</label>
                <input type="text" class="form-control" id="application_name" placeholder="application name"
                    autofocus>
            </div>
            <div class="mb-3">
                <label for="departement" class="form-label">Departement</label>
                <input type="text" class="form-control" readonly id="departement" placeholder="application name"
                    value="Financing & Collection Services">
            </div>
            <div class="mb-3">
                <div class="form-check mb-3">
                    <input class="form-check-input" type="checkbox" value="" id="checkbox_load_balancer" />
                    <label class="form-check-label" for="flexCheckDefault">
                        Is using load balancer?
                    </label>
                </div>
                <input type="text" class="form-control d-none" id="ip_load_balancer" placeholder="10.0.0.4"
                    value="">
            </div>
            <div class="mb-3">
                <button class="btn btn-primary btn-sm" id="generate_btn" style="margin-right: 5px;"
                    id="generate_btn">Generate</button>
                <button class="btn btn-success btn-sm" id="save_diagram_btn">Save</button>
            </div>

        </div>
    </div>
</div>
