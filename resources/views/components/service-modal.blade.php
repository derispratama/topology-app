<div class="modal fade " tabindex="-1" id="input_service_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="service_form" class="form" action="#" autocomplete="off">
                <div class="modal-header">
                    <h3 class="modal-title">Service</h3>

                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal"
                        aria-label="Close">
                        <i class="ki-duotone ki-cross fs-1"><span class="path1"></span><span class="path2"></span></i>
                    </div>
                    <!--end::Close-->
                </div>
                <div class="modal-body row align-items-center">
                    <!--begin::name & port-->
                    <div class="fv-row col-md-6 has-validation" style="height: 95px;">
                        <label for="service_name" class="form-label">Service Name</label>
                        <input type="text" class="form-control" id="service_name" name="service_name"
                            placeholder="Service name">
                    </div>
                    <div class="fv-row col-md-6" style="height: 95px;">
                        <label for="service_port" class="form-label">Service Port</label>
                        <input type="number" class="form-control" id="service_port" name="service_port"
                            placeholder="3000">
                    </div>
                    <!--end::name & port-->
                    <!--begin::url -->
                    <div class="fv-row has-validation" style="height: 95px;">
                        <label for="service_url" class="form-label">Service URL</label>

                        <div class="input-group mb-5">
                            <span class="input-group-text" id="path_url_addons"
                                aria-describedby="surrounding_server_ram"></span>
                            <input type="text" class="form-control" id="service_url" name="service_url"
                                placeholder="api/v1/check-ktp">
                        </div>
                    </div>
                    <!--end::url -->

                    <!--begin::RequestParams -->
                    <div class="fv-row has-validation mb-5">
                        <label for="service_request_params" class="form-label">Request Params</label>
                        <textarea class="form-control" id="service_request_params" name="service_request_params" placeholder="id=10"
                            style="height: 100px"></textarea>
                    </div>
                    <!--end::RequestParams -->
                    <!--begin::ResponseParams -->
                    <div class="fv-row has-validation">
                        <label for="service_response_params" class="form-label">Response Params</label>
                        <textarea class="form-control" id="service_response_params" name="service_response_params" placeholder="id=10"
                            style="height: 100px"></textarea>
                    </div>
                    <!--end::ResponseParams -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="push_service_btn"><span class="indicator-label"
                            id="push_surrounding_server_label">
                            Add Service
                        </span>
                        <span class="indicator-progress">
                            Please wait... <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                        </span></button>
                </div>
            </form>
        </div>
    </div>

</div>
