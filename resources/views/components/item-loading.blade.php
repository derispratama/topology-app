<div class="col-sm mb-2">
    <a class="card hover-elevate-up shadow-sm parent-hover h-100 placeholder-glow">
        <div class="card-body d-flex align-items-center justify-content-start gap-5">
            <i class="bi bi-buildings placeholder" style="font-size:35px;"></i>
            <span class="placeholder col-10 fs-1"></span>
        </div>
    </a>
</div>
