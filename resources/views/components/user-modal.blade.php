<div class="modal fade " tabindex="-1" id="user_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="user_form" class="form" action="#" autocomplete="off">
                <div class="modal-header">
                    <h3 class="modal-title">Users</h3>

                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal"
                        aria-label="Close">
                        <i class="ki-duotone ki-cross fs-1"><span class="path1"></span><span class="path2"></span></i>
                    </div>
                    <!--end::Close-->
                </div>

                <div class="modal-body">
                    <!--begin::name&email-->
                    <div class="container row">
                        <div class="col-12 col-md-6">
                            <div class="fv-row mb-10">
                                <label for="exampleFormControlInput1" class="required form-label">Name</label>
                                <input type="text" class="form-control form-control-solid" placeholder="Input name"
                                    name="name" id="inputName" />
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="fv-row mb-10">
                                <label for="inputEmail" class="required form-label">Email</label>
                                <input type="email" class="form-control form-control-solid" placeholder="Input email"
                                    name="email" id="inputEmail" />
                            </div>
                        </div>
                    </div>
                    <!--end::name&email-->
                    <!--begin::username&password-->
                    <div class="container row">
                        <div class="col-12 col-md-6">
                            <div class="fv-row mb-10">
                                <label for="inputUsername" class="required form-label">Username</label>
                                <input type="text" class="form-control form-control-solid"
                                    placeholder="Input username" id="inputUsername" name="username" />
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="fv-row mb-10">
                                <label for="inputPassword" class="required form-label">Password</label>
                                <div class="input-group input-group-solid mb-5">
                                    <input type="password" class="form-control" placeholder="Input password"
                                        aria-label="Input password" id="inputPassword" name="password" />
                                    <span class="input-group-text" style="cursor: pointer;" id="showPasswordBtn"><i
                                            class="bi bi-eye-fill fs-2"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end::username&password-->
                    <!--begin::isLdap&role-->
                    <div class="container row">
                        <div class="col-12 col-md-6" id="container-ldap">
                            <div class="fv-row mb-10">
                                <label for="inputIsLdap" class=" form-label">is LDAP?</label>
                                <div class="form-check mt-4">
                                    <input class="form-check-input" type="checkbox" value="" id="inputIsLdap"
                                        name="is_ldap" />
                                    <label class="form-check-label text-black" for="inputIsLdap">
                                        LDAP
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6" id="container-role">
                            <div class="fv-row mb-10">
                                <label for="inputRole" class="required form-label">Role</label>
                                <select class="form-select" aria-label="Select example" id="inputRole" name="id_role">
                                    <option disabled selected>Select role</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!--end::isLdap&role-->
                    <!--begin::department-->
                    <div class="container row">
                        <div class="fv-row mb-10 col-12" id="container-department">
                            <label for="inputDepartment" class="required form-label">Department</label>
                            <select class="form-select" aria-label="Select example" id="inputDepartment"
                                name="id_departement">
                                <option disabled selected>Select department</option>
                            </select>
                        </div>
                    </div>
                    <!--begin::department-->
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="saveUserBtn"><span class="indicator-label">
                            Save
                        </span>
                        <span class="indicator-progress">
                            Please wait... <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                        </span></button>
                </div>
            </form>
        </div>
    </div>
</div>
