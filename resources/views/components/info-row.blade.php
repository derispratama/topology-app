@php
    $bottomClass = $isBottom ? '' : 'border-bottom';
@endphp

<div class="row g-3 align-items-center mb-3 fs-3 {{ $bottomClass }} pb-3 d-none d-lg-flex">
    <div class="col-3">
        <div>{{ $field }}</div>
    </div>
    <div class="col-9">
        <div>{{ $value }}</div>
    </div>
</div>

<div class="row g-3 align-items-center mb-3 fs-3 {{ $bottomClass }} pb-3 d-flex d-lg-none">
    <div class="col-12">
        <div class="fw-bold">{{ $field }}</div>
        <div class="fs-4">{{ $value }}</div>
    </div>
</div>
