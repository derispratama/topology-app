<div class="card shadow-sm">
    <div class="card-header">
        <h3 class="card-title fs-2 fw-bold">Details</h3>
        <div class="card-toolbar">
            <button type="button" class="btn btn-sm btn-light" id="editBtn">
                Edit
            </button>
        </div>
    </div>
    <div class="card-body">
        <x-info-row :field="'Name'" :value="$name" />
        <x-info-row :field="'Email'" :value="$email" />
        <x-info-row :field="'Username'" :value="$username" />
        <x-info-row :field="'Role'" :value="$role" />
        <x-info-row :field="'Department'" :value="$department" :isBottom="true" />
    </div>
    <div class="card-footer">
        Footer
    </div>
</div>
