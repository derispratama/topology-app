<div class="card shadow-sm mb-5">
    <div class="card-header justify-content-center p-5">
        <img class="mw-100 mh-300px card-rounded-bottom " alt="" src="assets/media/moslem/man_icon.png" />
    </div>
    <div class="card-body text-center">
        <div class="fs-1 my-2 fw-bold">{{ $name }}</div>
        <span class="badge text-bg-secondary p-2">{{ $role }}</span>
    </div>
    <div class="card-footer">
        <button type="button" class="btn btn-sm btn-light w-100 p-3  mb-2 bg-light text-dark fw-bold"
            onclick="logout2(event)">
            Logout </button>
    </div>
</div>
