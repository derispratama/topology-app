<div class="app-header-primary" data-kt-sticky="true" data-kt-sticky-name="app-header-primary-sticky"
    data-kt-sticky-offset="{default: 'false', lg: '300px'}">
    <!--begin::Header primary container-->
    <div class="app-container container-xxl d-flex align-items-stretch justify-content-between">
        <!--begin::Logo and search-->
        <div class="d-flex flex-grow-1 flex-lg-grow-0">
            <!--begin::Logo wrapper-->
            <div class="d-flex align-items-center me-7" id="kt_app_header_logo_wrapper">
                <!--begin::Header toggle-->
                <button
                    class="d-lg-none btn btn-icon btn-flex btn-color-gray-600 btn-active-color-primary w-35px h-35px ms-n2 me-2"
                    id="kt_app_header_menu_toggle">
                    <i class="ki-outline ki-abstract-14 fs-2"></i>
                </button>
                <!--end::Header toggle-->
                <!--begin::Logo-->
                <a href="/application" class="d-flex align-items-center me-lg-20 me-5">
                    <img alt="Logo" src="assets/media/logos/logo_bsi_polos.png"
                        class="h-20px h-lg-150px theme-light-show d-none d-sm-inline" />
                </a>
                <!--end::Logo-->
            </div>
            <!--end::Logo wrapper-->
            <!--begin::Menu wrapper-->
            <div class="app-header-menu app-header-mobile-drawer align-items-stretch" data-kt-drawer="true"
                data-kt-drawer-name="app-header-menu" data-kt-drawer-activate="{default: true, lg: false}"
                data-kt-drawer-overlay="true" data-kt-drawer-width="250px" data-kt-drawer-direction="start"
                data-kt-drawer-toggle="#kt_app_header_menu_toggle" data-kt-swapper="true"
                data-kt-swapper-mode="{default: 'append', lg: 'prepend'}"
                data-kt-swapper-parent="{default: '#kt_app_body', lg: '#kt_app_header_wrapper'}">
                <!--begin::Menu-->
                <div class="menu menu-rounded menu-active-bg menu-state-primary menu-column menu-lg-row menu-title-gray-700 menu-icon-gray-500 menu-arrow-gray-500 menu-bullet-gray-500 my-5 my-lg-0 align-items-stretch fw-semibold px-2 px-lg-0"
                    id="kt_app_header_menu" data-kt-menu="true">
                    <!--begin:Menu item-->
                    <div data-kt-menu-placement="bottom-start"
                        class="menu-item d-none menu-lg-down-accordion me-0 me-lg-2" id="dashboard_menu">
                        <!--begin:Menu link-->
                        <a href="/dashboard" class="menu-link py-3">
                            <span class="">
                                <span class="menu-title">Dashboards</span>
                                <span class="menu-arrow d-lg-none"></span>
                            </span>
                        </a>
                        <!--end:Menu link-->
                    </div>

                    <!--end:Menu item-->
                    <!--begin:Menu item-->
                    {{-- <div data-kt-menu-trigger="{default: 'click'}" data-kt-menu-placement="bottom-start"
                        class="menu-item menu-lg-down-accordion menu-sub-lg-down-indention me-0 me-lg-2">
                        <!--begin:Menu link-->
                        <span class="menu-link py-3">
                            <span class="menu-title">Apps</span>
                            <span class="menu-arrow d-lg-none"></span>
                        </span>
                        <!--end:Menu link-->
                        <!--begin:Menu sub-->
                        <div
                            class="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown px-lg-2 py-lg-4 w-lg-250px">
                            <!--begin:Menu item-->
                            <div data-kt-menu-trigger="{default:'click', lg: 'hover'}"
                                data-kt-menu-placement="right-start" class="menu-item menu-lg-down-accordion">
                                <!--begin:Menu link-->
                                <span class="menu-link py-3">
                                    <span class="menu-icon">
                                        <i class="ki-outline ki-chart fs-2"></i>
                                    </span>
                                    <span class="menu-title">List Apps</span>
                                    <span class="menu-arrow"></span>
                                </span>
                                <!--end:Menu link-->
                                <!--begin:Menu sub-->
                                <div class="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-active-bg px-lg-2 py-lg-4 w-lg-225px"
                                    id="get_all_app">
                                </div>
                                <!--end:Menu sub-->
                            </div>
                            <!--end:Menu item-->
                            <!--begin:Menu item-->
                            <div class="menu-item">
                                <!--begin:Menu link-->
                                <a class="menu-link py-3" href="../../priority/list">
                                    <span class="menu-icon">
                                        <i class="ki-outline ki-rocket fs-2"></i>
                                    </span>
                                    <span class="menu-title">Priority Apps</span>
                                </a>
                                <!--end:Menu link-->
                            </div>
                            <!--end:Menu item-->
                        </div>
                        <!--end:Menu sub-->
                    </div> --}}
                    <!--end:Menu item-->
                    <!--begin:Menu item-->
                    <div data-kt-menu-trigger="{default: 'click'}" data-kt-menu-placement="bottom-start"
                        class="menu-item menu-lg-down-accordion menu-sub-lg-down-indention me-0 me-lg-2"
                        id="setting_menu">
                        <!--begin:Menu link-->
                        <span class="menu-link py-3">
                            <span class="menu-title">Settings</span>
                            <span class="menu-arrow d-lg-none"></span>
                        </span>
                        <!--end:Menu link-->
                        <!--begin:Menu sub-->
                        <div
                            class="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown px-lg-2 py-lg-4 w-lg-250px">
                            <!--begin:Menu item-->
                            <div class="menu-item d-none" id="users_menu">
                                <!--begin:Menu link-->
                                <a class="menu-link py-3" href="../../users">
                                    <span class="menu-icon">
                                        <i class="ki-outline ki-user-edit fs-2"></i>
                                    </span>
                                    <span class="menu-title">User</span>
                                </a>
                                <!--end:Menu link-->
                            </div>
                            <!--end:Menu item-->
                            <!--begin:Menu item-->
                            <div class="menu-item d-none" id="department_menu">
                                <!--begin:Menu link-->
                                <a class="menu-link py-3" href="../../department">
                                    <span class="menu-icon">
                                        <i class="ki-outline ki-archive fs-2"></i>
                                    </span>
                                    <span class="menu-title">Department</span>
                                </a>
                                <!--end:Menu link-->
                            </div>
                            <!--end:Menu item-->
                            <!--begin:Menu item-->
                            <div class="menu-item">
                                <!--begin:Menu link-->
                                <a class="menu-link py-3" href="/application">
                                    <span class="menu-icon">
                                        <i class="ki-outline ki-technology fs-2"></i>
                                    </span>
                                    <span class="menu-title">Application</span>
                                </a>
                                <!--end:Menu link-->
                            </div>
                            <!--end:Menu item-->
                        </div>
                        <!--end:Menu sub-->
                    </div>
                    <!--end:Menu item-->
                </div>
                <!--end::Menu-->
            </div>
            <!--end::Menu wrapper-->
        </div>
        <!--end::Logo and search-->
        <!--begin::Navbar-->
        <div class="app-navbar flex-shrink-0">
            <!--begin::User menu-->
            @include('layouts.profile')
            <!--end::User menu-->
            <!--begin::Header menu toggle-->
            <div class="app-navbar-item d-lg-none ms-2 me-n3" title="Show header menu">
                <div class="btn btn-icon btn-color-gray-500 btn-active-color-primary w-35px h-35px"
                    id="kt_app_header_menu_toggle">
                    <i class="ki-outline ki-text-align-left fs-1"></i>
                </div>
            </div>
            <!--end::Header menu toggle-->
        </div>
        <!--end::Navbar-->
    </div>
    <!--end::Header primary container-->
</div>

<script>
    document.addEventListener("DOMContentLoaded", function() {
        const path = '{{ request()->path() }}';
        const user = JSON.parse(localStorage.getItem("user"));
        const role = user.roles.role_name;

        switch (path) {
            case 'dashboard':
                document.getElementById('dashboard_menu').classList.add('here');
                break;
            case 'application':
                document.getElementById('setting_menu').classList.add('here');

                break;
            case 'users':
                document.getElementById('setting_menu').classList.add('here');
                break;
            case 'department':
                document.getElementById('setting_menu').classList.add('here');
                break;
        }

        switch (role) {
            case 'superadmin':
                document.getElementById('users_menu').classList.remove('d-none');
                document.getElementById('department_menu').classList.remove('d-none');
                document.getElementById('dashboard_menu').classList.remove('d-none');
                break;
            case 'officer':
                document.getElementById('users_menu').classList.add('d-none');
                document.getElementById('department_menu').classList.add('d-none');
                document.getElementById('dashboard_menu').classList.add('d-none');
                break;
        }
    })
</script>
