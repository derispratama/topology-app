<div class="app-navbar-item ms-3 ms-lg-9" id="kt_header_user_menu_toggle">
    <!--begin::Menu wrapper-->
    <div class="d-flex align-items-center" data-kt-menu-trigger="{default: 'click', lg: 'hover'}"
        data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
        <!--begin:Info-->
        <div class="text-end d-none d-sm-flex flex-column justify-content-center me-3">
            <span class="text-white fs-8 fw-bold" id="greeting">Assalamu'alaikum</span>
            <a class="text-white text-hover-primary fs-7 fw-bold d-block" id="username">-</a>
        </div>
        <!--end:Info-->
        <!--begin::User-->
        <div class="cursor-pointer symbol symbol symbol-circle symbol-35px symbol-md-40px">
            <img id="user-avatar" class="" src="assets/media/avatars/300-23.jpg" alt="user" />
            <div
                class="position-absolute translate-middle bottom-0 mb-1 start-100 ms-n1 bg-success rounded-circle h-8px w-8px">
            </div>
        </div>
        <!--end::User-->
    </div>
    <!--begin::User account menu-->
    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-color fw-semibold py-4 fs-6 w-275px"
        data-kt-menu="true">
        <!--begin::Menu item-->
        <div class="menu-item px-3">
            <div class="menu-content d-flex align-items-center px-3">
                <!--begin::Avatar-->
                <div class="symbol symbol-50px me-5">
                    <img id="menu-avatar" alt="Logo" src="assets/media/avatars/300-23.jpg" />
                </div>
                <!--end::Avatar-->
                <!--begin::Username-->
                <div class="d-flex flex-column">
                    <div class="fw-bold d-flex align-items-center fs-5">
                        <span id="menu-username">-</span>
                        <span class="badge badge-light-success fw-bold fs-8 px-2 py-1 ms-2" id="user-role">Admin</span>
                    </div>
                    <a id="user-email" href="#"
                        class="fw-semibold text-muted text-hover-primary fs-7">deris@gmail.com</a>
                </div>
                <!--end::Username-->
            </div>
        </div>
        <!--end::Menu item-->
        <!--begin::Menu separator-->
        <div class="separator my-2"></div>
        <!--end::Menu separator-->
        <!--begin::Menu item-->
        <div class="menu-item px-5">

            <a href="" class="menu-link px-5" onclick="openProfile(event)">Profile</a>
            <a href="" class="menu-link px-5" onclick="logout(event)">Sign Out</a>
            {{-- <form id="logout-form" action="" method="post" style="display: none;">
                @csrf
            </form> --}}
        </div>
        <!--end::Menu item-->
    </div>
    <!--end::User account menu-->
    <!--end::Menu wrapper-->
</div>

<script>
    document.addEventListener("DOMContentLoaded", function() {
        const user = JSON.parse(localStorage.getItem("user"));

        if (user) {
            document.getElementById("username").textContent = user.name;
            document.getElementById("menu-username").textContent = user.name;
            document.getElementById("user-role").textContent = user.roles.role_name;
            // document.getElementById("user-role").textContent = 'a';
            document.getElementById("user-email").textContent = user.email;
        }

        window.logout = (event) => {
            event.preventDefault();
            localStorage.removeItem("token");
            localStorage.removeItem("user");
            window.location.href = "/";
        }
        window.openProfile = (event) => {
            event.preventDefault();
            window.location.href = `/profile/${user.id}`;
        }
    });
</script>
