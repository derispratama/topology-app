<div id="kt_app_footer" class="app-footer align-items-center justify-content-center justify-content-md-between flex-column flex-md-row py-3 py-lg-6">
	<!--begin::Copyright-->
	<div class="text-dark order-2 order-md-1">
		<span class="text-muted fw-semibold me-1">{{date('Y')}}&copy;</span>
		<a href="" target="_blank" class="text-gray-800 text-hover-primary">PT Bank Syariah Indonesia</a>
	</div>
	<!--end::Copyright-->
</div>