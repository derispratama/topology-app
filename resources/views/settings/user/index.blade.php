@extends('layouts.main')
@section('container')
    <x-user-modal />
    <x-breadcrumb :paths="['Home', 'Settings', 'Users']" :urls="['/dashboard', '', '']" />

    <x-confirm-alert-modal :title="'Delete User'" :message="'Are you sure you want to delete this user?'" :callback="'deleteUser()'" />
    <div class="card">
        <div class="card-body">
            <a class="btn btn-primary mb-5" id="addUserBtn">Add Users</a>
            <table id="kt_datatable_dom_positioning" class="table table-striped table-row-bordered gy-5 gs-7 border rounded">
                <thead>
                    <tr class="fw-bold fs-6 text-gray-800 px-7">
                        <th>No</th>
                        <th>Name</th>
                        <th>Role</th>
                        <th>Department Name</th>
                        <th style="width:200px">Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    @push('my-scripts')
        <script src="{{ asset('assets/js/user/index.js') }}" type="module"></script>
    @endpush
@endsection
