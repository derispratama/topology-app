@extends('layouts.main')

@section('container')
    <x-user-modal />
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-4">
                <x-profile-info-panel :name="$user->name" :role="$user->roles->role_name" :id="$user->id" />
            </div>
            <div class="col-12 col-lg-8">
                <x-user-info-details :name="$user->name" :email="$user->email" :username="$user->username" :role="$user->roles->role_name"
                    :department="$user->department->departement_name" />
            </div>
        </div>
    </div>
    @push('my-scripts')
        <script src="{{ asset('assets/js/profile/index.js') }}" type="module"></script>
    @endpush
@endsection
