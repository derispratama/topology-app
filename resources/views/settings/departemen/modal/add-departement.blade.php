<div class="modal fade" tabindex="-1" id="add_department_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="department_form" class="form" action="#" autocomplete="off">

                <div class="modal-header">
                    <h3 class="modal-title">Department</h3>

                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal"
                        aria-label="Close">
                        <i class="ki-duotone ki-cross fs-1"><span class="path1"></span><span class="path2"></span></i>
                    </div>
                    <!--end::Close-->
                </div>

                <div class="modal-body">
                    <div class="fv-row mb-10">
                        <label for="inputDepartmentName" class="required form-label">Department Name</label>
                        <input id="inputDepartmentName" name="inputDepartmentName" type="text"
                            class="form-control form-control-solid" placeholder="Department Name" autofocus />
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="submitBtn"><span class="indicator-label">
                            Save
                        </span>
                        <span class="indicator-progress">
                            Please wait... <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                        </span></button>
                </div>
            </form>
        </div>
    </div>
</div>
