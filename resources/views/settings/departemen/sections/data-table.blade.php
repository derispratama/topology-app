@extends('settings.departemen.index')
@section('content')
    <a class="btn btn-primary mb-5" id="addDepartmentBtn">Add Department</a>
    <table id="kt_datatable_dom_positioning" class="table table-striped table-row-bordered gy-5 gs-7 border rounded">
        <thead>
            <tr class="fw-bold fs-6 text-gray-800 px-7">
                <th>No</th>
                <th>Department Name</th>
                <th>Total Users</th>
                <th style="width:200px">Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    @push('my-scripts')
        <script src="{{ asset('assets/js/department/index.js') }}" type="module"></script>
    @endpush
@endsection
