@extends('layouts.main')
@section('container')
    @include('settings.departemen.modal.add-departement')
    @include('settings.departemen.modal.delete-department')
    <x-breadcrumb :paths="['Home', 'Settings', 'Department']" :urls="['/dashboard', '', '']" />

    <div class="card">
        <div class="card-body">
            @yield('content')
        </div>
    </div>
@endsection
