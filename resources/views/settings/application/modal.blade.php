<!-- Modal Structure for Server Removal -->
<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmModalLabel">Alert</h5>
            </div>
            <div class="modal-body fs-4">
                Are you sure you want to remove this server?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" id="cancelRemoveBtn">Cancel</button>
                <button type="button" class="btn btn-danger" id="confirmRemoveBtn">Yes</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Structure for Surrounding Server Removal -->
<div class="modal fade" id="confirmSurroundingModal" tabindex="-1" role="dialog"
    aria-labelledby="confirmSurroundingModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmSurroundingModalLabel">Alert</h5>
            </div>
            <div class="modal-body fs-4">
                Are you sure you want to remove this surrounding server?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" id="cancelRemoveSurroundingBtn">Cancel</button>
                <button type="button" class="btn btn-danger" id="confirmRemoveSurroundingBtn">Yes</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Structure for Service Surrounding Server Removal -->
<div class="modal fade" id="confirmServiceSurroundingModal" tabindex="-1" role="dialog"
    aria-labelledby="confirmServiceSurroundingModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmServiceSurroundingModalLabel">Alert</h5>
            </div>
            <div class="modal-body fs-4">
                Are you sure you want to remove this service?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" id="cancelRemoveServiceSurroundingBtn">Cancel</button>
                <button type="button" class="btn btn-danger" id="confirmRemoveServiceSurroundingBtn">Yes</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal Structure for Update Service -->
<div class="modal fade" id="updateServiceOfSurroundingServerModal" tabindex="-1" role="dialog"
    aria-labelledby="updateServiceOfSurroundingServerLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="updateServiceOfSurroundingServerLabel">Service</h5>
            </div>
            <div class="modal-body row g-3 align-items-center">
                <div class="col-md-8">
                    <label for="inputServiceName" class="form-label">Service Name</label>
                    <input type="text" class="form-control" id="inputServiceName" placeholder="Service name">
                </div>
                <div class="col-md-4">
                    <label for="inputPort" class="form-label">Port</label>
                    <input type="number" class="form-control" id="inputPort" placeholder="Port number">
                </div>
                <div class="col-md-12">
                    <label for="inputUrl" class="form-label">Url</label>
                    <input type="text" class="form-control" id="inputUrl" placeholder="url">
                </div>
                <div class=" col-12 mt-6">
                    <label for="inputRequestParams">Request Params</label>
                    <textarea class="form-control" placeholder="Leave a comment here" id="inputRequestParams" style="height: 100px"></textarea>
                </div>
                <div class=" col-12 mt-6">
                    <label for="inputResponseParams">Response Params</label>
                    <textarea class="form-control" placeholder="Leave a comment here" id="inputResponseParams" style="height: 100px"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary"
                    id="cancelServiceOfSurroundingServerBtn">Cancel</button>
                <button type="button" class="btn btn-primary"
                    id="confirmServiceOfSurroundingServerBtn">Save</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Structure for Input Server -->
<div class="modal fade" id="inputServerModal" tabindex="-1" role="dialog" aria-labelledby="inputServerLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="inputServerLabel">Server</h5>
            </div>
            <form class="needs-validation" novalidate>
                <div class="modal-body row g-3 align-items-center">
                    <div class="col-md-6 has-validation">
                        <label id="inputServerNameLabel" for="inputServerName" class="form-label">Server Name</label>
                        <input type="text" class="form-control" id="inputServerName" placeholder="Server name"
                            required>
                        <div class="invalid-feedback">
                            Please input server name.
                        </div>
                        <div class="valid-feedback">
                            valid!
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label id="inputServerIPLabel" for="inputServerIP" class="form-label">Server IP</label>
                        <input type="text" class="form-control" id="inputServerIP" placeholder="10.0.0.2"
                            required>
                        <div class="invalid-feedback">
                            Please input valid ip.
                        </div>
                        <div class="valid-feedback">
                            valid!
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label for="inputOSServer" class="form-label">OS </label>
                        <input type="text" class="form-control" id="inputOSServer" placeholder="RHEL 7" required>
                        <div class="invalid-feedback">
                            Please input server name.
                        </div>
                        <div class="valid-feedback">
                            valid!
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label for="inputCPUServer" class="form-label">CPU </label>
                        <input type="text" class="form-control" id="inputCPUServer" placeholder="8 Core"
                            required>
                        <div class="invalid-feedback">
                            Please input server name.
                        </div>
                        <div class="valid-feedback">
                            valid!
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label for="inputRamServer" class="form-label">RAM </label>
                        <input type="text" class="form-control" id="inputRamServer" placeholder="32 GB" required>
                        <div class="invalid-feedback">
                            Please input server name.
                        </div>
                        <div class="valid-feedback">
                            valid!
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label for="inputMemoryServer" class="form-label">Memory</label>
                        <input type="text" class="form-control" id="inputMemoryServer" placeholder="100 GB"
                            required>
                        <div class="invalid-feedback">
                            Please input server name.
                        </div>
                        <div class="valid-feedback">
                            valid!
                        </div>
                    </div>
                    <div class="col-md-12" id="divSelecServerType">
                        <label id="inputSurroundingServerTypeLabel" for="inputSurroundingServerType"
                            class="form-label">Server Type</label>

                        <select class="form-select" aria-label="Default select example" id="inputServerType"
                            required>
                            <option value="">Server Type</option>
                            <option value="app">Application</option>
                            <option value="db">Database</option>
                        </select>
                        <div class="invalid-feedback">
                            Please chose server name.
                        </div>
                    </div>
                    <div class="form-floating col-12 mt-6">
                        <textarea class="form-control" placeholder="Leave a comment here" id="inputAppServer" style="height: 100px"
                            required></textarea>
                        <label for="inputAppServer">Description</label>
                        <div class="invalid-feedback">
                            Please input server name.
                        </div>
                        <div class="valid-feedback">
                            valid!
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="cancleInputServerModal">Cancel</button>
                    <button type="submit" class="btn btn-primary" id="saveServerInfoBtn">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
