@extends('layouts.main')

@section('container')
    <style>
        @keyframes dash {
            from {
                stroke-dashoffset: 1000;
            }

            to {
                stroke-dashoffset: 0;
            }
        }

        .animated-link {
            stroke-dasharray: 5, 5;
            animation: dash 2s linear infinite;
        }

        #group-btn:hover {
            background-color: #218838 !important;
        }

        #ungroup-btn:hover {
            background-color: #C82333 !important;
        }
    </style>
    <div class="col-12">
        <div class="card mb-5">
            <div class="card-header">
                <h5 class="card-title">Form Create Application</h5>
                <div class="card-title"></div>
            </div>
            <div class="card-body d-flex w-100">
                <div class="row w-100">
                    <div class="d-flex col w-100 gap-3">
                        <div class="mb-3 col-6">
                            <label for="application_name" class="form-label">Application Name</label>
                            <input type="text" class="form-control" id="application_name" placeholder="application name"
                                autofocus>
                        </div>
                        <div class="mb-3 col-6">
                            <label for="departement" class="form-label">Departement</label>
                            <input type="text" class="form-control" readonly id="departement"
                                placeholder="application name" value="Financing & Collection Services">
                        </div>
                    </div>
                    <div class="mb-3">
                        <div class="d-flex flex-row justify-content-between">
                            <button class="btn btn-success btn-sm" id="save_diagram_btn">Save</button>
                            <div style="width: fit-content">
                                <button class="btn btn-success btn-sm group-btn" id="group-btn"
                                    style="background-color: #28A745" onclick="group()">Group</button>
                                <button class="btn btn-success btn-sm ungroup-btn" id="ungroup-btn"
                                    style="background-color: #DC3545" onclick="ungroup()">Ungroup</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="allSampleContent" class="w-full">
        <div id="sample" style="display: flex; justify-content: space-between">
            <!-- Palette Column -->
            <div id="myPaletteDiv"
                style="
                    width: 150px;
                    margin-right: 2px;
                    border: 1px solid #ccc;
                    border-radius: 3px;
                    position: relative;
                    -webkit-tap-highlight-color: rgba(255, 255, 255, 0);
                    background-color: #fff;
                    cursor: auto;
                ">
            </div>

            <!-- Diagram Column -->
            <div id="myDiagramDiv"
                style="
                    flex-grow: 1;
                    border: 1px solid #ccc;
                    border-radius: 3px;
                    height: 100%;
                    min-height: 600px;
                    margin-right: 2px;
                    position: relative;
                    -webkit-tap-highlight-color: rgba(255, 255, 255, 0);
                    cursor: auto;
                    background-color: #fff;
                ">
            </div>

            <!-- Properties Column -->
            <div id="propertiesDiv"
                class="d-none"style="
                                                                                                                                                                                                                                            width: 200px;
                                                                                                                                                                                                                                            border: 1px solid #ccc;
                                                                                                                                                                                                                                            border-radius: 3px;
                                                                                                                                                                                                                                            position: relative;
                                                                                                                                                                                                                                            -webkit-tap-highlight-color: rgba(255, 255, 255, 0);
                                                                                                                                                                                                                                            background-color: #fff;
                                                                                                                                                                                                                                            cursor: auto;
                                                                                                                                                                                                                                        ">
                <div class="fs-5 py-4 text-center border-bottom">Properties</div>

                <!-- Server Properties -->
                <div id="server-properties" class="property-section d-none">
                    <div class="mb-3">
                        <label for="server-name" class="form-label">Name</label>
                        <input type="text" class="form-control form-control-sm" id="server-name" placeholder="Name" />
                    </div>
                    <div class="mb-3">
                        <label for="server-ip" class="form-label">IP Address</label>
                        <input type="text" class="form-control form-control-sm" id="server-ip"
                            placeholder="IP Address" />
                    </div>
                    <div class="mb-3">
                        <label for="server-os" class="form-label">OS</label>
                        <input type="text" class="form-control form-control-sm" id="server-os" placeholder="OS" />
                    </div>
                    <div class="mb-3">
                        <label for="server-cpu" class="form-label">CPU</label>
                        <input type="text" class="form-control form-control-sm" id="server-cpu" placeholder="CPU" />
                    </div>
                    <div class="mb-3">
                        <label for="server-ram" class="form-label">RAM</label>
                        <input type="text" class="form-control form-control-sm" id="server-ram" placeholder="RAM" />
                    </div>
                    <div class="mb-3">
                        <label for="server-memory" class="form-label">Storage</label>
                        <input type="text" class="form-control form-control-sm" id="server-memory"
                            placeholder="Memory" />
                    </div>
                    <div class="mb-3">
                        <label for="server-apps" class="form-label">Installed Apps</label>
                        <textarea class="form-control form-control-sm" id="server-apps" placeholder="Installed Apps"></textarea>
                    </div>
                </div>

                <!-- Load Balancer Properties -->
                <div id="load-balancer-properties" class="property-section d-none">
                    <div class="mb-3">
                        <label for="loadbalancer-name" class="form-label">Name</label>
                        <input type="text" class="form-control form-control-sm" id="loadbalancer-name"
                            placeholder="Name" />
                    </div>
                    <div class="mb-3">
                        <label for="loadbalancer-ip" class="form-label">IP Address</label>
                        <input type="text" class="form-control form-control-sm" id="loadbalancer-ip"
                            placeholder="IP Address" />
                    </div>
                </div>

                <!-- Database Properties -->
                <div id="database-properties" class="property-section d-none">
                    <div class="mb-3">
                        <label for="database-name" class="form-label">Name</label>
                        <input type="text" class="form-control form-control-sm" id="database-name"
                            placeholder="Name" />
                    </div>
                    <div class="mb-3">
                        <label for="database-ip" class="form-label">IP Address</label>
                        <input type="text" class="form-control form-control-sm" id="database-ip"
                            placeholder="IP Address" />
                    </div>
                    <div class="mb-3">
                        <label for="database-os" class="form-label">OS</label>
                        <input type="text" class="form-control form-control-sm" id="database-os" placeholder="OS" />
                    </div>
                    <div class="mb-3">
                        <label for="database-cpu" class="form-label">CPU</label>
                        <input type="text" class="form-control form-control-sm" id="database-cpu"
                            placeholder="CPU" />
                    </div>
                    <div class="mb-3">
                        <label for="database-ram" class="form-label">RAM</label>
                        <input type="text" class="form-control form-control-sm" id="database-ram"
                            placeholder="RAM" />
                    </div>
                    <div class="mb-3">
                        <label for="database-memory" class="form-label">Storage</label>
                        <input type="text" class="form-control form-control-sm" id="database-memory"
                            placeholder="Memory" />
                    </div>
                    <div class="mb-3">
                        <label for="database-apps" class="form-label">Installed Apps</label>
                        <textarea class="form-control form-control-sm" id="database-apps" placeholder="Installed Apps"></textarea>
                    </div>
                </div>

                <!-- Service Properties -->
                <div id="service-properties" class="property-section d-none">
                    <div class="mb-3">
                        <label for="service-name" class="form-label">Name</label>
                        <input type="text" class="form-control form-control-sm" id="service-name"
                            placeholder="Name" />
                    </div>
                    <div class="mb-3">
                        <label for="service-port" class="form-label">Port</label>
                        <input type="text" class="form-control form-control-sm" id="service-port"
                            placeholder="Port" />
                    </div>
                    <div class="mb-3">
                        <label for="service-path-url" class="form-label">Path URL</label>
                        <input type="text" class="form-control form-control-sm" id="service-path-url"
                            placeholder="Path URL" />
                    </div>
                    <div class="mb-3">
                        <label for="service-request-params" class="form-label">Request Params</label>
                        <textarea class="form-control form-control-sm" id="service-request-params" placeholder="Request Params"></textarea>
                    </div>
                    <div class="mb-3">
                        <label for="service-response-params" class="form-label">Response Params</label>
                        <textarea class="form-control form-control-sm" id="service-response-params" placeholder="Response Params"></textarea>
                    </div>
                </div>

                <!-- Group Properties -->
                <div id="group-properties" class="property-section d-none">
                    <div class="mb-3">
                        <label for="group-name" class="form-label">Name</label>
                        <input type="text" class="form-control form-control-sm" id="group-name" placeholder="Name" />
                    </div>
                    <div class="mb-3">
                        <label for="group-bgcolor" class="form-label">Background Color</label>
                        <select class="form-control form-control-sm" id="group-bgcolor">
                            <option value="transparent">None</option>
                            <option value="#f8f9fa">Light Gray</option>
                            <option value="#e3f2fd">Light Blue</option>
                            <option value="#fce4ec">Light Pink</option>
                            <option value="#e8f5e9">Light Green</option>
                            <option value="#fff3e0">Light Orange</option>
                            <option value="#f3e5f5">Light Purple</option>
                            <option value="#ffebee">Light Red</option>
                            <option value="#fffde7">Light Yellow</option>
                        </select>
                    </div>
                </div>

            </div>
        </div>

        <style>
            .property-section {
                padding: 10px;
            }

            .form-label {
                font-weight: bold;
            }

            .form-control {
                margin-bottom: 5px;
            }
        </style>


        <fieldset class="border mt-2 p-3 d-none">
            <legend>Helper</legend>
            <button onclick="save()" class="btn btn-sm btn-primary">Save</button>
            <button onclick="load()" class="btn btn-sm btn-primary">Load</button>
            <button onclick="group()" class="btn btn-sm btn-primary">Group</button>
            <button onclick="ungroup()" class="btn btn-sm btn-primary">Ungroup</button>
        </fieldset>

        <pre><div id="modelJson" style="width:100%;height:600px" class="d-none">
            { "class": "GraphLinksModel",
            "nodeDataArray": [
          {{-- {"type":"Server","text":"Server","key":-1,"loc":"-10 -200","data":{"ip":"192.168.0.1","os":"Linux","cpu":"8 cores","ram":"16GB","memory":"256GB","app_list":"App1, App2"}},
          {"type":"load-balancer","text":"Load Balancer","key":-2,"loc":"-10 -100","ip":"192.168.0.1"},
          {"type":"service","text":"Service","key":-3,"loc":"-130 90","data":{"port":"8080","path_url":"/api/v1/resource","request_params":"param1=value1\u00b6m2=value2","response_params":"result=success"}},
          {"type":"Server","text":"Server","key":-4,"loc":"-130 -10","data":{"ip":"192.168.0.2","os":"Windows","cpu":"4 cores","ram":"8GB","memory":"128GB","app_list":"App3, App4"}},
          {"type":"Server","text":"Server","key":-5,"loc":"-10 -10","data":{"ip":"192.168.0.3","os":"Linux","cpu":"8 cores","ram":"16GB","memory":"512GB","app_list":"App5, App6"}},
          {"type":"database","text":"Database","key":-6,"loc":"-10 90","data":{"ip":"192.168.0.4","os":"Linux","cpu":"8 cores","ram":"16GB","memory":"256GB"}},
          {"type":"Server","text":"Server","key":-7,"loc":"100 60","data":{"ip":"192.168.0.4","os":"Linux","cpu":"8 cores","ram":"16GB","memory":"256GB","app_list":"App7, App8"}} --}}
          ],
            "linkDataArray": [
          {{-- {"from":-1,"to":-2},
          {"from":-4,"to":-3},
          {"from":-5,"to":-6},
          {"from":-5,"to":-7},
          {"from":-2,"to":-5},
          {"from":-2,"to":-4} --}}
          ]}
        </div></pre>
    </div>
    @push('my-scripts')
        <script>
            function getId() {
                return "{{ isset($id) ? $id : '' }}"
            }

            function getCurrentPath() {
                return "{{ request()->path() }}";
            }

            function getPathUrl() {
                var currentPath = getCurrentPath();

                if (currentPath === 'application') {
                    return 'paginate';
                } else if (currentPath === 'application/create') {
                    return 'create';
                } else if (currentPath.startsWith('application/') && currentPath.endsWith('/edit')) {
                    return 'edit';
                } else if (currentPath.startsWith('application/')) {
                    return 'view';
                } else {
                    console.error('Invalid route specified');
                    return '';
                }
            }
        </script>
        <script src="{{ asset('assets/js/diagram-gojs/index.js') }}" type="module"></script>
        <script src="{{ asset('assets/js/go.js') }}"></script>
        <script src="{{ asset('assets/js/linkShiftTools.js') }}"></script>
    @endpush
@endsection
