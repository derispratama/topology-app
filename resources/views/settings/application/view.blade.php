<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Topology</title>
    <style>
        body,
        html {
            margin: 0;
            /* display: flex;
            flex-direction: column; */
            /* justify-content: center;
            align-items: center; */
        }

        #graphDiv {
            /* text-align: center; */
            /* margin-bottom: 20px; */
            /* width: 100vw;
            height: 100vh; */
            /* max-width: 100vw;
            max-height: 100vh; */
        }

        #topology {
            display: flex;
            justify-content: center;
            width: 100%;
        }
    </style>
</head>

<body>
    <div>
        <div id="graphDiv"></div>
        <div id="topology">
        </div>
    </div>

    <script src="../assets/js/jquery.js"></script>
    <!--begin::Global Javascript Bundle(mandatory for all pages)-->
    <script src="../assets/plugins/global/plugins.bundle.js"></script>
    <script src="../assets/js/scripts.bundle.js"></script>
    <!--end::Global Javascript Bundle-->
    <!--begin::Vendors Javascript(used for this page only)-->
    <script src="../assets/plugins/custom/datatables/datatables.bundle.js"></script>
    <script src="../assets/plugins/custom/vis-timeline/vis-timeline.bundle.js"></script>
    <script src="../assets/js/custom/apps/customers/list/list.js"></script>
    <!--end::Vendors Javascript-->
    <!--begin::Custom Javascript(used for this page only)-->
    <script src="../assets/js/widgets.bundle.js"></script>
    <script src="../assets/js/custom/apps/chat/chat.js"></script>
    <script src="../assets/js/custom/utilities/modals/upgrade-plan.js"></script>
    <script src="../assets/js/custom/utilities/modals/create-app.js"></script>
    <script src="../assets/js/custom/utilities/modals/users-search.js"></script>
    <script>
        function getId() {
            return "{{ isset($id) ? $id : '' }}"
        }

        function getEnv() {
            return "{{ env('API_URL_LOCAL') }}"
        }
    </script>
    <script src="{{ asset('assets/js/applications/view-app.js') }}" type="module"></script>

</body>

</html>
