@extends('layouts.main')

@section('container')
    <link rel="stylesheet" href="{{ asset('assets/css/tree.css') }}">

    <div class="row">
        <div class="col-12">
            <x-breadcrumb :paths="['Home', 'Settings', 'Application', 'Create']" :urls="['', '', '/application', '']" />
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-6">
            <div class="card mb-5">
                <div class="card-header">
                    <h5 class="card-title">Form Create Application</h5>
                    <div class="card-title">
                        <button class="btn btn-primary btn-sm" id="generateBtn" style="margin-right: 5px;">Generate</button>
                        <button class="btn btn-success btn-sm" id="saveBtn">Save</button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="mb-3">
                        <label for="application_name" class="form-label">Application Name</label>
                        <input type="text" class="form-control" id="application_name" placeholder="application name"
                            autofocus>
                    </div>
                    <div class="mb-3">
                        <label for="departement" class="form-label">Departement</label>
                        <input type="text" class="form-control" readonly id="departement" placeholder="application name"
                            value="Financing & Collection Services">
                    </div>
                    <div class="mb-3">
                        <button class="btn btn-success btn-sm" id="addServerBtn">+ Add Server</button>
                    </div>

                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Tree Diagram</h5>
                    <button class="btn btn-sm" id="saveTopologyBtn"><i class="bi bi-arrows-angle-expand fs-3"></i></button>
                </div>
                <div class="card-body">
                    <h1>Topology</h1>
                    <ol class="wtree fs-5">
                        <li class="server-level">
                            <div class="label-item">
                                <span>Runtime Engine 1</span>
                                <div class="button-container">
                                    <button class="btn btn-sm btn-success">add</button>
                                    <button class="btn btn-sm btn-primary">edit</button>
                                    <button class="btn btn-sm btn-danger">remove</button>
                                </div>
                            </div>
                            <ol>
                                <li class="surrounding-db">
                                    <div class="label-item">
                                        <span>Database 1</span>
                                        <div class="button-container">
                                            <button class="btn btn-sm btn-primary">edit</button>
                                            <button class="btn btn-sm btn-danger">remove</button>
                                        </div>
                                    </div>
                                </li>
                                <li class="surrounding-db">
                                    <div class="label-item">
                                        <span>Surrounding Server 1</span>
                                        <div class="button-container">
                                            <button class="btn btn-sm btn-success">add</button>
                                            <button class="btn btn-sm btn-primary">edit</button>
                                            <button class="btn btn-sm btn-danger">remove</button>
                                        </div>
                                    </div>
                                    <ol>
                                        <li>
                                            <div class="label-item">
                                                <span>Cek KTP</span>
                                                <div class="button-container">
                                                    <button class="btn btn-sm btn-primary">edit</button>
                                                    <button class="btn btn-sm btn-danger">remove</button>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="label-item">
                                                <span>CEK SLIK </span>
                                                <div class="button-container">
                                                    <button class="btn btn-sm btn-primary">edit</button>
                                                    <button class="btn btn-sm btn-danger">remove</button>
                                                </div>
                                            </div>
                                        </li>
                                    </ol>
                                </li>
                                <li class="load-balancer">
                                    <div class="label-item">
                                        <span>Load Balancer 1</span>
                                        <div class="button-container">
                                            <button class="btn btn-sm btn-success">add</button>
                                            <button class="btn btn-sm btn-primary">edit</button>
                                            <button class="btn btn-sm btn-danger">remove</button>
                                        </div>
                                    </div>
                                    <ol>
                                        <li class="surrounding-db">
                                            <div class="label-item">
                                                <span>Node 1 Surrounding Server 2</span>
                                                <div class="button-container">
                                                    <button class="btn btn-sm btn-success">add</button>
                                                    <button class="btn btn-sm btn-primary">edit</button>
                                                    <button class="btn btn-sm btn-danger">remove</button>
                                                </div>
                                            </div>
                                            <ol>
                                                <li>
                                                    <div class="label-item">
                                                        <span>Service 1</span>
                                                        <div class="button-container">
                                                            <button class="btn btn-sm btn-primary">edit</button>
                                                            <button class="btn btn-sm btn-danger">remove</button>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="label-item">
                                                        <span>Service 2</span>
                                                        <div class="button-container">
                                                            <button class="btn btn-sm btn-primary">edit</button>
                                                            <button class="btn btn-sm btn-danger">remove</button>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="label-item">
                                                        <span>Service 3</span>
                                                        <div class="button-container">
                                                            <button class="btn btn-sm btn-primary">edit</button>
                                                            <button class="btn btn-sm btn-danger">remove</button>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ol>
                                        </li>
                                        <li class="surrounding-db">
                                            <div class="label-item">
                                                <span>Node 2 Surrounding Server 2</span>
                                                <div class="button-container">
                                                    <button class="btn btn-sm btn-success">add</button>
                                                    <button class="btn btn-sm btn-primary">edit</button>
                                                    <button class="btn btn-sm btn-danger">remove</button>
                                                </div>
                                            </div>
                                            <ol>
                                                <li>
                                                    <div class="label-item">
                                                        <span>Service 1</span>
                                                        <div class="button-container">
                                                            <button class="btn btn-sm btn-primary">edit</button>
                                                            <button class="btn btn-sm btn-danger">remove</button>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="label-item">
                                                        <span>Service 2</span>
                                                        <div class="button-container">
                                                            <button class="btn btn-sm btn-primary">edit</button>
                                                            <button class="btn btn-sm btn-danger">remove</button>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ol>
                                        </li>
                                        <li class="surrounding-db">
                                            <div class="label-item">
                                                <span>Node 3 Surrounding Server 2</span>
                                                <div class="button-container">
                                                    <button class="btn btn-sm btn-success">add</button>
                                                    <button class="btn btn-sm btn-primary">edit</button>
                                                    <button class="btn btn-sm btn-danger">remove</button>
                                                </div>
                                            </div>
                                            <ol>
                                                <li>
                                                    <div class="label-item">
                                                        <span>Service 3</span>
                                                        <div class="button-container">
                                                            <button class="btn btn-sm btn-primary">edit</button>
                                                            <button class="btn btn-sm btn-danger">remove</button>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ol>
                                        </li>
                                    </ol>
                                </li>
                            </ol>
                        </li>
                        <li class="server-level">
                            <div class="label-item">
                                <span>Runtime Engine 2</span>
                                <div class="button-container">
                                    <button class="btn btn-sm btn-success">add</button>
                                    <button class="btn btn-sm btn-primary">edit</button>
                                    <button class="btn btn-sm btn-danger">remove</button>
                                </div>
                            </div>
                            <ol>
                                <li class="surrounding-db">
                                    <div class="label-item">
                                        <span>Surrounding Server 3</span>
                                        <div class="button-container">
                                            <button class="btn btn-sm btn-success">add</button>
                                            <button class="btn btn-sm btn-primary">edit</button>
                                            <button class="btn btn-sm btn-danger">remove</button>
                                        </div>
                                    </div>
                                    <ol>
                                        <li>
                                            <div class="label-item">
                                                <span>Face Recognition</span>
                                                <div class="button-container">
                                                    <button class="btn btn-sm btn-primary">edit</button>
                                                    <button class="btn btn-sm btn-danger">remove</button>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="label-item">
                                                <span>BI Checking</span>
                                                <div class="button-container">
                                                    <button class="btn btn-sm btn-primary">edit</button>
                                                    <button class="btn btn-sm btn-danger">remove</button>
                                                </div>
                                            </div>
                                        </li>
                                    </ol>
                                </li>
                                <li class="surrounding-db">
                                    <div class="label-item">
                                        <span>DB 3</span>
                                        <div class="button-container">
                                            <button class="btn btn-sm btn-success">add</button>
                                            <button class="btn btn-sm btn-primary">edit</button>
                                            <button class="btn btn-sm btn-danger">remove</button>
                                        </div>
                                    </div>
                                </li>
                            </ol>
                        </li>
                    </ol>

                </div>
            </div>

        </div>
        <div class="col-12 col-md-6">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Topology Preview</h5>
                    <button class="btn btn-sm" id="saveTopologyBtn"><i
                            class="bi bi-arrows-angle-expand fs-3"></i></button>
                </div>
                <div class="card-body">
                    <div id="graphDiv"></div>
                    <div id="topology"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
