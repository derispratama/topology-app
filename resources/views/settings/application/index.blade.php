@extends('layouts.main')
@section('container')
    <x-breadcrumb :paths="['Home', 'Settings', 'Application']" :urls="['/dashboard', '', '']" />
    <div class="card">
        <div class="card-body">
            <a href="/application/create" class="btn btn-primary">Add Application</a>
            <table id="kt_datatable_dom_positioning" class="table table-striped table-row-bordered gy-5 gs-7 border rounded">
                <thead>
                    <tr class="fw-bold fs-6 text-gray-800 px-7">
                        <th>No</th>
                        <th>Application Name</th>
                        <th>Departement</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>



    @push('my-scripts')
        <script src="{{ asset('assets/js/applications/index.js') }}" type="module"></script>
        <script>
            const API_URL = "{{ env('API_URL_LOCAL') }}"
            const token = localStorage.getItem('token')
            const user = JSON.parse(localStorage.getItem("user") || null);
            const departmentId = @json($id) == null ? '' : @json($id);

            const payload = {
                'department_id': departmentId,
                'user_id': user.id
            }
            const encodedPayload = btoa(JSON.stringify(payload))
            getDataTable();

            function getDataTable() {
                $.ajax({
                    url: API_URL + `/api/applications?payload=${encodedPayload}`,
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    },
                    dataType: 'json',
                    beforeSend(res) {},
                    complete(res) {},
                    error(jqXHR, textStatus, errorThrown) {
                        if (jqXHR.status === 401) {
                            Swal.fire({
                                html: `Invalid token, please login!`,
                                icon: "error",
                                buttonsStyling: false,
                                confirmButtonText: "Ok",
                                customClass: {
                                    confirmButton: "btn btn-danger",
                                },
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    localStorage.removeItem("token");
                                    localStorage.removeItem("user");
                                    window.location.href = "/";
                                }
                            });
                        } else {
                            console.log("Error:", errorThrown);
                        }
                    },
                    success(res) {
                        let no = 1;
                        if ($.fn.DataTable.isDataTable("#kt_datatable_dom_positioning")) {
                            $("#kt_datatable_dom_positioning").DataTable().destroy();
                        }

                        // Clear the table body
                        $('#kt_datatable_dom_positioning tbody').empty();
                        const html = res.data.map((v, i) => {
                            return `<tr>
                                <td>${no++}</td>
                                <td>${v.application_name}</td>
                                <td>${v.departement.departement_name}</td>
                                <td>
                                    <a href="/application/${v.id}/edit" class="btn btn-warning"> Edit</a>  
                                    <a class="btn btn-light-danger"  onclick="warningMessage('${v.id}')">Delete</a>  
                                    <a href="/application/${v.id}" class="btn btn-info">Detail</a>  
                                </td>
                            </tr>`;
                        })
                        $('#kt_datatable_dom_positioning tbody').html(html);

                        $("#kt_datatable_dom_positioning").DataTable({
                            "language": {
                                "lengthMenu": "Show _MENU_",
                            },
                            "dom": "<'row mb-2'" +
                                "<'col-sm-6 d-flex align-items-center justify-conten-start dt-toolbar'l>" +
                                "<'col-sm-6 d-flex align-items-center justify-content-end dt-toolbar'f>" +
                                ">" +

                                "<'table-responsive'tr>" +

                                "<'row'" +
                                "<'col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start'i>" +
                                "<'col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end'p>" +
                                ">"
                        });
                    },
                })
            }

            window.warningMessage = function(id) {
                Swal.fire({
                    html: `Are you sure you want to delete this application diagram?`,
                    icon: "warning",
                    buttonsStyling: false,
                    showCancelButton: true,
                    cancelButtonText: 'Nope, cancel it',
                    confirmButtonText: "Ok, got it!",
                    customClass: {
                        cancelButton: 'btn btn-primary',
                        confirmButton: "btn ",
                    }
                }).then((result) => {
                    if (result.isConfirmed) {
                        deleteAppById(id)
                    }
                });
            }



            async function deleteAppById(id) {
                try {
                    const response = await fetch(`${API_URL}/api/applications/${id}`, {
                        method: 'DELETE',
                        headers: {
                            'Content-Type': 'application/json',
                            Authorization: `Bearer ${token}`,
                            // Add any additional headers you need here
                        }
                    });

                    if (!response.ok) {
                        throw new Error(`Error: ${response.status} ${response.statusText}`);
                    }

                    const result = await response.json();

                    Swal.fire({
                        icon: 'success',
                        title: 'Deleted!',
                        text: 'The application has been deleted.',
                        confirmButtonText: 'OK'
                    }).then(() => {
                        getDataTable(); // Refresh the table after the delete confirmation
                    });
                } catch (error) {
                    console.error('Error deleting application:', error);
                    Swal.fire({
                        icon: 'error',
                        title: 'Error!',
                        text: 'There was a problem deleting the application.',
                        confirmButtonText: 'OK'
                    });
                }
            }
        </script>
    @endpush
@endsection
