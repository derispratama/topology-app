@extends('layouts.main')

@section('container')
    @include('settings.application.modal')
    <x-breadcrumb :paths="['Home', 'Settings', 'Application', 'Create']" :urls="['', '', '/application', '']" />

    <div class="row">
        <div class="col-6">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Form Create Application</h5>
                    <div class="card-title">
                        <button class="btn btn-primary btn-sm" id="generateBtn" style="margin-right: 5px;">Generate</button>
                        <button class="btn btn-success btn-sm" id="saveBtn">Save</button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="mb-3">
                        <label for="application_name" class="form-label">Application Name</label>
                        <input type="text" class="form-control" id="application_name" placeholder="application name"
                            autofocus>
                        <span class="text-danger m-2" id="application_name_error"></span>
                    </div>
                    <div class="mb-3">
                        <label for="departement" class="form-label">Departement</label>
                        <input type="text" class="form-control" disabled id="departement" placeholder="application name"
                            value="Financing & Collection Services">
                    </div>

                    <div class="mb-3">
                        <button class="btn btn-success btn-sm" id="addServerBtn">+ Add Server</button>
                    </div>

                    <table class="table table-bordered" id="serverTable">
                        <thead>
                            <tr>
                                <th scope="col" style="width: 5%;">#</th>
                                <th scope="col" style="width: 40%;">Server Name</th>
                                <th scope="col" style="width: 35%;">IP Address</th>
                                <th scope="col" style="width: 20%;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- Rows will be added dynamically here -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Topology Preview</h5>
                </div>
                <div class="card-body">
                    <div id="graphDiv"></div>
                    <div id="topology"></div>
                </div>
            </div>
        </div>
    </div>

    @push('my-scripts')
        <script src="{{ asset('assets/js/generate-topology.js') }}" type="module"></script>
        <script src="{{ asset('assets/js/application-form.js') }}" type="module"></script>
        <script>
            function getId() {
                return "{{ isset($id) ? $id : '' }}"
            }

            function getEnv() {
                return "{{ env('API_URL_LOCAL') }}"
            }

            function getMermaid() {
                return "{{ asset('node_modules/mermaid/dist/mermaid.esm.min.mjs') }}";
            }

            function getCurrentPath() {
                return "{{ request()->path() }}";
            }

            function getPathUrl() {
                var currentPath = getCurrentPath();

                if (currentPath === 'application') {
                    return 'paginate';
                } else if (currentPath === 'application/create-old') {
                    return 'create';
                } else if (currentPath.startsWith('application/') && currentPath.endsWith('/edit')) {
                    return 'edit';
                } else if (currentPath.startsWith('application/')) {
                    return 'view';
                } else {
                    console.error('Invalid route specified');
                    return '';
                }
            }
        </script>
    @endpush
@endsection
