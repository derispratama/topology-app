@extends('layouts.main')

@section('container')
    <style>
        @keyframes dash {
            from {
                stroke-dashoffset: 1000;
            }

            to {
                stroke-dashoffset: 0;
            }
        }

        .animated-link {
            stroke-dasharray: 5, 5;
            animation: dash 2s linear infinite;
        }

        #group-btn:hover {
            background-color: #218838 !important;
        }

        #ungroup-btn:hover {
            background-color: #C82333 !important;
        }
    </style>

    <div id="allSampleContent" class="w-full">

        <!-- Diagram Column -->
        <div id="myDiagramDiv"
            style="
            height: 80vh; /* Viewport height */
            width: 100%; /* Viewport width */
            border: 1px solid #ccc;
            border-radius: 3px;
            margin: 0; /* Remove margin */
            padding: 0; /* Remove padding */
            position: relative;
            -webkit-tap-highlight-color: rgba(255, 255, 255, 0);
            cursor: auto;
            background-color: #fff;
                ">
        </div>

        <style>
            .property-section {
                padding: 10px;
            }

            .form-label {
                font-weight: bold;
            }

            .form-control {
                margin-bottom: 5px;
            }
        </style>

        <pre><div id="modelJson" style="width:100%;height:600px" class="d-none">
            { "class": "GraphLinksModel",
            "nodeDataArray": [
          
          ],
            "linkDataArray": [
        
          ]}
        </div></pre>
    </div>
    @push('my-scripts')
        <script>
            function getId() {
                return "{{ isset($id) ? $id : '' }}"
            }

            function getPathUrl() {
                var currentPath = getCurrentPath();

                if (currentPath === 'application') {
                    return 'paginate';
                } else if (currentPath === 'application/create') {
                    return 'create';
                } else if (currentPath.startsWith('application/') && currentPath.endsWith('/edit')) {
                    return 'edit';
                } else if (currentPath.startsWith('application/')) {
                    return 'view';
                } else {
                    console.error('Invalid route specified');
                    return '';
                }
            }
        </script>
        <script src="{{ asset('assets/js/diagram-gojs/view.js') }}" type="module"></script>
        <script src="{{ asset('assets/js/go.js') }}"></script>
        <script src="{{ asset('assets/js/linkShiftTools.js') }}"></script>
    @endpush
@endsection
