@extends('layouts.main')

@section('container')
    <link rel="stylesheet" href="{{ asset('assets/css/tree.css') }}">
    <x-choose-server-modal />
    <x-server-modal />
    <x-surrounding-modal />
    <x-load-balancer-modal />
    <x-confirm-alert-modal />
    <x-service-modal />
    <div class="row">
        <div class="col-12">
            <x-breadcrumb :paths="['Home', 'Settings', 'Application', 'Create']" :urls="['', '', '/application', '']" />
        </div>
    </div>
    <div class="row">
        <x-topology-preview />
        <div class="col-12 ">
            <div class="row">
                <x-form-create-application />
                <x-tree-diagram />
            </div>
        </div>
    </div>
    @push('my-scripts')
        <script src="{{ asset('assets/js/applications/create-applications.js') }}" type="module"></script>
        <script>
            function getId() {
                return "{{ isset($id) ? $id : '' }}"
            }

            function getEnv() {
                return "{{ env('API_URL_LOCAL') }}"
            }

            function getCurrentPath() {
                return "{{ request()->path() }}";
            }

            function getPathUrl() {
                var currentPath = getCurrentPath();

                if (currentPath === 'application') {
                    return 'paginate';
                } else if (currentPath === 'application/create') {
                    return 'create';
                } else if (currentPath.startsWith('application/') && currentPath.endsWith('/edit')) {
                    return 'edit';
                } else if (currentPath.startsWith('application/')) {
                    return 'view';
                } else {
                    console.error('Invalid route specified');
                    return '';
                }
            }
        </script>
    @endpush
@endsection
